/**
 */
package org.eclipse.kit.decia.provider;

import java.util.Collection;
import java.util.List;

import org.eclipse.emf.common.notify.AdapterFactory;
import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.common.util.ResourceLocator;

import org.eclipse.emf.ecore.EStructuralFeature;

import org.eclipse.emf.edit.provider.ComposeableAdapterFactory;
import org.eclipse.emf.edit.provider.IEditingDomainItemProvider;
import org.eclipse.emf.edit.provider.IItemLabelProvider;
import org.eclipse.emf.edit.provider.IItemPropertyDescriptor;
import org.eclipse.emf.edit.provider.IItemPropertySource;
import org.eclipse.emf.edit.provider.IStructuredItemContentProvider;
import org.eclipse.emf.edit.provider.ITreeItemContentProvider;
import org.eclipse.emf.edit.provider.ItemPropertyDescriptor;
import org.eclipse.emf.edit.provider.ItemProviderAdapter;
import org.eclipse.emf.edit.provider.ViewerNotification;

import org.eclipse.kit.decia.Cloud;
import org.eclipse.kit.decia.DeciaFactory;
import org.eclipse.kit.decia.DeciaPackage;

/**
 * This is the item provider adapter for a {@link org.eclipse.kit.decia.Cloud} object.
 * <!-- begin-user-doc -->
 * <!-- end-user-doc -->
 * @generated
 */
public class CloudItemProvider extends ItemProviderAdapter implements IEditingDomainItemProvider,
		IStructuredItemContentProvider, ITreeItemContentProvider, IItemLabelProvider, IItemPropertySource {
	/**
	 * This constructs an instance from a factory and a notifier.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public CloudItemProvider(AdapterFactory adapterFactory) {
		super(adapterFactory);
	}

	/**
	 * This returns the property descriptors for the adapted class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public List<IItemPropertyDescriptor> getPropertyDescriptors(Object object) {
		if (itemPropertyDescriptors == null) {
			super.getPropertyDescriptors(object);

			addNamePropertyDescriptor(object);
		}
		return itemPropertyDescriptors;
	}

	/**
	 * This adds a property descriptor for the Name feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addNamePropertyDescriptor(Object object) {
		itemPropertyDescriptors
				.add(createItemPropertyDescriptor(((ComposeableAdapterFactory) adapterFactory).getRootAdapterFactory(),
						getResourceLocator(), getString("_UI_Cloud_name_feature"),
						getString("_UI_PropertyDescriptor_description", "_UI_Cloud_name_feature", "_UI_Cloud_type"),
						DeciaPackage.Literals.CLOUD__NAME, true, false, false,
						ItemPropertyDescriptor.GENERIC_VALUE_IMAGE, null, null));
	}

	/**
	 * This specifies how to implement {@link #getChildren} and is used to deduce an appropriate feature for an
	 * {@link org.eclipse.emf.edit.command.AddCommand}, {@link org.eclipse.emf.edit.command.RemoveCommand} or
	 * {@link org.eclipse.emf.edit.command.MoveCommand} in {@link #createCommand}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Collection<? extends EStructuralFeature> getChildrenFeatures(Object object) {
		if (childrenFeatures == null) {
			super.getChildrenFeatures(object);
			childrenFeatures.add(DeciaPackage.Literals.CLOUD__HAS_CONFIG);
			childrenFeatures.add(DeciaPackage.Literals.CLOUD__CONTAINS);
			childrenFeatures.add(DeciaPackage.Literals.CLOUD__HAS_ENVIRONMENT);
		}
		return childrenFeatures;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EStructuralFeature getChildFeature(Object object, Object child) {
		// Check the type of the specified child object and return the proper feature to use for
		// adding (see {@link AddCommand}) it as a child.

		return super.getChildFeature(object, child);
	}

	/**
	 * This returns Cloud.gif.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object getImage(Object object) {
		return overlayImage(object, getResourceLocator().getImage("full/obj16/Cloud"));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected boolean shouldComposeCreationImage() {
		return true;
	}

	/**
	 * This returns the label text for the adapted class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String getText(Object object) {
		String label = ((Cloud) object).getName();
		return label == null || label.length() == 0 ? getString("_UI_Cloud_type")
				: getString("_UI_Cloud_type") + " " + label;
	}

	/**
	 * This handles model notifications by calling {@link #updateChildren} to update any cached
	 * children and by creating a viewer notification, which it passes to {@link #fireNotifyChanged}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void notifyChanged(Notification notification) {
		updateChildren(notification);

		switch (notification.getFeatureID(Cloud.class)) {
		case DeciaPackage.CLOUD__NAME:
			fireNotifyChanged(new ViewerNotification(notification, notification.getNotifier(), false, true));
			return;
		case DeciaPackage.CLOUD__HAS_CONFIG:
		case DeciaPackage.CLOUD__CONTAINS:
		case DeciaPackage.CLOUD__HAS_ENVIRONMENT:
			fireNotifyChanged(new ViewerNotification(notification, notification.getNotifier(), true, false));
			return;
		}
		super.notifyChanged(notification);
	}

	/**
	 * This adds {@link org.eclipse.emf.edit.command.CommandParameter}s describing the children
	 * that can be created under this object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected void collectNewChildDescriptors(Collection<Object> newChildDescriptors, Object object) {
		super.collectNewChildDescriptors(newChildDescriptors, object);

		newChildDescriptors.add(createChildParameter(DeciaPackage.Literals.CLOUD__HAS_CONFIG,
				DeciaFactory.eINSTANCE.createCFConfiguration()));

		newChildDescriptors.add(createChildParameter(DeciaPackage.Literals.CLOUD__CONTAINS,
				DeciaFactory.eINSTANCE.createMicroservice()));

		newChildDescriptors.add(createChildParameter(DeciaPackage.Literals.CLOUD__CONTAINS,
				DeciaFactory.eINSTANCE.createRabbitMQBroker()));

		newChildDescriptors.add(createChildParameter(DeciaPackage.Literals.CLOUD__CONTAINS,
				DeciaFactory.eINSTANCE.createThresholdbasedAutoscaler()));

		newChildDescriptors.add(
				createChildParameter(DeciaPackage.Literals.CLOUD__CONTAINS, DeciaFactory.eINSTANCE.createRabbitMQ()));

		newChildDescriptors.add(createChildParameter(DeciaPackage.Literals.CLOUD__CONTAINS,
				DeciaFactory.eINSTANCE.createSystemStatMetric()));

		newChildDescriptors.add(createChildParameter(DeciaPackage.Literals.CLOUD__CONTAINS,
				DeciaFactory.eINSTANCE.createQueueMetric()));

		newChildDescriptors.add(createChildParameter(DeciaPackage.Literals.CLOUD__HAS_ENVIRONMENT,
				DeciaFactory.eINSTANCE.createEnvironment()));
	}

	/**
	 * Return the resource locator for this item provider's resources.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public ResourceLocator getResourceLocator() {
		return DeciaEditPlugin.INSTANCE;
	}

}
