/**
 */
package org.eclipse.kit.decia.provider;

import java.util.Collection;
import java.util.List;

import org.eclipse.emf.common.notify.AdapterFactory;
import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.edit.provider.ComposeableAdapterFactory;
import org.eclipse.emf.edit.provider.IItemPropertyDescriptor;
import org.eclipse.emf.edit.provider.ItemPropertyDescriptor;
import org.eclipse.emf.edit.provider.ViewerNotification;

import org.eclipse.kit.decia.DeciaPackage;
import org.eclipse.kit.decia.ThresholdbasedAutoscaler;

/**
 * This is the item provider adapter for a {@link org.eclipse.kit.decia.ThresholdbasedAutoscaler} object.
 * <!-- begin-user-doc -->
 * <!-- end-user-doc -->
 * @generated
 */
public class ThresholdbasedAutoscalerItemProvider extends AutoscalerItemProvider {
	/**
	 * This constructs an instance from a factory and a notifier.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ThresholdbasedAutoscalerItemProvider(AdapterFactory adapterFactory) {
		super(adapterFactory);
	}

	/**
	 * This returns the property descriptors for the adapted class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public List<IItemPropertyDescriptor> getPropertyDescriptors(Object object) {
		if (itemPropertyDescriptors == null) {
			super.getPropertyDescriptors(object);

			addMinInstancesPropertyDescriptor(object);
			addMaxInstancesPropertyDescriptor(object);
			addLowerThresholdPropertyDescriptor(object);
			addUpperThresholdPropertyDescriptor(object);
			addScalesMicroservicePropertyDescriptor(object);
			addScalesMessagequeuePropertyDescriptor(object);
			addNamePropertyDescriptor(object);
			addObservesSystemStatMetricPropertyDescriptor(object);
			addObservesQueuemetricPropertyDescriptor(object);
		}
		return itemPropertyDescriptors;
	}

	/**
	 * This adds a property descriptor for the Min Instances feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addMinInstancesPropertyDescriptor(Object object) {
		itemPropertyDescriptors.add(createItemPropertyDescriptor(
				((ComposeableAdapterFactory) adapterFactory).getRootAdapterFactory(), getResourceLocator(),
				getString("_UI_ThresholdbasedAutoscaler_minInstances_feature"),
				getString("_UI_PropertyDescriptor_description", "_UI_ThresholdbasedAutoscaler_minInstances_feature",
						"_UI_ThresholdbasedAutoscaler_type"),
				DeciaPackage.Literals.THRESHOLDBASED_AUTOSCALER__MIN_INSTANCES, true, false, false,
				ItemPropertyDescriptor.GENERIC_VALUE_IMAGE, null, null));
	}

	/**
	 * This adds a property descriptor for the Max Instances feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addMaxInstancesPropertyDescriptor(Object object) {
		itemPropertyDescriptors.add(createItemPropertyDescriptor(
				((ComposeableAdapterFactory) adapterFactory).getRootAdapterFactory(), getResourceLocator(),
				getString("_UI_ThresholdbasedAutoscaler_maxInstances_feature"),
				getString("_UI_PropertyDescriptor_description", "_UI_ThresholdbasedAutoscaler_maxInstances_feature",
						"_UI_ThresholdbasedAutoscaler_type"),
				DeciaPackage.Literals.THRESHOLDBASED_AUTOSCALER__MAX_INSTANCES, true, false, false,
				ItemPropertyDescriptor.GENERIC_VALUE_IMAGE, null, null));
	}

	/**
	 * This adds a property descriptor for the Lower Threshold feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addLowerThresholdPropertyDescriptor(Object object) {
		itemPropertyDescriptors.add(createItemPropertyDescriptor(
				((ComposeableAdapterFactory) adapterFactory).getRootAdapterFactory(), getResourceLocator(),
				getString("_UI_ThresholdbasedAutoscaler_lowerThreshold_feature"),
				getString("_UI_PropertyDescriptor_description", "_UI_ThresholdbasedAutoscaler_lowerThreshold_feature",
						"_UI_ThresholdbasedAutoscaler_type"),
				DeciaPackage.Literals.THRESHOLDBASED_AUTOSCALER__LOWER_THRESHOLD, true, false, false,
				ItemPropertyDescriptor.GENERIC_VALUE_IMAGE, null, null));
	}

	/**
	 * This adds a property descriptor for the Upper Threshold feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addUpperThresholdPropertyDescriptor(Object object) {
		itemPropertyDescriptors.add(createItemPropertyDescriptor(
				((ComposeableAdapterFactory) adapterFactory).getRootAdapterFactory(), getResourceLocator(),
				getString("_UI_ThresholdbasedAutoscaler_upperThreshold_feature"),
				getString("_UI_PropertyDescriptor_description", "_UI_ThresholdbasedAutoscaler_upperThreshold_feature",
						"_UI_ThresholdbasedAutoscaler_type"),
				DeciaPackage.Literals.THRESHOLDBASED_AUTOSCALER__UPPER_THRESHOLD, true, false, false,
				ItemPropertyDescriptor.GENERIC_VALUE_IMAGE, null, null));
	}

	/**
	 * This adds a property descriptor for the Scales Microservice feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addScalesMicroservicePropertyDescriptor(Object object) {
		itemPropertyDescriptors.add(createItemPropertyDescriptor(
				((ComposeableAdapterFactory) adapterFactory).getRootAdapterFactory(), getResourceLocator(),
				getString("_UI_ThresholdbasedAutoscaler_scalesMicroservice_feature"),
				getString("_UI_PropertyDescriptor_description",
						"_UI_ThresholdbasedAutoscaler_scalesMicroservice_feature", "_UI_ThresholdbasedAutoscaler_type"),
				DeciaPackage.Literals.THRESHOLDBASED_AUTOSCALER__SCALES_MICROSERVICE, true, false, true, null, null,
				null));
	}

	/**
	 * This adds a property descriptor for the Scales Messagequeue feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addScalesMessagequeuePropertyDescriptor(Object object) {
		itemPropertyDescriptors.add(createItemPropertyDescriptor(
				((ComposeableAdapterFactory) adapterFactory).getRootAdapterFactory(), getResourceLocator(),
				getString("_UI_ThresholdbasedAutoscaler_scalesMessagequeue_feature"),
				getString("_UI_PropertyDescriptor_description",
						"_UI_ThresholdbasedAutoscaler_scalesMessagequeue_feature", "_UI_ThresholdbasedAutoscaler_type"),
				DeciaPackage.Literals.THRESHOLDBASED_AUTOSCALER__SCALES_MESSAGEQUEUE, true, false, true, null, null,
				null));
	}

	/**
	 * This adds a property descriptor for the Name feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addNamePropertyDescriptor(Object object) {
		itemPropertyDescriptors.add(createItemPropertyDescriptor(
				((ComposeableAdapterFactory) adapterFactory).getRootAdapterFactory(), getResourceLocator(),
				getString("_UI_ThresholdbasedAutoscaler_name_feature"),
				getString("_UI_PropertyDescriptor_description", "_UI_ThresholdbasedAutoscaler_name_feature",
						"_UI_ThresholdbasedAutoscaler_type"),
				DeciaPackage.Literals.THRESHOLDBASED_AUTOSCALER__NAME, true, false, false,
				ItemPropertyDescriptor.GENERIC_VALUE_IMAGE, null, null));
	}

	/**
	 * This adds a property descriptor for the Observes System Stat Metric feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addObservesSystemStatMetricPropertyDescriptor(Object object) {
		itemPropertyDescriptors.add(createItemPropertyDescriptor(
				((ComposeableAdapterFactory) adapterFactory).getRootAdapterFactory(), getResourceLocator(),
				getString("_UI_ThresholdbasedAutoscaler_observesSystemStatMetric_feature"),
				getString("_UI_PropertyDescriptor_description",
						"_UI_ThresholdbasedAutoscaler_observesSystemStatMetric_feature",
						"_UI_ThresholdbasedAutoscaler_type"),
				DeciaPackage.Literals.THRESHOLDBASED_AUTOSCALER__OBSERVES_SYSTEM_STAT_METRIC, true, false, true, null,
				null, null));
	}

	/**
	 * This adds a property descriptor for the Observes Queuemetric feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addObservesQueuemetricPropertyDescriptor(Object object) {
		itemPropertyDescriptors
				.add(createItemPropertyDescriptor(((ComposeableAdapterFactory) adapterFactory).getRootAdapterFactory(),
						getResourceLocator(), getString("_UI_ThresholdbasedAutoscaler_observesQueuemetric_feature"),
						getString("_UI_PropertyDescriptor_description",
								"_UI_ThresholdbasedAutoscaler_observesQueuemetric_feature",
								"_UI_ThresholdbasedAutoscaler_type"),
						DeciaPackage.Literals.THRESHOLDBASED_AUTOSCALER__OBSERVES_QUEUEMETRIC, true, false, true, null,
						null, null));
	}

	/**
	 * This returns ThresholdbasedAutoscaler.gif.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object getImage(Object object) {
		return overlayImage(object, getResourceLocator().getImage("full/obj16/ThresholdbasedAutoscaler"));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected boolean shouldComposeCreationImage() {
		return true;
	}

	/**
	 * This returns the label text for the adapted class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String getText(Object object) {
		String label = ((ThresholdbasedAutoscaler) object).getName();
		return label == null || label.length() == 0 ? getString("_UI_ThresholdbasedAutoscaler_type")
				: getString("_UI_ThresholdbasedAutoscaler_type") + " " + label;
	}

	/**
	 * This handles model notifications by calling {@link #updateChildren} to update any cached
	 * children and by creating a viewer notification, which it passes to {@link #fireNotifyChanged}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void notifyChanged(Notification notification) {
		updateChildren(notification);

		switch (notification.getFeatureID(ThresholdbasedAutoscaler.class)) {
		case DeciaPackage.THRESHOLDBASED_AUTOSCALER__MIN_INSTANCES:
		case DeciaPackage.THRESHOLDBASED_AUTOSCALER__MAX_INSTANCES:
		case DeciaPackage.THRESHOLDBASED_AUTOSCALER__LOWER_THRESHOLD:
		case DeciaPackage.THRESHOLDBASED_AUTOSCALER__UPPER_THRESHOLD:
		case DeciaPackage.THRESHOLDBASED_AUTOSCALER__NAME:
			fireNotifyChanged(new ViewerNotification(notification, notification.getNotifier(), false, true));
			return;
		}
		super.notifyChanged(notification);
	}

	/**
	 * This adds {@link org.eclipse.emf.edit.command.CommandParameter}s describing the children
	 * that can be created under this object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected void collectNewChildDescriptors(Collection<Object> newChildDescriptors, Object object) {
		super.collectNewChildDescriptors(newChildDescriptors, object);
	}

}
