/**
 */
package org.eclipse.kit.decia.impl;

import java.util.Collection;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

import org.eclipse.emf.ecore.util.EObjectContainmentEList;
import org.eclipse.emf.ecore.util.InternalEList;

import org.eclipse.kit.decia.CFConfiguration;
import org.eclipse.kit.decia.Cloud;
import org.eclipse.kit.decia.CloudEntity;
import org.eclipse.kit.decia.DeciaPackage;
import org.eclipse.kit.decia.Environment;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Cloud</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link org.eclipse.kit.decia.impl.CloudImpl#getHasConfig <em>Has Config</em>}</li>
 *   <li>{@link org.eclipse.kit.decia.impl.CloudImpl#getContains <em>Contains</em>}</li>
 *   <li>{@link org.eclipse.kit.decia.impl.CloudImpl#getName <em>Name</em>}</li>
 *   <li>{@link org.eclipse.kit.decia.impl.CloudImpl#getHasEnvironment <em>Has Environment</em>}</li>
 * </ul>
 *
 * @generated
 */
public class CloudImpl extends MinimalEObjectImpl.Container implements Cloud {
	/**
	 * The cached value of the '{@link #getHasConfig() <em>Has Config</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getHasConfig()
	 * @generated
	 * @ordered
	 */
	protected CFConfiguration hasConfig;

	/**
	 * The cached value of the '{@link #getContains() <em>Contains</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getContains()
	 * @generated
	 * @ordered
	 */
	protected EList<CloudEntity> contains;

	/**
	 * The default value of the '{@link #getName() <em>Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getName()
	 * @generated
	 * @ordered
	 */
	protected static final String NAME_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getName() <em>Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getName()
	 * @generated
	 * @ordered
	 */
	protected String name = NAME_EDEFAULT;

	/**
	 * The cached value of the '{@link #getHasEnvironment() <em>Has Environment</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getHasEnvironment()
	 * @generated
	 * @ordered
	 */
	protected EList<Environment> hasEnvironment;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected CloudImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return DeciaPackage.Literals.CLOUD;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public CFConfiguration getHasConfig() {
		return hasConfig;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetHasConfig(CFConfiguration newHasConfig, NotificationChain msgs) {
		CFConfiguration oldHasConfig = hasConfig;
		hasConfig = newHasConfig;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET,
					DeciaPackage.CLOUD__HAS_CONFIG, oldHasConfig, newHasConfig);
			if (msgs == null)
				msgs = notification;
			else
				msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setHasConfig(CFConfiguration newHasConfig) {
		if (newHasConfig != hasConfig) {
			NotificationChain msgs = null;
			if (hasConfig != null)
				msgs = ((InternalEObject) hasConfig).eInverseRemove(this,
						EOPPOSITE_FEATURE_BASE - DeciaPackage.CLOUD__HAS_CONFIG, null, msgs);
			if (newHasConfig != null)
				msgs = ((InternalEObject) newHasConfig).eInverseAdd(this,
						EOPPOSITE_FEATURE_BASE - DeciaPackage.CLOUD__HAS_CONFIG, null, msgs);
			msgs = basicSetHasConfig(newHasConfig, msgs);
			if (msgs != null)
				msgs.dispatch();
		} else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, DeciaPackage.CLOUD__HAS_CONFIG, newHasConfig,
					newHasConfig));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<CloudEntity> getContains() {
		if (contains == null) {
			contains = new EObjectContainmentEList<CloudEntity>(CloudEntity.class, this, DeciaPackage.CLOUD__CONTAINS);
		}
		return contains;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getName() {
		return name;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setName(String newName) {
		String oldName = name;
		name = newName;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, DeciaPackage.CLOUD__NAME, oldName, name));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<Environment> getHasEnvironment() {
		if (hasEnvironment == null) {
			hasEnvironment = new EObjectContainmentEList<Environment>(Environment.class, this,
					DeciaPackage.CLOUD__HAS_ENVIRONMENT);
		}
		return hasEnvironment;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
		case DeciaPackage.CLOUD__HAS_CONFIG:
			return basicSetHasConfig(null, msgs);
		case DeciaPackage.CLOUD__CONTAINS:
			return ((InternalEList<?>) getContains()).basicRemove(otherEnd, msgs);
		case DeciaPackage.CLOUD__HAS_ENVIRONMENT:
			return ((InternalEList<?>) getHasEnvironment()).basicRemove(otherEnd, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
		case DeciaPackage.CLOUD__HAS_CONFIG:
			return getHasConfig();
		case DeciaPackage.CLOUD__CONTAINS:
			return getContains();
		case DeciaPackage.CLOUD__NAME:
			return getName();
		case DeciaPackage.CLOUD__HAS_ENVIRONMENT:
			return getHasEnvironment();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
		case DeciaPackage.CLOUD__HAS_CONFIG:
			setHasConfig((CFConfiguration) newValue);
			return;
		case DeciaPackage.CLOUD__CONTAINS:
			getContains().clear();
			getContains().addAll((Collection<? extends CloudEntity>) newValue);
			return;
		case DeciaPackage.CLOUD__NAME:
			setName((String) newValue);
			return;
		case DeciaPackage.CLOUD__HAS_ENVIRONMENT:
			getHasEnvironment().clear();
			getHasEnvironment().addAll((Collection<? extends Environment>) newValue);
			return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
		case DeciaPackage.CLOUD__HAS_CONFIG:
			setHasConfig((CFConfiguration) null);
			return;
		case DeciaPackage.CLOUD__CONTAINS:
			getContains().clear();
			return;
		case DeciaPackage.CLOUD__NAME:
			setName(NAME_EDEFAULT);
			return;
		case DeciaPackage.CLOUD__HAS_ENVIRONMENT:
			getHasEnvironment().clear();
			return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
		case DeciaPackage.CLOUD__HAS_CONFIG:
			return hasConfig != null;
		case DeciaPackage.CLOUD__CONTAINS:
			return contains != null && !contains.isEmpty();
		case DeciaPackage.CLOUD__NAME:
			return NAME_EDEFAULT == null ? name != null : !NAME_EDEFAULT.equals(name);
		case DeciaPackage.CLOUD__HAS_ENVIRONMENT:
			return hasEnvironment != null && !hasEnvironment.isEmpty();
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy())
			return super.toString();

		StringBuffer result = new StringBuffer(super.toString());
		result.append(" (name: ");
		result.append(name);
		result.append(')');
		return result.toString();
	}

} //CloudImpl
