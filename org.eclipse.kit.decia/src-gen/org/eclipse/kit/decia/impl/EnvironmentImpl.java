/**
 */
package org.eclipse.kit.decia.impl;

import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.ecore.EClass;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

import org.eclipse.kit.decia.DeciaPackage;
import org.eclipse.kit.decia.Environment;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Environment</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link org.eclipse.kit.decia.impl.EnvironmentImpl#getName <em>Name</em>}</li>
 *   <li>{@link org.eclipse.kit.decia.impl.EnvironmentImpl#getProxyHost <em>Proxy Host</em>}</li>
 *   <li>{@link org.eclipse.kit.decia.impl.EnvironmentImpl#getProxyPort <em>Proxy Port</em>}</li>
 * </ul>
 *
 * @generated
 */
public class EnvironmentImpl extends MinimalEObjectImpl.Container implements Environment {
	/**
	 * The default value of the '{@link #getName() <em>Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getName()
	 * @generated
	 * @ordered
	 */
	protected static final String NAME_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getName() <em>Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getName()
	 * @generated
	 * @ordered
	 */
	protected String name = NAME_EDEFAULT;

	/**
	 * The default value of the '{@link #getProxyHost() <em>Proxy Host</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getProxyHost()
	 * @generated
	 * @ordered
	 */
	protected static final String PROXY_HOST_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getProxyHost() <em>Proxy Host</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getProxyHost()
	 * @generated
	 * @ordered
	 */
	protected String proxyHost = PROXY_HOST_EDEFAULT;

	/**
	 * The default value of the '{@link #getProxyPort() <em>Proxy Port</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getProxyPort()
	 * @generated
	 * @ordered
	 */
	protected static final String PROXY_PORT_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getProxyPort() <em>Proxy Port</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getProxyPort()
	 * @generated
	 * @ordered
	 */
	protected String proxyPort = PROXY_PORT_EDEFAULT;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected EnvironmentImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return DeciaPackage.Literals.ENVIRONMENT;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getName() {
		return name;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setName(String newName) {
		String oldName = name;
		name = newName;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, DeciaPackage.ENVIRONMENT__NAME, oldName, name));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getProxyHost() {
		return proxyHost;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setProxyHost(String newProxyHost) {
		String oldProxyHost = proxyHost;
		proxyHost = newProxyHost;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, DeciaPackage.ENVIRONMENT__PROXY_HOST, oldProxyHost,
					proxyHost));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getProxyPort() {
		return proxyPort;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setProxyPort(String newProxyPort) {
		String oldProxyPort = proxyPort;
		proxyPort = newProxyPort;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, DeciaPackage.ENVIRONMENT__PROXY_PORT, oldProxyPort,
					proxyPort));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
		case DeciaPackage.ENVIRONMENT__NAME:
			return getName();
		case DeciaPackage.ENVIRONMENT__PROXY_HOST:
			return getProxyHost();
		case DeciaPackage.ENVIRONMENT__PROXY_PORT:
			return getProxyPort();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
		case DeciaPackage.ENVIRONMENT__NAME:
			setName((String) newValue);
			return;
		case DeciaPackage.ENVIRONMENT__PROXY_HOST:
			setProxyHost((String) newValue);
			return;
		case DeciaPackage.ENVIRONMENT__PROXY_PORT:
			setProxyPort((String) newValue);
			return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
		case DeciaPackage.ENVIRONMENT__NAME:
			setName(NAME_EDEFAULT);
			return;
		case DeciaPackage.ENVIRONMENT__PROXY_HOST:
			setProxyHost(PROXY_HOST_EDEFAULT);
			return;
		case DeciaPackage.ENVIRONMENT__PROXY_PORT:
			setProxyPort(PROXY_PORT_EDEFAULT);
			return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
		case DeciaPackage.ENVIRONMENT__NAME:
			return NAME_EDEFAULT == null ? name != null : !NAME_EDEFAULT.equals(name);
		case DeciaPackage.ENVIRONMENT__PROXY_HOST:
			return PROXY_HOST_EDEFAULT == null ? proxyHost != null : !PROXY_HOST_EDEFAULT.equals(proxyHost);
		case DeciaPackage.ENVIRONMENT__PROXY_PORT:
			return PROXY_PORT_EDEFAULT == null ? proxyPort != null : !PROXY_PORT_EDEFAULT.equals(proxyPort);
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy())
			return super.toString();

		StringBuffer result = new StringBuffer(super.toString());
		result.append(" (name: ");
		result.append(name);
		result.append(", proxyHost: ");
		result.append(proxyHost);
		result.append(", proxyPort: ");
		result.append(proxyPort);
		result.append(')');
		return result.toString();
	}

} //EnvironmentImpl
