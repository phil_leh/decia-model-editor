/**
 */
package org.eclipse.kit.decia.impl;

import java.util.Collection;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;

import org.eclipse.emf.ecore.util.EObjectContainmentEList;
import org.eclipse.emf.ecore.util.EObjectResolvingEList;
import org.eclipse.emf.ecore.util.InternalEList;

import org.eclipse.kit.decia.DeciaPackage;
import org.eclipse.kit.decia.MessageQueue;
import org.eclipse.kit.decia.Microservice;
import org.eclipse.kit.decia.SystemStatMetric;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Microservice</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link org.eclipse.kit.decia.impl.MicroserviceImpl#getProducesMessages <em>Produces Messages</em>}</li>
 *   <li>{@link org.eclipse.kit.decia.impl.MicroserviceImpl#getCfAppName <em>Cf App Name</em>}</li>
 *   <li>{@link org.eclipse.kit.decia.impl.MicroserviceImpl#getConsumesMessages <em>Consumes Messages</em>}</li>
 *   <li>{@link org.eclipse.kit.decia.impl.MicroserviceImpl#getContainsSystemStatMetric <em>Contains System Stat Metric</em>}</li>
 * </ul>
 *
 * @generated
 */
public class MicroserviceImpl extends CloudEntityImpl implements Microservice {
	/**
	 * The cached value of the '{@link #getProducesMessages() <em>Produces Messages</em>}' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getProducesMessages()
	 * @generated
	 * @ordered
	 */
	protected EList<MessageQueue> producesMessages;

	/**
	 * The default value of the '{@link #getCfAppName() <em>Cf App Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getCfAppName()
	 * @generated
	 * @ordered
	 */
	protected static final String CF_APP_NAME_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getCfAppName() <em>Cf App Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getCfAppName()
	 * @generated
	 * @ordered
	 */
	protected String cfAppName = CF_APP_NAME_EDEFAULT;

	/**
	 * The cached value of the '{@link #getConsumesMessages() <em>Consumes Messages</em>}' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getConsumesMessages()
	 * @generated
	 * @ordered
	 */
	protected EList<MessageQueue> consumesMessages;

	/**
	 * The cached value of the '{@link #getContainsSystemStatMetric() <em>Contains System Stat Metric</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getContainsSystemStatMetric()
	 * @generated
	 * @ordered
	 */
	protected EList<SystemStatMetric> containsSystemStatMetric;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected MicroserviceImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return DeciaPackage.Literals.MICROSERVICE;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<MessageQueue> getProducesMessages() {
		if (producesMessages == null) {
			producesMessages = new EObjectResolvingEList<MessageQueue>(MessageQueue.class, this,
					DeciaPackage.MICROSERVICE__PRODUCES_MESSAGES);
		}
		return producesMessages;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getCfAppName() {
		return cfAppName;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setCfAppName(String newCfAppName) {
		String oldCfAppName = cfAppName;
		cfAppName = newCfAppName;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, DeciaPackage.MICROSERVICE__CF_APP_NAME, oldCfAppName,
					cfAppName));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<MessageQueue> getConsumesMessages() {
		if (consumesMessages == null) {
			consumesMessages = new EObjectResolvingEList<MessageQueue>(MessageQueue.class, this,
					DeciaPackage.MICROSERVICE__CONSUMES_MESSAGES);
		}
		return consumesMessages;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<SystemStatMetric> getContainsSystemStatMetric() {
		if (containsSystemStatMetric == null) {
			containsSystemStatMetric = new EObjectContainmentEList<SystemStatMetric>(SystemStatMetric.class, this,
					DeciaPackage.MICROSERVICE__CONTAINS_SYSTEM_STAT_METRIC);
		}
		return containsSystemStatMetric;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
		case DeciaPackage.MICROSERVICE__CONTAINS_SYSTEM_STAT_METRIC:
			return ((InternalEList<?>) getContainsSystemStatMetric()).basicRemove(otherEnd, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
		case DeciaPackage.MICROSERVICE__PRODUCES_MESSAGES:
			return getProducesMessages();
		case DeciaPackage.MICROSERVICE__CF_APP_NAME:
			return getCfAppName();
		case DeciaPackage.MICROSERVICE__CONSUMES_MESSAGES:
			return getConsumesMessages();
		case DeciaPackage.MICROSERVICE__CONTAINS_SYSTEM_STAT_METRIC:
			return getContainsSystemStatMetric();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
		case DeciaPackage.MICROSERVICE__PRODUCES_MESSAGES:
			getProducesMessages().clear();
			getProducesMessages().addAll((Collection<? extends MessageQueue>) newValue);
			return;
		case DeciaPackage.MICROSERVICE__CF_APP_NAME:
			setCfAppName((String) newValue);
			return;
		case DeciaPackage.MICROSERVICE__CONSUMES_MESSAGES:
			getConsumesMessages().clear();
			getConsumesMessages().addAll((Collection<? extends MessageQueue>) newValue);
			return;
		case DeciaPackage.MICROSERVICE__CONTAINS_SYSTEM_STAT_METRIC:
			getContainsSystemStatMetric().clear();
			getContainsSystemStatMetric().addAll((Collection<? extends SystemStatMetric>) newValue);
			return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
		case DeciaPackage.MICROSERVICE__PRODUCES_MESSAGES:
			getProducesMessages().clear();
			return;
		case DeciaPackage.MICROSERVICE__CF_APP_NAME:
			setCfAppName(CF_APP_NAME_EDEFAULT);
			return;
		case DeciaPackage.MICROSERVICE__CONSUMES_MESSAGES:
			getConsumesMessages().clear();
			return;
		case DeciaPackage.MICROSERVICE__CONTAINS_SYSTEM_STAT_METRIC:
			getContainsSystemStatMetric().clear();
			return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
		case DeciaPackage.MICROSERVICE__PRODUCES_MESSAGES:
			return producesMessages != null && !producesMessages.isEmpty();
		case DeciaPackage.MICROSERVICE__CF_APP_NAME:
			return CF_APP_NAME_EDEFAULT == null ? cfAppName != null : !CF_APP_NAME_EDEFAULT.equals(cfAppName);
		case DeciaPackage.MICROSERVICE__CONSUMES_MESSAGES:
			return consumesMessages != null && !consumesMessages.isEmpty();
		case DeciaPackage.MICROSERVICE__CONTAINS_SYSTEM_STAT_METRIC:
			return containsSystemStatMetric != null && !containsSystemStatMetric.isEmpty();
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy())
			return super.toString();

		StringBuffer result = new StringBuffer(super.toString());
		result.append(" (CfAppName: ");
		result.append(cfAppName);
		result.append(')');
		return result.toString();
	}

} //MicroserviceImpl
