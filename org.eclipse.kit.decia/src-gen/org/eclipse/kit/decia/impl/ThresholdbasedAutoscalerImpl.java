/**
 */
package org.eclipse.kit.decia.impl;

import java.util.Collection;

import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;

import org.eclipse.emf.ecore.util.EObjectResolvingEList;

import org.eclipse.kit.decia.DeciaPackage;
import org.eclipse.kit.decia.MessageQueue;
import org.eclipse.kit.decia.Microservice;
import org.eclipse.kit.decia.QueueMetric;
import org.eclipse.kit.decia.SystemStatMetric;
import org.eclipse.kit.decia.ThresholdbasedAutoscaler;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Thresholdbased Autoscaler</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link org.eclipse.kit.decia.impl.ThresholdbasedAutoscalerImpl#getMinInstances <em>Min Instances</em>}</li>
 *   <li>{@link org.eclipse.kit.decia.impl.ThresholdbasedAutoscalerImpl#getMaxInstances <em>Max Instances</em>}</li>
 *   <li>{@link org.eclipse.kit.decia.impl.ThresholdbasedAutoscalerImpl#getLowerThreshold <em>Lower Threshold</em>}</li>
 *   <li>{@link org.eclipse.kit.decia.impl.ThresholdbasedAutoscalerImpl#getUpperThreshold <em>Upper Threshold</em>}</li>
 *   <li>{@link org.eclipse.kit.decia.impl.ThresholdbasedAutoscalerImpl#getScalesMicroservice <em>Scales Microservice</em>}</li>
 *   <li>{@link org.eclipse.kit.decia.impl.ThresholdbasedAutoscalerImpl#getScalesMessagequeue <em>Scales Messagequeue</em>}</li>
 *   <li>{@link org.eclipse.kit.decia.impl.ThresholdbasedAutoscalerImpl#getName <em>Name</em>}</li>
 *   <li>{@link org.eclipse.kit.decia.impl.ThresholdbasedAutoscalerImpl#getObservesSystemStatMetric <em>Observes System Stat Metric</em>}</li>
 *   <li>{@link org.eclipse.kit.decia.impl.ThresholdbasedAutoscalerImpl#getObservesQueuemetric <em>Observes Queuemetric</em>}</li>
 * </ul>
 *
 * @generated
 */
public class ThresholdbasedAutoscalerImpl extends AutoscalerImpl implements ThresholdbasedAutoscaler {
	/**
	 * The default value of the '{@link #getMinInstances() <em>Min Instances</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getMinInstances()
	 * @generated
	 * @ordered
	 */
	protected static final String MIN_INSTANCES_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getMinInstances() <em>Min Instances</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getMinInstances()
	 * @generated
	 * @ordered
	 */
	protected String minInstances = MIN_INSTANCES_EDEFAULT;

	/**
	 * The default value of the '{@link #getMaxInstances() <em>Max Instances</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getMaxInstances()
	 * @generated
	 * @ordered
	 */
	protected static final String MAX_INSTANCES_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getMaxInstances() <em>Max Instances</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getMaxInstances()
	 * @generated
	 * @ordered
	 */
	protected String maxInstances = MAX_INSTANCES_EDEFAULT;

	/**
	 * The default value of the '{@link #getLowerThreshold() <em>Lower Threshold</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getLowerThreshold()
	 * @generated
	 * @ordered
	 */
	protected static final String LOWER_THRESHOLD_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getLowerThreshold() <em>Lower Threshold</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getLowerThreshold()
	 * @generated
	 * @ordered
	 */
	protected String lowerThreshold = LOWER_THRESHOLD_EDEFAULT;

	/**
	 * The default value of the '{@link #getUpperThreshold() <em>Upper Threshold</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getUpperThreshold()
	 * @generated
	 * @ordered
	 */
	protected static final String UPPER_THRESHOLD_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getUpperThreshold() <em>Upper Threshold</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getUpperThreshold()
	 * @generated
	 * @ordered
	 */
	protected String upperThreshold = UPPER_THRESHOLD_EDEFAULT;

	/**
	 * The cached value of the '{@link #getScalesMicroservice() <em>Scales Microservice</em>}' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getScalesMicroservice()
	 * @generated
	 * @ordered
	 */
	protected EList<Microservice> scalesMicroservice;

	/**
	 * The cached value of the '{@link #getScalesMessagequeue() <em>Scales Messagequeue</em>}' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getScalesMessagequeue()
	 * @generated
	 * @ordered
	 */
	protected EList<MessageQueue> scalesMessagequeue;

	/**
	 * The default value of the '{@link #getName() <em>Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getName()
	 * @generated
	 * @ordered
	 */
	protected static final String NAME_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getName() <em>Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getName()
	 * @generated
	 * @ordered
	 */
	protected String name = NAME_EDEFAULT;

	/**
	 * The cached value of the '{@link #getObservesSystemStatMetric() <em>Observes System Stat Metric</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getObservesSystemStatMetric()
	 * @generated
	 * @ordered
	 */
	protected SystemStatMetric observesSystemStatMetric;

	/**
	 * The cached value of the '{@link #getObservesQueuemetric() <em>Observes Queuemetric</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getObservesQueuemetric()
	 * @generated
	 * @ordered
	 */
	protected QueueMetric observesQueuemetric;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected ThresholdbasedAutoscalerImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return DeciaPackage.Literals.THRESHOLDBASED_AUTOSCALER;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getMinInstances() {
		return minInstances;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setMinInstances(String newMinInstances) {
		String oldMinInstances = minInstances;
		minInstances = newMinInstances;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, DeciaPackage.THRESHOLDBASED_AUTOSCALER__MIN_INSTANCES,
					oldMinInstances, minInstances));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getMaxInstances() {
		return maxInstances;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setMaxInstances(String newMaxInstances) {
		String oldMaxInstances = maxInstances;
		maxInstances = newMaxInstances;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, DeciaPackage.THRESHOLDBASED_AUTOSCALER__MAX_INSTANCES,
					oldMaxInstances, maxInstances));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getLowerThreshold() {
		return lowerThreshold;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setLowerThreshold(String newLowerThreshold) {
		String oldLowerThreshold = lowerThreshold;
		lowerThreshold = newLowerThreshold;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET,
					DeciaPackage.THRESHOLDBASED_AUTOSCALER__LOWER_THRESHOLD, oldLowerThreshold, lowerThreshold));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getUpperThreshold() {
		return upperThreshold;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setUpperThreshold(String newUpperThreshold) {
		String oldUpperThreshold = upperThreshold;
		upperThreshold = newUpperThreshold;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET,
					DeciaPackage.THRESHOLDBASED_AUTOSCALER__UPPER_THRESHOLD, oldUpperThreshold, upperThreshold));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<Microservice> getScalesMicroservice() {
		if (scalesMicroservice == null) {
			scalesMicroservice = new EObjectResolvingEList<Microservice>(Microservice.class, this,
					DeciaPackage.THRESHOLDBASED_AUTOSCALER__SCALES_MICROSERVICE);
		}
		return scalesMicroservice;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<MessageQueue> getScalesMessagequeue() {
		if (scalesMessagequeue == null) {
			scalesMessagequeue = new EObjectResolvingEList<MessageQueue>(MessageQueue.class, this,
					DeciaPackage.THRESHOLDBASED_AUTOSCALER__SCALES_MESSAGEQUEUE);
		}
		return scalesMessagequeue;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getName() {
		return name;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setName(String newName) {
		String oldName = name;
		name = newName;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, DeciaPackage.THRESHOLDBASED_AUTOSCALER__NAME, oldName,
					name));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public SystemStatMetric getObservesSystemStatMetric() {
		if (observesSystemStatMetric != null && observesSystemStatMetric.eIsProxy()) {
			InternalEObject oldObservesSystemStatMetric = (InternalEObject) observesSystemStatMetric;
			observesSystemStatMetric = (SystemStatMetric) eResolveProxy(oldObservesSystemStatMetric);
			if (observesSystemStatMetric != oldObservesSystemStatMetric) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE,
							DeciaPackage.THRESHOLDBASED_AUTOSCALER__OBSERVES_SYSTEM_STAT_METRIC,
							oldObservesSystemStatMetric, observesSystemStatMetric));
			}
		}
		return observesSystemStatMetric;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public SystemStatMetric basicGetObservesSystemStatMetric() {
		return observesSystemStatMetric;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setObservesSystemStatMetric(SystemStatMetric newObservesSystemStatMetric) {
		SystemStatMetric oldObservesSystemStatMetric = observesSystemStatMetric;
		observesSystemStatMetric = newObservesSystemStatMetric;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET,
					DeciaPackage.THRESHOLDBASED_AUTOSCALER__OBSERVES_SYSTEM_STAT_METRIC, oldObservesSystemStatMetric,
					observesSystemStatMetric));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public QueueMetric getObservesQueuemetric() {
		if (observesQueuemetric != null && observesQueuemetric.eIsProxy()) {
			InternalEObject oldObservesQueuemetric = (InternalEObject) observesQueuemetric;
			observesQueuemetric = (QueueMetric) eResolveProxy(oldObservesQueuemetric);
			if (observesQueuemetric != oldObservesQueuemetric) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE,
							DeciaPackage.THRESHOLDBASED_AUTOSCALER__OBSERVES_QUEUEMETRIC, oldObservesQueuemetric,
							observesQueuemetric));
			}
		}
		return observesQueuemetric;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public QueueMetric basicGetObservesQueuemetric() {
		return observesQueuemetric;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setObservesQueuemetric(QueueMetric newObservesQueuemetric) {
		QueueMetric oldObservesQueuemetric = observesQueuemetric;
		observesQueuemetric = newObservesQueuemetric;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET,
					DeciaPackage.THRESHOLDBASED_AUTOSCALER__OBSERVES_QUEUEMETRIC, oldObservesQueuemetric,
					observesQueuemetric));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
		case DeciaPackage.THRESHOLDBASED_AUTOSCALER__MIN_INSTANCES:
			return getMinInstances();
		case DeciaPackage.THRESHOLDBASED_AUTOSCALER__MAX_INSTANCES:
			return getMaxInstances();
		case DeciaPackage.THRESHOLDBASED_AUTOSCALER__LOWER_THRESHOLD:
			return getLowerThreshold();
		case DeciaPackage.THRESHOLDBASED_AUTOSCALER__UPPER_THRESHOLD:
			return getUpperThreshold();
		case DeciaPackage.THRESHOLDBASED_AUTOSCALER__SCALES_MICROSERVICE:
			return getScalesMicroservice();
		case DeciaPackage.THRESHOLDBASED_AUTOSCALER__SCALES_MESSAGEQUEUE:
			return getScalesMessagequeue();
		case DeciaPackage.THRESHOLDBASED_AUTOSCALER__NAME:
			return getName();
		case DeciaPackage.THRESHOLDBASED_AUTOSCALER__OBSERVES_SYSTEM_STAT_METRIC:
			if (resolve)
				return getObservesSystemStatMetric();
			return basicGetObservesSystemStatMetric();
		case DeciaPackage.THRESHOLDBASED_AUTOSCALER__OBSERVES_QUEUEMETRIC:
			if (resolve)
				return getObservesQueuemetric();
			return basicGetObservesQueuemetric();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
		case DeciaPackage.THRESHOLDBASED_AUTOSCALER__MIN_INSTANCES:
			setMinInstances((String) newValue);
			return;
		case DeciaPackage.THRESHOLDBASED_AUTOSCALER__MAX_INSTANCES:
			setMaxInstances((String) newValue);
			return;
		case DeciaPackage.THRESHOLDBASED_AUTOSCALER__LOWER_THRESHOLD:
			setLowerThreshold((String) newValue);
			return;
		case DeciaPackage.THRESHOLDBASED_AUTOSCALER__UPPER_THRESHOLD:
			setUpperThreshold((String) newValue);
			return;
		case DeciaPackage.THRESHOLDBASED_AUTOSCALER__SCALES_MICROSERVICE:
			getScalesMicroservice().clear();
			getScalesMicroservice().addAll((Collection<? extends Microservice>) newValue);
			return;
		case DeciaPackage.THRESHOLDBASED_AUTOSCALER__SCALES_MESSAGEQUEUE:
			getScalesMessagequeue().clear();
			getScalesMessagequeue().addAll((Collection<? extends MessageQueue>) newValue);
			return;
		case DeciaPackage.THRESHOLDBASED_AUTOSCALER__NAME:
			setName((String) newValue);
			return;
		case DeciaPackage.THRESHOLDBASED_AUTOSCALER__OBSERVES_SYSTEM_STAT_METRIC:
			setObservesSystemStatMetric((SystemStatMetric) newValue);
			return;
		case DeciaPackage.THRESHOLDBASED_AUTOSCALER__OBSERVES_QUEUEMETRIC:
			setObservesQueuemetric((QueueMetric) newValue);
			return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
		case DeciaPackage.THRESHOLDBASED_AUTOSCALER__MIN_INSTANCES:
			setMinInstances(MIN_INSTANCES_EDEFAULT);
			return;
		case DeciaPackage.THRESHOLDBASED_AUTOSCALER__MAX_INSTANCES:
			setMaxInstances(MAX_INSTANCES_EDEFAULT);
			return;
		case DeciaPackage.THRESHOLDBASED_AUTOSCALER__LOWER_THRESHOLD:
			setLowerThreshold(LOWER_THRESHOLD_EDEFAULT);
			return;
		case DeciaPackage.THRESHOLDBASED_AUTOSCALER__UPPER_THRESHOLD:
			setUpperThreshold(UPPER_THRESHOLD_EDEFAULT);
			return;
		case DeciaPackage.THRESHOLDBASED_AUTOSCALER__SCALES_MICROSERVICE:
			getScalesMicroservice().clear();
			return;
		case DeciaPackage.THRESHOLDBASED_AUTOSCALER__SCALES_MESSAGEQUEUE:
			getScalesMessagequeue().clear();
			return;
		case DeciaPackage.THRESHOLDBASED_AUTOSCALER__NAME:
			setName(NAME_EDEFAULT);
			return;
		case DeciaPackage.THRESHOLDBASED_AUTOSCALER__OBSERVES_SYSTEM_STAT_METRIC:
			setObservesSystemStatMetric((SystemStatMetric) null);
			return;
		case DeciaPackage.THRESHOLDBASED_AUTOSCALER__OBSERVES_QUEUEMETRIC:
			setObservesQueuemetric((QueueMetric) null);
			return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
		case DeciaPackage.THRESHOLDBASED_AUTOSCALER__MIN_INSTANCES:
			return MIN_INSTANCES_EDEFAULT == null ? minInstances != null : !MIN_INSTANCES_EDEFAULT.equals(minInstances);
		case DeciaPackage.THRESHOLDBASED_AUTOSCALER__MAX_INSTANCES:
			return MAX_INSTANCES_EDEFAULT == null ? maxInstances != null : !MAX_INSTANCES_EDEFAULT.equals(maxInstances);
		case DeciaPackage.THRESHOLDBASED_AUTOSCALER__LOWER_THRESHOLD:
			return LOWER_THRESHOLD_EDEFAULT == null ? lowerThreshold != null
					: !LOWER_THRESHOLD_EDEFAULT.equals(lowerThreshold);
		case DeciaPackage.THRESHOLDBASED_AUTOSCALER__UPPER_THRESHOLD:
			return UPPER_THRESHOLD_EDEFAULT == null ? upperThreshold != null
					: !UPPER_THRESHOLD_EDEFAULT.equals(upperThreshold);
		case DeciaPackage.THRESHOLDBASED_AUTOSCALER__SCALES_MICROSERVICE:
			return scalesMicroservice != null && !scalesMicroservice.isEmpty();
		case DeciaPackage.THRESHOLDBASED_AUTOSCALER__SCALES_MESSAGEQUEUE:
			return scalesMessagequeue != null && !scalesMessagequeue.isEmpty();
		case DeciaPackage.THRESHOLDBASED_AUTOSCALER__NAME:
			return NAME_EDEFAULT == null ? name != null : !NAME_EDEFAULT.equals(name);
		case DeciaPackage.THRESHOLDBASED_AUTOSCALER__OBSERVES_SYSTEM_STAT_METRIC:
			return observesSystemStatMetric != null;
		case DeciaPackage.THRESHOLDBASED_AUTOSCALER__OBSERVES_QUEUEMETRIC:
			return observesQueuemetric != null;
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy())
			return super.toString();

		StringBuffer result = new StringBuffer(super.toString());
		result.append(" (minInstances: ");
		result.append(minInstances);
		result.append(", maxInstances: ");
		result.append(maxInstances);
		result.append(", lowerThreshold: ");
		result.append(lowerThreshold);
		result.append(", upperThreshold: ");
		result.append(upperThreshold);
		result.append(", name: ");
		result.append(name);
		result.append(')');
		return result.toString();
	}

} //ThresholdbasedAutoscalerImpl
