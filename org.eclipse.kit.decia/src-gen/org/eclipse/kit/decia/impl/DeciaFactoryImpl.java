/**
 */
package org.eclipse.kit.decia.impl;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EDataType;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EPackage;

import org.eclipse.emf.ecore.impl.EFactoryImpl;

import org.eclipse.emf.ecore.plugin.EcorePlugin;

import org.eclipse.kit.decia.*;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model <b>Factory</b>.
 * <!-- end-user-doc -->
 * @generated
 */
public class DeciaFactoryImpl extends EFactoryImpl implements DeciaFactory {
	/**
	 * Creates the default factory implementation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static DeciaFactory init() {
		try {
			DeciaFactory theDeciaFactory = (DeciaFactory) EPackage.Registry.INSTANCE.getEFactory(DeciaPackage.eNS_URI);
			if (theDeciaFactory != null) {
				return theDeciaFactory;
			}
		} catch (Exception exception) {
			EcorePlugin.INSTANCE.log(exception);
		}
		return new DeciaFactoryImpl();
	}

	/**
	 * Creates an instance of the factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public DeciaFactoryImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EObject create(EClass eClass) {
		switch (eClass.getClassifierID()) {
		case DeciaPackage.CF_CONFIGURATION:
			return createCFConfiguration();
		case DeciaPackage.CLOUD:
			return createCloud();
		case DeciaPackage.MICROSERVICE:
			return createMicroservice();
		case DeciaPackage.RABBIT_MQ_BROKER:
			return createRabbitMQBroker();
		case DeciaPackage.THRESHOLDBASED_AUTOSCALER:
			return createThresholdbasedAutoscaler();
		case DeciaPackage.RABBIT_MQ:
			return createRabbitMQ();
		case DeciaPackage.ENVIRONMENT:
			return createEnvironment();
		case DeciaPackage.SYSTEM_STAT_METRIC:
			return createSystemStatMetric();
		case DeciaPackage.QUEUE_METRIC:
			return createQueueMetric();
		default:
			throw new IllegalArgumentException("The class '" + eClass.getName() + "' is not a valid classifier");
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object createFromString(EDataType eDataType, String initialValue) {
		switch (eDataType.getClassifierID()) {
		case DeciaPackage.SYSTEM_STAT_METRIC_TYPE:
			return createSystemStatMetricTypeFromString(eDataType, initialValue);
		case DeciaPackage.RABBIT_MQ_TYPE:
			return createRabbitMQTypeFromString(eDataType, initialValue);
		case DeciaPackage.QUEUE_METRIC_TYPE:
			return createQueueMetricTypeFromString(eDataType, initialValue);
		default:
			throw new IllegalArgumentException("The datatype '" + eDataType.getName() + "' is not a valid classifier");
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String convertToString(EDataType eDataType, Object instanceValue) {
		switch (eDataType.getClassifierID()) {
		case DeciaPackage.SYSTEM_STAT_METRIC_TYPE:
			return convertSystemStatMetricTypeToString(eDataType, instanceValue);
		case DeciaPackage.RABBIT_MQ_TYPE:
			return convertRabbitMQTypeToString(eDataType, instanceValue);
		case DeciaPackage.QUEUE_METRIC_TYPE:
			return convertQueueMetricTypeToString(eDataType, instanceValue);
		default:
			throw new IllegalArgumentException("The datatype '" + eDataType.getName() + "' is not a valid classifier");
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public CFConfiguration createCFConfiguration() {
		CFConfigurationImpl cfConfiguration = new CFConfigurationImpl();
		return cfConfiguration;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Cloud createCloud() {
		CloudImpl cloud = new CloudImpl();
		return cloud;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Microservice createMicroservice() {
		MicroserviceImpl microservice = new MicroserviceImpl();
		return microservice;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public RabbitMQBroker createRabbitMQBroker() {
		RabbitMQBrokerImpl rabbitMQBroker = new RabbitMQBrokerImpl();
		return rabbitMQBroker;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ThresholdbasedAutoscaler createThresholdbasedAutoscaler() {
		ThresholdbasedAutoscalerImpl thresholdbasedAutoscaler = new ThresholdbasedAutoscalerImpl();
		return thresholdbasedAutoscaler;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public RabbitMQ createRabbitMQ() {
		RabbitMQImpl rabbitMQ = new RabbitMQImpl();
		return rabbitMQ;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Environment createEnvironment() {
		EnvironmentImpl environment = new EnvironmentImpl();
		return environment;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public SystemStatMetric createSystemStatMetric() {
		SystemStatMetricImpl systemStatMetric = new SystemStatMetricImpl();
		return systemStatMetric;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public QueueMetric createQueueMetric() {
		QueueMetricImpl queueMetric = new QueueMetricImpl();
		return queueMetric;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public SystemStatMetricType createSystemStatMetricTypeFromString(EDataType eDataType, String initialValue) {
		SystemStatMetricType result = SystemStatMetricType.get(initialValue);
		if (result == null)
			throw new IllegalArgumentException(
					"The value '" + initialValue + "' is not a valid enumerator of '" + eDataType.getName() + "'");
		return result;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String convertSystemStatMetricTypeToString(EDataType eDataType, Object instanceValue) {
		return instanceValue == null ? null : instanceValue.toString();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public RabbitMQType createRabbitMQTypeFromString(EDataType eDataType, String initialValue) {
		RabbitMQType result = RabbitMQType.get(initialValue);
		if (result == null)
			throw new IllegalArgumentException(
					"The value '" + initialValue + "' is not a valid enumerator of '" + eDataType.getName() + "'");
		return result;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String convertRabbitMQTypeToString(EDataType eDataType, Object instanceValue) {
		return instanceValue == null ? null : instanceValue.toString();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public QueueMetricType createQueueMetricTypeFromString(EDataType eDataType, String initialValue) {
		QueueMetricType result = QueueMetricType.get(initialValue);
		if (result == null)
			throw new IllegalArgumentException(
					"The value '" + initialValue + "' is not a valid enumerator of '" + eDataType.getName() + "'");
		return result;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String convertQueueMetricTypeToString(EDataType eDataType, Object instanceValue) {
		return instanceValue == null ? null : instanceValue.toString();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public DeciaPackage getDeciaPackage() {
		return (DeciaPackage) getEPackage();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @deprecated
	 * @generated
	 */
	@Deprecated
	public static DeciaPackage getPackage() {
		return DeciaPackage.eINSTANCE;
	}

} //DeciaFactoryImpl
