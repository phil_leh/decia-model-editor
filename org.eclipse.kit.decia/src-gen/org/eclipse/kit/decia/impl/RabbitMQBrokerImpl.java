/**
 */
package org.eclipse.kit.decia.impl;

import java.util.Collection;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;

import org.eclipse.emf.ecore.util.EObjectContainmentEList;
import org.eclipse.emf.ecore.util.InternalEList;

import org.eclipse.kit.decia.DeciaPackage;
import org.eclipse.kit.decia.RabbitMQBroker;
import org.eclipse.kit.decia.SystemStatMetric;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Rabbit MQ Broker</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link org.eclipse.kit.decia.impl.RabbitMQBrokerImpl#getMessageBrokerManagementURL <em>Message Broker Management URL</em>}</li>
 *   <li>{@link org.eclipse.kit.decia.impl.RabbitMQBrokerImpl#getUser <em>User</em>}</li>
 *   <li>{@link org.eclipse.kit.decia.impl.RabbitMQBrokerImpl#getPassword <em>Password</em>}</li>
 *   <li>{@link org.eclipse.kit.decia.impl.RabbitMQBrokerImpl#getName <em>Name</em>}</li>
 *   <li>{@link org.eclipse.kit.decia.impl.RabbitMQBrokerImpl#getContainsSystemstatmetric <em>Contains Systemstatmetric</em>}</li>
 * </ul>
 *
 * @generated
 */
public class RabbitMQBrokerImpl extends AMQPBrokerImpl implements RabbitMQBroker {
	/**
	 * The default value of the '{@link #getMessageBrokerManagementURL() <em>Message Broker Management URL</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getMessageBrokerManagementURL()
	 * @generated
	 * @ordered
	 */
	protected static final String MESSAGE_BROKER_MANAGEMENT_URL_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getMessageBrokerManagementURL() <em>Message Broker Management URL</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getMessageBrokerManagementURL()
	 * @generated
	 * @ordered
	 */
	protected String messageBrokerManagementURL = MESSAGE_BROKER_MANAGEMENT_URL_EDEFAULT;

	/**
	 * The default value of the '{@link #getUser() <em>User</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getUser()
	 * @generated
	 * @ordered
	 */
	protected static final String USER_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getUser() <em>User</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getUser()
	 * @generated
	 * @ordered
	 */
	protected String user = USER_EDEFAULT;

	/**
	 * The default value of the '{@link #getPassword() <em>Password</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getPassword()
	 * @generated
	 * @ordered
	 */
	protected static final String PASSWORD_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getPassword() <em>Password</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getPassword()
	 * @generated
	 * @ordered
	 */
	protected String password = PASSWORD_EDEFAULT;

	/**
	 * The default value of the '{@link #getName() <em>Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getName()
	 * @generated
	 * @ordered
	 */
	protected static final String NAME_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getName() <em>Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getName()
	 * @generated
	 * @ordered
	 */
	protected String name = NAME_EDEFAULT;

	/**
	 * The cached value of the '{@link #getContainsSystemstatmetric() <em>Contains Systemstatmetric</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getContainsSystemstatmetric()
	 * @generated
	 * @ordered
	 */
	protected EList<SystemStatMetric> containsSystemstatmetric;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected RabbitMQBrokerImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return DeciaPackage.Literals.RABBIT_MQ_BROKER;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getMessageBrokerManagementURL() {
		return messageBrokerManagementURL;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setMessageBrokerManagementURL(String newMessageBrokerManagementURL) {
		String oldMessageBrokerManagementURL = messageBrokerManagementURL;
		messageBrokerManagementURL = newMessageBrokerManagementURL;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET,
					DeciaPackage.RABBIT_MQ_BROKER__MESSAGE_BROKER_MANAGEMENT_URL, oldMessageBrokerManagementURL,
					messageBrokerManagementURL));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getUser() {
		return user;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setUser(String newUser) {
		String oldUser = user;
		user = newUser;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, DeciaPackage.RABBIT_MQ_BROKER__USER, oldUser, user));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getPassword() {
		return password;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setPassword(String newPassword) {
		String oldPassword = password;
		password = newPassword;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, DeciaPackage.RABBIT_MQ_BROKER__PASSWORD, oldPassword,
					password));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getName() {
		return name;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setName(String newName) {
		String oldName = name;
		name = newName;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, DeciaPackage.RABBIT_MQ_BROKER__NAME, oldName, name));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<SystemStatMetric> getContainsSystemstatmetric() {
		if (containsSystemstatmetric == null) {
			containsSystemstatmetric = new EObjectContainmentEList<SystemStatMetric>(SystemStatMetric.class, this,
					DeciaPackage.RABBIT_MQ_BROKER__CONTAINS_SYSTEMSTATMETRIC);
		}
		return containsSystemstatmetric;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
		case DeciaPackage.RABBIT_MQ_BROKER__CONTAINS_SYSTEMSTATMETRIC:
			return ((InternalEList<?>) getContainsSystemstatmetric()).basicRemove(otherEnd, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
		case DeciaPackage.RABBIT_MQ_BROKER__MESSAGE_BROKER_MANAGEMENT_URL:
			return getMessageBrokerManagementURL();
		case DeciaPackage.RABBIT_MQ_BROKER__USER:
			return getUser();
		case DeciaPackage.RABBIT_MQ_BROKER__PASSWORD:
			return getPassword();
		case DeciaPackage.RABBIT_MQ_BROKER__NAME:
			return getName();
		case DeciaPackage.RABBIT_MQ_BROKER__CONTAINS_SYSTEMSTATMETRIC:
			return getContainsSystemstatmetric();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
		case DeciaPackage.RABBIT_MQ_BROKER__MESSAGE_BROKER_MANAGEMENT_URL:
			setMessageBrokerManagementURL((String) newValue);
			return;
		case DeciaPackage.RABBIT_MQ_BROKER__USER:
			setUser((String) newValue);
			return;
		case DeciaPackage.RABBIT_MQ_BROKER__PASSWORD:
			setPassword((String) newValue);
			return;
		case DeciaPackage.RABBIT_MQ_BROKER__NAME:
			setName((String) newValue);
			return;
		case DeciaPackage.RABBIT_MQ_BROKER__CONTAINS_SYSTEMSTATMETRIC:
			getContainsSystemstatmetric().clear();
			getContainsSystemstatmetric().addAll((Collection<? extends SystemStatMetric>) newValue);
			return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
		case DeciaPackage.RABBIT_MQ_BROKER__MESSAGE_BROKER_MANAGEMENT_URL:
			setMessageBrokerManagementURL(MESSAGE_BROKER_MANAGEMENT_URL_EDEFAULT);
			return;
		case DeciaPackage.RABBIT_MQ_BROKER__USER:
			setUser(USER_EDEFAULT);
			return;
		case DeciaPackage.RABBIT_MQ_BROKER__PASSWORD:
			setPassword(PASSWORD_EDEFAULT);
			return;
		case DeciaPackage.RABBIT_MQ_BROKER__NAME:
			setName(NAME_EDEFAULT);
			return;
		case DeciaPackage.RABBIT_MQ_BROKER__CONTAINS_SYSTEMSTATMETRIC:
			getContainsSystemstatmetric().clear();
			return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
		case DeciaPackage.RABBIT_MQ_BROKER__MESSAGE_BROKER_MANAGEMENT_URL:
			return MESSAGE_BROKER_MANAGEMENT_URL_EDEFAULT == null ? messageBrokerManagementURL != null
					: !MESSAGE_BROKER_MANAGEMENT_URL_EDEFAULT.equals(messageBrokerManagementURL);
		case DeciaPackage.RABBIT_MQ_BROKER__USER:
			return USER_EDEFAULT == null ? user != null : !USER_EDEFAULT.equals(user);
		case DeciaPackage.RABBIT_MQ_BROKER__PASSWORD:
			return PASSWORD_EDEFAULT == null ? password != null : !PASSWORD_EDEFAULT.equals(password);
		case DeciaPackage.RABBIT_MQ_BROKER__NAME:
			return NAME_EDEFAULT == null ? name != null : !NAME_EDEFAULT.equals(name);
		case DeciaPackage.RABBIT_MQ_BROKER__CONTAINS_SYSTEMSTATMETRIC:
			return containsSystemstatmetric != null && !containsSystemstatmetric.isEmpty();
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy())
			return super.toString();

		StringBuffer result = new StringBuffer(super.toString());
		result.append(" (MessageBrokerManagementURL: ");
		result.append(messageBrokerManagementURL);
		result.append(", User: ");
		result.append(user);
		result.append(", Password: ");
		result.append(password);
		result.append(", name: ");
		result.append(name);
		result.append(')');
		return result.toString();
	}

} //RabbitMQBrokerImpl
