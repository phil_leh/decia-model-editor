/**
 */
package org.eclipse.kit.decia.impl;

import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.ecore.EClass;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

import org.eclipse.kit.decia.CFConfiguration;
import org.eclipse.kit.decia.DeciaPackage;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>CF Configuration</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link org.eclipse.kit.decia.impl.CFConfigurationImpl#getAPIEndpoint <em>API Endpoint</em>}</li>
 *   <li>{@link org.eclipse.kit.decia.impl.CFConfigurationImpl#getUsername <em>Username</em>}</li>
 *   <li>{@link org.eclipse.kit.decia.impl.CFConfigurationImpl#getPassword <em>Password</em>}</li>
 *   <li>{@link org.eclipse.kit.decia.impl.CFConfigurationImpl#getOrgName <em>Org Name</em>}</li>
 *   <li>{@link org.eclipse.kit.decia.impl.CFConfigurationImpl#getSpaceName <em>Space Name</em>}</li>
 * </ul>
 *
 * @generated
 */
public class CFConfigurationImpl extends MinimalEObjectImpl.Container implements CFConfiguration {
	/**
	 * The default value of the '{@link #getAPIEndpoint() <em>API Endpoint</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getAPIEndpoint()
	 * @generated
	 * @ordered
	 */
	protected static final String API_ENDPOINT_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getAPIEndpoint() <em>API Endpoint</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getAPIEndpoint()
	 * @generated
	 * @ordered
	 */
	protected String apiEndpoint = API_ENDPOINT_EDEFAULT;

	/**
	 * The default value of the '{@link #getUsername() <em>Username</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getUsername()
	 * @generated
	 * @ordered
	 */
	protected static final String USERNAME_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getUsername() <em>Username</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getUsername()
	 * @generated
	 * @ordered
	 */
	protected String username = USERNAME_EDEFAULT;

	/**
	 * The default value of the '{@link #getPassword() <em>Password</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getPassword()
	 * @generated
	 * @ordered
	 */
	protected static final String PASSWORD_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getPassword() <em>Password</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getPassword()
	 * @generated
	 * @ordered
	 */
	protected String password = PASSWORD_EDEFAULT;

	/**
	 * The default value of the '{@link #getOrgName() <em>Org Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getOrgName()
	 * @generated
	 * @ordered
	 */
	protected static final String ORG_NAME_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getOrgName() <em>Org Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getOrgName()
	 * @generated
	 * @ordered
	 */
	protected String orgName = ORG_NAME_EDEFAULT;

	/**
	 * The default value of the '{@link #getSpaceName() <em>Space Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getSpaceName()
	 * @generated
	 * @ordered
	 */
	protected static final String SPACE_NAME_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getSpaceName() <em>Space Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getSpaceName()
	 * @generated
	 * @ordered
	 */
	protected String spaceName = SPACE_NAME_EDEFAULT;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected CFConfigurationImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return DeciaPackage.Literals.CF_CONFIGURATION;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getAPIEndpoint() {
		return apiEndpoint;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setAPIEndpoint(String newAPIEndpoint) {
		String oldAPIEndpoint = apiEndpoint;
		apiEndpoint = newAPIEndpoint;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, DeciaPackage.CF_CONFIGURATION__API_ENDPOINT,
					oldAPIEndpoint, apiEndpoint));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getUsername() {
		return username;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setUsername(String newUsername) {
		String oldUsername = username;
		username = newUsername;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, DeciaPackage.CF_CONFIGURATION__USERNAME, oldUsername,
					username));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getPassword() {
		return password;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setPassword(String newPassword) {
		String oldPassword = password;
		password = newPassword;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, DeciaPackage.CF_CONFIGURATION__PASSWORD, oldPassword,
					password));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getOrgName() {
		return orgName;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setOrgName(String newOrgName) {
		String oldOrgName = orgName;
		orgName = newOrgName;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, DeciaPackage.CF_CONFIGURATION__ORG_NAME, oldOrgName,
					orgName));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getSpaceName() {
		return spaceName;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setSpaceName(String newSpaceName) {
		String oldSpaceName = spaceName;
		spaceName = newSpaceName;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, DeciaPackage.CF_CONFIGURATION__SPACE_NAME,
					oldSpaceName, spaceName));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
		case DeciaPackage.CF_CONFIGURATION__API_ENDPOINT:
			return getAPIEndpoint();
		case DeciaPackage.CF_CONFIGURATION__USERNAME:
			return getUsername();
		case DeciaPackage.CF_CONFIGURATION__PASSWORD:
			return getPassword();
		case DeciaPackage.CF_CONFIGURATION__ORG_NAME:
			return getOrgName();
		case DeciaPackage.CF_CONFIGURATION__SPACE_NAME:
			return getSpaceName();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
		case DeciaPackage.CF_CONFIGURATION__API_ENDPOINT:
			setAPIEndpoint((String) newValue);
			return;
		case DeciaPackage.CF_CONFIGURATION__USERNAME:
			setUsername((String) newValue);
			return;
		case DeciaPackage.CF_CONFIGURATION__PASSWORD:
			setPassword((String) newValue);
			return;
		case DeciaPackage.CF_CONFIGURATION__ORG_NAME:
			setOrgName((String) newValue);
			return;
		case DeciaPackage.CF_CONFIGURATION__SPACE_NAME:
			setSpaceName((String) newValue);
			return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
		case DeciaPackage.CF_CONFIGURATION__API_ENDPOINT:
			setAPIEndpoint(API_ENDPOINT_EDEFAULT);
			return;
		case DeciaPackage.CF_CONFIGURATION__USERNAME:
			setUsername(USERNAME_EDEFAULT);
			return;
		case DeciaPackage.CF_CONFIGURATION__PASSWORD:
			setPassword(PASSWORD_EDEFAULT);
			return;
		case DeciaPackage.CF_CONFIGURATION__ORG_NAME:
			setOrgName(ORG_NAME_EDEFAULT);
			return;
		case DeciaPackage.CF_CONFIGURATION__SPACE_NAME:
			setSpaceName(SPACE_NAME_EDEFAULT);
			return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
		case DeciaPackage.CF_CONFIGURATION__API_ENDPOINT:
			return API_ENDPOINT_EDEFAULT == null ? apiEndpoint != null : !API_ENDPOINT_EDEFAULT.equals(apiEndpoint);
		case DeciaPackage.CF_CONFIGURATION__USERNAME:
			return USERNAME_EDEFAULT == null ? username != null : !USERNAME_EDEFAULT.equals(username);
		case DeciaPackage.CF_CONFIGURATION__PASSWORD:
			return PASSWORD_EDEFAULT == null ? password != null : !PASSWORD_EDEFAULT.equals(password);
		case DeciaPackage.CF_CONFIGURATION__ORG_NAME:
			return ORG_NAME_EDEFAULT == null ? orgName != null : !ORG_NAME_EDEFAULT.equals(orgName);
		case DeciaPackage.CF_CONFIGURATION__SPACE_NAME:
			return SPACE_NAME_EDEFAULT == null ? spaceName != null : !SPACE_NAME_EDEFAULT.equals(spaceName);
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy())
			return super.toString();

		StringBuffer result = new StringBuffer(super.toString());
		result.append(" (APIEndpoint: ");
		result.append(apiEndpoint);
		result.append(", username: ");
		result.append(username);
		result.append(", password: ");
		result.append(password);
		result.append(", OrgName: ");
		result.append(orgName);
		result.append(", SpaceName: ");
		result.append(spaceName);
		result.append(')');
		return result.toString();
	}

} //CFConfigurationImpl
