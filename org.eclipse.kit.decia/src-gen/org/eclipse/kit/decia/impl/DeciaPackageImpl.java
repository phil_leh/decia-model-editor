/**
 */
package org.eclipse.kit.decia.impl;

import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EEnum;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EReference;

import org.eclipse.emf.ecore.impl.EPackageImpl;

import org.eclipse.kit.decia.AMQPBroker;
import org.eclipse.kit.decia.Autoscaler;
import org.eclipse.kit.decia.CFConfiguration;
import org.eclipse.kit.decia.Cloud;
import org.eclipse.kit.decia.CloudEntity;
import org.eclipse.kit.decia.DeciaFactory;
import org.eclipse.kit.decia.DeciaPackage;
import org.eclipse.kit.decia.Environment;
import org.eclipse.kit.decia.MessageBroker;
import org.eclipse.kit.decia.MessageQueue;
import org.eclipse.kit.decia.Microservice;
import org.eclipse.kit.decia.QueueMetric;
import org.eclipse.kit.decia.QueueMetricType;
import org.eclipse.kit.decia.RabbitMQ;
import org.eclipse.kit.decia.RabbitMQBroker;
import org.eclipse.kit.decia.RabbitMQType;
import org.eclipse.kit.decia.SystemStatMetric;
import org.eclipse.kit.decia.SystemStatMetricType;
import org.eclipse.kit.decia.ThresholdbasedAutoscaler;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model <b>Package</b>.
 * <!-- end-user-doc -->
 * @generated
 */
public class DeciaPackageImpl extends EPackageImpl implements DeciaPackage {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass cfConfigurationEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass cloudEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass cloudEntityEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass microserviceEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass messageQueueEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass messageBrokerEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass amqpBrokerEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass rabbitMQBrokerEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass autoscalerEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass thresholdbasedAutoscalerEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass rabbitMQEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass environmentEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass systemStatMetricEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass queueMetricEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EEnum systemStatMetricTypeEEnum = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EEnum rabbitMQTypeEEnum = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EEnum queueMetricTypeEEnum = null;

	/**
	 * Creates an instance of the model <b>Package</b>, registered with
	 * {@link org.eclipse.emf.ecore.EPackage.Registry EPackage.Registry} by the package
	 * package URI value.
	 * <p>Note: the correct way to create the package is via the static
	 * factory method {@link #init init()}, which also performs
	 * initialization of the package, or returns the registered package,
	 * if one already exists.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.emf.ecore.EPackage.Registry
	 * @see org.eclipse.kit.decia.DeciaPackage#eNS_URI
	 * @see #init()
	 * @generated
	 */
	private DeciaPackageImpl() {
		super(eNS_URI, DeciaFactory.eINSTANCE);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private static boolean isInited = false;

	/**
	 * Creates, registers, and initializes the <b>Package</b> for this model, and for any others upon which it depends.
	 * 
	 * <p>This method is used to initialize {@link DeciaPackage#eINSTANCE} when that field is accessed.
	 * Clients should not invoke it directly. Instead, they should simply access that field to obtain the package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #eNS_URI
	 * @see #createPackageContents()
	 * @see #initializePackageContents()
	 * @generated
	 */
	public static DeciaPackage init() {
		if (isInited)
			return (DeciaPackage) EPackage.Registry.INSTANCE.getEPackage(DeciaPackage.eNS_URI);

		// Obtain or create and register package
		DeciaPackageImpl theDeciaPackage = (DeciaPackageImpl) (EPackage.Registry.INSTANCE
				.get(eNS_URI) instanceof DeciaPackageImpl ? EPackage.Registry.INSTANCE.get(eNS_URI)
						: new DeciaPackageImpl());

		isInited = true;

		// Create package meta-data objects
		theDeciaPackage.createPackageContents();

		// Initialize created meta-data
		theDeciaPackage.initializePackageContents();

		// Mark meta-data to indicate it can't be changed
		theDeciaPackage.freeze();

		// Update the registry and return the package
		EPackage.Registry.INSTANCE.put(DeciaPackage.eNS_URI, theDeciaPackage);
		return theDeciaPackage;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getCFConfiguration() {
		return cfConfigurationEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getCFConfiguration_APIEndpoint() {
		return (EAttribute) cfConfigurationEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getCFConfiguration_Username() {
		return (EAttribute) cfConfigurationEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getCFConfiguration_Password() {
		return (EAttribute) cfConfigurationEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getCFConfiguration_OrgName() {
		return (EAttribute) cfConfigurationEClass.getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getCFConfiguration_SpaceName() {
		return (EAttribute) cfConfigurationEClass.getEStructuralFeatures().get(4);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getCloud() {
		return cloudEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getCloud_HasConfig() {
		return (EReference) cloudEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getCloud_Contains() {
		return (EReference) cloudEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getCloud_Name() {
		return (EAttribute) cloudEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getCloud_HasEnvironment() {
		return (EReference) cloudEClass.getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getCloudEntity() {
		return cloudEntityEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getCloudEntity_Id() {
		return (EAttribute) cloudEntityEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getMicroservice() {
		return microserviceEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getMicroservice_ProducesMessages() {
		return (EReference) microserviceEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getMicroservice_CfAppName() {
		return (EAttribute) microserviceEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getMicroservice_ConsumesMessages() {
		return (EReference) microserviceEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getMicroservice_ContainsSystemStatMetric() {
		return (EReference) microserviceEClass.getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getMessageQueue() {
		return messageQueueEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getMessageQueue_ContainsQueuemetric() {
		return (EReference) messageQueueEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getMessageBroker() {
		return messageBrokerEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getMessageBroker_Manages() {
		return (EReference) messageBrokerEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getAMQPBroker() {
		return amqpBrokerEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getRabbitMQBroker() {
		return rabbitMQBrokerEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getRabbitMQBroker_MessageBrokerManagementURL() {
		return (EAttribute) rabbitMQBrokerEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getRabbitMQBroker_User() {
		return (EAttribute) rabbitMQBrokerEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getRabbitMQBroker_Password() {
		return (EAttribute) rabbitMQBrokerEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getRabbitMQBroker_Name() {
		return (EAttribute) rabbitMQBrokerEClass.getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getRabbitMQBroker_ContainsSystemstatmetric() {
		return (EReference) rabbitMQBrokerEClass.getEStructuralFeatures().get(4);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getAutoscaler() {
		return autoscalerEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getThresholdbasedAutoscaler() {
		return thresholdbasedAutoscalerEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getThresholdbasedAutoscaler_MinInstances() {
		return (EAttribute) thresholdbasedAutoscalerEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getThresholdbasedAutoscaler_MaxInstances() {
		return (EAttribute) thresholdbasedAutoscalerEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getThresholdbasedAutoscaler_LowerThreshold() {
		return (EAttribute) thresholdbasedAutoscalerEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getThresholdbasedAutoscaler_UpperThreshold() {
		return (EAttribute) thresholdbasedAutoscalerEClass.getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getThresholdbasedAutoscaler_ScalesMicroservice() {
		return (EReference) thresholdbasedAutoscalerEClass.getEStructuralFeatures().get(4);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getThresholdbasedAutoscaler_ScalesMessagequeue() {
		return (EReference) thresholdbasedAutoscalerEClass.getEStructuralFeatures().get(5);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getThresholdbasedAutoscaler_Name() {
		return (EAttribute) thresholdbasedAutoscalerEClass.getEStructuralFeatures().get(6);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getThresholdbasedAutoscaler_ObservesSystemStatMetric() {
		return (EReference) thresholdbasedAutoscalerEClass.getEStructuralFeatures().get(7);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getThresholdbasedAutoscaler_ObservesQueuemetric() {
		return (EReference) thresholdbasedAutoscalerEClass.getEStructuralFeatures().get(8);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getRabbitMQ() {
		return rabbitMQEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getRabbitMQ_MessageQueueName() {
		return (EAttribute) rabbitMQEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getRabbitMQ_Type() {
		return (EAttribute) rabbitMQEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getEnvironment() {
		return environmentEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getEnvironment_Name() {
		return (EAttribute) environmentEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getEnvironment_ProxyHost() {
		return (EAttribute) environmentEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getEnvironment_ProxyPort() {
		return (EAttribute) environmentEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getSystemStatMetric() {
		return systemStatMetricEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getSystemStatMetric_ServiceMetricType() {
		return (EAttribute) systemStatMetricEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getQueueMetric() {
		return queueMetricEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getQueueMetric_QueueMetricType() {
		return (EAttribute) queueMetricEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EEnum getSystemStatMetricType() {
		return systemStatMetricTypeEEnum;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EEnum getRabbitMQType() {
		return rabbitMQTypeEEnum;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EEnum getQueueMetricType() {
		return queueMetricTypeEEnum;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public DeciaFactory getDeciaFactory() {
		return (DeciaFactory) getEFactoryInstance();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private boolean isCreated = false;

	/**
	 * Creates the meta-model objects for the package.  This method is
	 * guarded to have no affect on any invocation but its first.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void createPackageContents() {
		if (isCreated)
			return;
		isCreated = true;

		// Create classes and their features
		cfConfigurationEClass = createEClass(CF_CONFIGURATION);
		createEAttribute(cfConfigurationEClass, CF_CONFIGURATION__API_ENDPOINT);
		createEAttribute(cfConfigurationEClass, CF_CONFIGURATION__USERNAME);
		createEAttribute(cfConfigurationEClass, CF_CONFIGURATION__PASSWORD);
		createEAttribute(cfConfigurationEClass, CF_CONFIGURATION__ORG_NAME);
		createEAttribute(cfConfigurationEClass, CF_CONFIGURATION__SPACE_NAME);

		cloudEClass = createEClass(CLOUD);
		createEReference(cloudEClass, CLOUD__HAS_CONFIG);
		createEReference(cloudEClass, CLOUD__CONTAINS);
		createEAttribute(cloudEClass, CLOUD__NAME);
		createEReference(cloudEClass, CLOUD__HAS_ENVIRONMENT);

		cloudEntityEClass = createEClass(CLOUD_ENTITY);
		createEAttribute(cloudEntityEClass, CLOUD_ENTITY__ID);

		microserviceEClass = createEClass(MICROSERVICE);
		createEReference(microserviceEClass, MICROSERVICE__PRODUCES_MESSAGES);
		createEAttribute(microserviceEClass, MICROSERVICE__CF_APP_NAME);
		createEReference(microserviceEClass, MICROSERVICE__CONSUMES_MESSAGES);
		createEReference(microserviceEClass, MICROSERVICE__CONTAINS_SYSTEM_STAT_METRIC);

		messageQueueEClass = createEClass(MESSAGE_QUEUE);
		createEReference(messageQueueEClass, MESSAGE_QUEUE__CONTAINS_QUEUEMETRIC);

		messageBrokerEClass = createEClass(MESSAGE_BROKER);
		createEReference(messageBrokerEClass, MESSAGE_BROKER__MANAGES);

		amqpBrokerEClass = createEClass(AMQP_BROKER);

		rabbitMQBrokerEClass = createEClass(RABBIT_MQ_BROKER);
		createEAttribute(rabbitMQBrokerEClass, RABBIT_MQ_BROKER__MESSAGE_BROKER_MANAGEMENT_URL);
		createEAttribute(rabbitMQBrokerEClass, RABBIT_MQ_BROKER__USER);
		createEAttribute(rabbitMQBrokerEClass, RABBIT_MQ_BROKER__PASSWORD);
		createEAttribute(rabbitMQBrokerEClass, RABBIT_MQ_BROKER__NAME);
		createEReference(rabbitMQBrokerEClass, RABBIT_MQ_BROKER__CONTAINS_SYSTEMSTATMETRIC);

		autoscalerEClass = createEClass(AUTOSCALER);

		thresholdbasedAutoscalerEClass = createEClass(THRESHOLDBASED_AUTOSCALER);
		createEAttribute(thresholdbasedAutoscalerEClass, THRESHOLDBASED_AUTOSCALER__MIN_INSTANCES);
		createEAttribute(thresholdbasedAutoscalerEClass, THRESHOLDBASED_AUTOSCALER__MAX_INSTANCES);
		createEAttribute(thresholdbasedAutoscalerEClass, THRESHOLDBASED_AUTOSCALER__LOWER_THRESHOLD);
		createEAttribute(thresholdbasedAutoscalerEClass, THRESHOLDBASED_AUTOSCALER__UPPER_THRESHOLD);
		createEReference(thresholdbasedAutoscalerEClass, THRESHOLDBASED_AUTOSCALER__SCALES_MICROSERVICE);
		createEReference(thresholdbasedAutoscalerEClass, THRESHOLDBASED_AUTOSCALER__SCALES_MESSAGEQUEUE);
		createEAttribute(thresholdbasedAutoscalerEClass, THRESHOLDBASED_AUTOSCALER__NAME);
		createEReference(thresholdbasedAutoscalerEClass, THRESHOLDBASED_AUTOSCALER__OBSERVES_SYSTEM_STAT_METRIC);
		createEReference(thresholdbasedAutoscalerEClass, THRESHOLDBASED_AUTOSCALER__OBSERVES_QUEUEMETRIC);

		rabbitMQEClass = createEClass(RABBIT_MQ);
		createEAttribute(rabbitMQEClass, RABBIT_MQ__MESSAGE_QUEUE_NAME);
		createEAttribute(rabbitMQEClass, RABBIT_MQ__TYPE);

		environmentEClass = createEClass(ENVIRONMENT);
		createEAttribute(environmentEClass, ENVIRONMENT__NAME);
		createEAttribute(environmentEClass, ENVIRONMENT__PROXY_HOST);
		createEAttribute(environmentEClass, ENVIRONMENT__PROXY_PORT);

		systemStatMetricEClass = createEClass(SYSTEM_STAT_METRIC);
		createEAttribute(systemStatMetricEClass, SYSTEM_STAT_METRIC__SERVICE_METRIC_TYPE);

		queueMetricEClass = createEClass(QUEUE_METRIC);
		createEAttribute(queueMetricEClass, QUEUE_METRIC__QUEUE_METRIC_TYPE);

		// Create enums
		systemStatMetricTypeEEnum = createEEnum(SYSTEM_STAT_METRIC_TYPE);
		rabbitMQTypeEEnum = createEEnum(RABBIT_MQ_TYPE);
		queueMetricTypeEEnum = createEEnum(QUEUE_METRIC_TYPE);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private boolean isInitialized = false;

	/**
	 * Complete the initialization of the package and its meta-model.  This
	 * method is guarded to have no affect on any invocation but its first.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void initializePackageContents() {
		if (isInitialized)
			return;
		isInitialized = true;

		// Initialize package
		setName(eNAME);
		setNsPrefix(eNS_PREFIX);
		setNsURI(eNS_URI);

		// Create type parameters

		// Set bounds for type parameters

		// Add supertypes to classes
		microserviceEClass.getESuperTypes().add(this.getCloudEntity());
		messageQueueEClass.getESuperTypes().add(this.getAutoscaler());
		messageBrokerEClass.getESuperTypes().add(this.getCloudEntity());
		amqpBrokerEClass.getESuperTypes().add(this.getMessageBroker());
		rabbitMQBrokerEClass.getESuperTypes().add(this.getAMQPBroker());
		autoscalerEClass.getESuperTypes().add(this.getCloudEntity());
		thresholdbasedAutoscalerEClass.getESuperTypes().add(this.getAutoscaler());
		rabbitMQEClass.getESuperTypes().add(this.getMessageQueue());
		systemStatMetricEClass.getESuperTypes().add(this.getCloudEntity());
		queueMetricEClass.getESuperTypes().add(this.getCloudEntity());

		// Initialize classes, features, and operations; add parameters
		initEClass(cfConfigurationEClass, CFConfiguration.class, "CFConfiguration", !IS_ABSTRACT, !IS_INTERFACE,
				IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getCFConfiguration_APIEndpoint(), ecorePackage.getEString(), "APIEndpoint", null, 0, 1,
				CFConfiguration.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE,
				!IS_DERIVED, IS_ORDERED);
		initEAttribute(getCFConfiguration_Username(), ecorePackage.getEString(), "username", null, 0, 1,
				CFConfiguration.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE,
				!IS_DERIVED, IS_ORDERED);
		initEAttribute(getCFConfiguration_Password(), ecorePackage.getEString(), "password", null, 0, 1,
				CFConfiguration.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE,
				!IS_DERIVED, IS_ORDERED);
		initEAttribute(getCFConfiguration_OrgName(), ecorePackage.getEString(), "OrgName", null, 0, 1,
				CFConfiguration.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE,
				!IS_DERIVED, IS_ORDERED);
		initEAttribute(getCFConfiguration_SpaceName(), ecorePackage.getEString(), "SpaceName", null, 0, 1,
				CFConfiguration.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE,
				!IS_DERIVED, IS_ORDERED);

		initEClass(cloudEClass, Cloud.class, "Cloud", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getCloud_HasConfig(), this.getCFConfiguration(), null, "hasConfig", null, 0, 1, Cloud.class,
				!IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE,
				IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getCloud_Contains(), this.getCloudEntity(), null, "contains", null, 0, -1, Cloud.class,
				!IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE,
				IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getCloud_Name(), ecorePackage.getEString(), "name", null, 0, 1, Cloud.class, !IS_TRANSIENT,
				!IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getCloud_HasEnvironment(), this.getEnvironment(), null, "hasEnvironment", null, 0, -1,
				Cloud.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES,
				!IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(cloudEntityEClass, CloudEntity.class, "CloudEntity", IS_ABSTRACT, !IS_INTERFACE,
				IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getCloudEntity_Id(), ecorePackage.getEInt(), "id", null, 0, 1, CloudEntity.class, !IS_TRANSIENT,
				!IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(microserviceEClass, Microservice.class, "Microservice", !IS_ABSTRACT, !IS_INTERFACE,
				IS_GENERATED_INSTANCE_CLASS);
		initEReference(getMicroservice_ProducesMessages(), this.getMessageQueue(), null, "producesMessages", null, 0,
				-1, Microservice.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES,
				!IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getMicroservice_CfAppName(), ecorePackage.getEString(), "CfAppName", null, 0, 1,
				Microservice.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE,
				!IS_DERIVED, IS_ORDERED);
		initEReference(getMicroservice_ConsumesMessages(), this.getMessageQueue(), null, "consumesMessages", null, 0,
				-1, Microservice.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES,
				!IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getMicroservice_ContainsSystemStatMetric(), this.getSystemStatMetric(), null,
				"containsSystemStatMetric", null, 0, -1, Microservice.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE,
				IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(messageQueueEClass, MessageQueue.class, "MessageQueue", IS_ABSTRACT, !IS_INTERFACE,
				IS_GENERATED_INSTANCE_CLASS);
		initEReference(getMessageQueue_ContainsQueuemetric(), this.getQueueMetric(), null, "containsQueuemetric", null,
				0, -1, MessageQueue.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE,
				!IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(messageBrokerEClass, MessageBroker.class, "MessageBroker", IS_ABSTRACT, !IS_INTERFACE,
				IS_GENERATED_INSTANCE_CLASS);
		initEReference(getMessageBroker_Manages(), this.getMessageQueue(), null, "manages", null, 0, -1,
				MessageBroker.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES,
				!IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(amqpBrokerEClass, AMQPBroker.class, "AMQPBroker", IS_ABSTRACT, !IS_INTERFACE,
				IS_GENERATED_INSTANCE_CLASS);

		initEClass(rabbitMQBrokerEClass, RabbitMQBroker.class, "RabbitMQBroker", !IS_ABSTRACT, !IS_INTERFACE,
				IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getRabbitMQBroker_MessageBrokerManagementURL(), ecorePackage.getEString(),
				"MessageBrokerManagementURL", null, 0, 1, RabbitMQBroker.class, !IS_TRANSIENT, !IS_VOLATILE,
				IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getRabbitMQBroker_User(), ecorePackage.getEString(), "User", null, 0, 1, RabbitMQBroker.class,
				!IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getRabbitMQBroker_Password(), ecorePackage.getEString(), "Password", null, 0, 1,
				RabbitMQBroker.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE,
				!IS_DERIVED, IS_ORDERED);
		initEAttribute(getRabbitMQBroker_Name(), ecorePackage.getEString(), "name", null, 0, 1, RabbitMQBroker.class,
				!IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getRabbitMQBroker_ContainsSystemstatmetric(), this.getSystemStatMetric(), null,
				"containsSystemstatmetric", null, 0, -1, RabbitMQBroker.class, !IS_TRANSIENT, !IS_VOLATILE,
				IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(autoscalerEClass, Autoscaler.class, "Autoscaler", IS_ABSTRACT, !IS_INTERFACE,
				IS_GENERATED_INSTANCE_CLASS);

		initEClass(thresholdbasedAutoscalerEClass, ThresholdbasedAutoscaler.class, "ThresholdbasedAutoscaler",
				!IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getThresholdbasedAutoscaler_MinInstances(), ecorePackage.getEString(), "minInstances", null, 0,
				1, ThresholdbasedAutoscaler.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID,
				IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getThresholdbasedAutoscaler_MaxInstances(), ecorePackage.getEString(), "maxInstances", null, 0,
				1, ThresholdbasedAutoscaler.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID,
				IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getThresholdbasedAutoscaler_LowerThreshold(), ecorePackage.getEString(), "lowerThreshold", null,
				0, 1, ThresholdbasedAutoscaler.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE,
				!IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getThresholdbasedAutoscaler_UpperThreshold(), ecorePackage.getEString(), "upperThreshold", null,
				0, 1, ThresholdbasedAutoscaler.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE,
				!IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getThresholdbasedAutoscaler_ScalesMicroservice(), this.getMicroservice(), null,
				"scalesMicroservice", null, 0, -1, ThresholdbasedAutoscaler.class, !IS_TRANSIENT, !IS_VOLATILE,
				IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getThresholdbasedAutoscaler_ScalesMessagequeue(), this.getMessageQueue(), null,
				"scalesMessagequeue", null, 0, -1, ThresholdbasedAutoscaler.class, !IS_TRANSIENT, !IS_VOLATILE,
				IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getThresholdbasedAutoscaler_Name(), ecorePackage.getEString(), "name", null, 0, 1,
				ThresholdbasedAutoscaler.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID,
				IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getThresholdbasedAutoscaler_ObservesSystemStatMetric(), this.getSystemStatMetric(), null,
				"observesSystemStatMetric", null, 0, 1, ThresholdbasedAutoscaler.class, !IS_TRANSIENT, !IS_VOLATILE,
				IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getThresholdbasedAutoscaler_ObservesQueuemetric(), this.getQueueMetric(), null,
				"observesQueuemetric", null, 0, 1, ThresholdbasedAutoscaler.class, !IS_TRANSIENT, !IS_VOLATILE,
				IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(rabbitMQEClass, RabbitMQ.class, "RabbitMQ", !IS_ABSTRACT, !IS_INTERFACE,
				IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getRabbitMQ_MessageQueueName(), ecorePackage.getEString(), "MessageQueueName", null, 0, 1,
				RabbitMQ.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE,
				!IS_DERIVED, IS_ORDERED);
		initEAttribute(getRabbitMQ_Type(), this.getRabbitMQType(), "Type", null, 0, 2, RabbitMQ.class, !IS_TRANSIENT,
				!IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(environmentEClass, Environment.class, "Environment", !IS_ABSTRACT, !IS_INTERFACE,
				IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getEnvironment_Name(), ecorePackage.getEString(), "name", null, 0, 1, Environment.class,
				!IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getEnvironment_ProxyHost(), ecorePackage.getEString(), "proxyHost", null, 0, 1,
				Environment.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE,
				!IS_DERIVED, IS_ORDERED);
		initEAttribute(getEnvironment_ProxyPort(), ecorePackage.getEString(), "proxyPort", null, 0, 1,
				Environment.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE,
				!IS_DERIVED, IS_ORDERED);

		initEClass(systemStatMetricEClass, SystemStatMetric.class, "SystemStatMetric", !IS_ABSTRACT, !IS_INTERFACE,
				IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getSystemStatMetric_ServiceMetricType(), this.getSystemStatMetricType(), "ServiceMetricType",
				null, 0, -1, SystemStatMetric.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID,
				IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(queueMetricEClass, QueueMetric.class, "QueueMetric", !IS_ABSTRACT, !IS_INTERFACE,
				IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getQueueMetric_QueueMetricType(), this.getQueueMetricType(), "QueueMetricType", null, 0, -1,
				QueueMetric.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE,
				!IS_DERIVED, IS_ORDERED);

		// Initialize enums and add enum literals
		initEEnum(systemStatMetricTypeEEnum, SystemStatMetricType.class, "SystemStatMetricType");
		addEEnumLiteral(systemStatMetricTypeEEnum, SystemStatMetricType.CPU_UTILIZATION);

		initEEnum(rabbitMQTypeEEnum, RabbitMQType.class, "RabbitMQType");
		addEEnumLiteral(rabbitMQTypeEEnum, RabbitMQType.DURABLE);
		addEEnumLiteral(rabbitMQTypeEEnum, RabbitMQType.TRANSIENT);

		initEEnum(queueMetricTypeEEnum, QueueMetricType.class, "QueueMetricType");
		addEEnumLiteral(queueMetricTypeEEnum, QueueMetricType.QUEUE_LENGTH);
		addEEnumLiteral(queueMetricTypeEEnum, QueueMetricType.QUEUE_GROWTH);
		addEEnumLiteral(queueMetricTypeEEnum, QueueMetricType.QUEUEING_DELAY);

		// Create resource
		createResource(eNS_URI);
	}

} //DeciaPackageImpl
