/**
 */
package org.eclipse.kit.decia.impl;

import org.eclipse.emf.ecore.EClass;

import org.eclipse.kit.decia.Autoscaler;
import org.eclipse.kit.decia.DeciaPackage;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Autoscaler</b></em>'.
 * <!-- end-user-doc -->
 *
 * @generated
 */
public abstract class AutoscalerImpl extends CloudEntityImpl implements Autoscaler {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected AutoscalerImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return DeciaPackage.Literals.AUTOSCALER;
	}

} //AutoscalerImpl
