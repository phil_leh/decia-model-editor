/**
 */
package org.eclipse.kit.decia.impl;

import java.util.Collection;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;

import org.eclipse.emf.ecore.util.EDataTypeUniqueEList;

import org.eclipse.kit.decia.DeciaPackage;
import org.eclipse.kit.decia.SystemStatMetric;
import org.eclipse.kit.decia.SystemStatMetricType;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>System Stat Metric</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link org.eclipse.kit.decia.impl.SystemStatMetricImpl#getServiceMetricType <em>Service Metric Type</em>}</li>
 * </ul>
 *
 * @generated
 */
public class SystemStatMetricImpl extends CloudEntityImpl implements SystemStatMetric {
	/**
	 * The cached value of the '{@link #getServiceMetricType() <em>Service Metric Type</em>}' attribute list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getServiceMetricType()
	 * @generated
	 * @ordered
	 */
	protected EList<SystemStatMetricType> serviceMetricType;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected SystemStatMetricImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return DeciaPackage.Literals.SYSTEM_STAT_METRIC;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<SystemStatMetricType> getServiceMetricType() {
		if (serviceMetricType == null) {
			serviceMetricType = new EDataTypeUniqueEList<SystemStatMetricType>(SystemStatMetricType.class, this,
					DeciaPackage.SYSTEM_STAT_METRIC__SERVICE_METRIC_TYPE);
		}
		return serviceMetricType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
		case DeciaPackage.SYSTEM_STAT_METRIC__SERVICE_METRIC_TYPE:
			return getServiceMetricType();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
		case DeciaPackage.SYSTEM_STAT_METRIC__SERVICE_METRIC_TYPE:
			getServiceMetricType().clear();
			getServiceMetricType().addAll((Collection<? extends SystemStatMetricType>) newValue);
			return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
		case DeciaPackage.SYSTEM_STAT_METRIC__SERVICE_METRIC_TYPE:
			getServiceMetricType().clear();
			return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
		case DeciaPackage.SYSTEM_STAT_METRIC__SERVICE_METRIC_TYPE:
			return serviceMetricType != null && !serviceMetricType.isEmpty();
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy())
			return super.toString();

		StringBuffer result = new StringBuffer(super.toString());
		result.append(" (ServiceMetricType: ");
		result.append(serviceMetricType);
		result.append(')');
		return result.toString();
	}

} //SystemStatMetricImpl
