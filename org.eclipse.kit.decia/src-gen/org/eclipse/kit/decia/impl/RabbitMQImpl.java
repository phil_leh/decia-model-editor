/**
 */
package org.eclipse.kit.decia.impl;

import java.util.Collection;

import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;

import org.eclipse.emf.ecore.impl.ENotificationImpl;

import org.eclipse.emf.ecore.util.EDataTypeUniqueEList;

import org.eclipse.kit.decia.DeciaPackage;
import org.eclipse.kit.decia.RabbitMQ;
import org.eclipse.kit.decia.RabbitMQType;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Rabbit MQ</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link org.eclipse.kit.decia.impl.RabbitMQImpl#getMessageQueueName <em>Message Queue Name</em>}</li>
 *   <li>{@link org.eclipse.kit.decia.impl.RabbitMQImpl#getType <em>Type</em>}</li>
 * </ul>
 *
 * @generated
 */
public class RabbitMQImpl extends MessageQueueImpl implements RabbitMQ {
	/**
	 * The default value of the '{@link #getMessageQueueName() <em>Message Queue Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getMessageQueueName()
	 * @generated
	 * @ordered
	 */
	protected static final String MESSAGE_QUEUE_NAME_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getMessageQueueName() <em>Message Queue Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getMessageQueueName()
	 * @generated
	 * @ordered
	 */
	protected String messageQueueName = MESSAGE_QUEUE_NAME_EDEFAULT;

	/**
	 * The cached value of the '{@link #getType() <em>Type</em>}' attribute list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getType()
	 * @generated
	 * @ordered
	 */
	protected EList<RabbitMQType> type;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected RabbitMQImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return DeciaPackage.Literals.RABBIT_MQ;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getMessageQueueName() {
		return messageQueueName;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setMessageQueueName(String newMessageQueueName) {
		String oldMessageQueueName = messageQueueName;
		messageQueueName = newMessageQueueName;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, DeciaPackage.RABBIT_MQ__MESSAGE_QUEUE_NAME,
					oldMessageQueueName, messageQueueName));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<RabbitMQType> getType() {
		if (type == null) {
			type = new EDataTypeUniqueEList<RabbitMQType>(RabbitMQType.class, this, DeciaPackage.RABBIT_MQ__TYPE);
		}
		return type;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
		case DeciaPackage.RABBIT_MQ__MESSAGE_QUEUE_NAME:
			return getMessageQueueName();
		case DeciaPackage.RABBIT_MQ__TYPE:
			return getType();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
		case DeciaPackage.RABBIT_MQ__MESSAGE_QUEUE_NAME:
			setMessageQueueName((String) newValue);
			return;
		case DeciaPackage.RABBIT_MQ__TYPE:
			getType().clear();
			getType().addAll((Collection<? extends RabbitMQType>) newValue);
			return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
		case DeciaPackage.RABBIT_MQ__MESSAGE_QUEUE_NAME:
			setMessageQueueName(MESSAGE_QUEUE_NAME_EDEFAULT);
			return;
		case DeciaPackage.RABBIT_MQ__TYPE:
			getType().clear();
			return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
		case DeciaPackage.RABBIT_MQ__MESSAGE_QUEUE_NAME:
			return MESSAGE_QUEUE_NAME_EDEFAULT == null ? messageQueueName != null
					: !MESSAGE_QUEUE_NAME_EDEFAULT.equals(messageQueueName);
		case DeciaPackage.RABBIT_MQ__TYPE:
			return type != null && !type.isEmpty();
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy())
			return super.toString();

		StringBuffer result = new StringBuffer(super.toString());
		result.append(" (MessageQueueName: ");
		result.append(messageQueueName);
		result.append(", Type: ");
		result.append(type);
		result.append(')');
		return result.toString();
	}

} //RabbitMQImpl
