/**
 */
package org.eclipse.kit.decia.impl;

import org.eclipse.emf.ecore.EClass;

import org.eclipse.kit.decia.AMQPBroker;
import org.eclipse.kit.decia.DeciaPackage;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>AMQP Broker</b></em>'.
 * <!-- end-user-doc -->
 *
 * @generated
 */
public abstract class AMQPBrokerImpl extends MessageBrokerImpl implements AMQPBroker {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected AMQPBrokerImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return DeciaPackage.Literals.AMQP_BROKER;
	}

} //AMQPBrokerImpl
