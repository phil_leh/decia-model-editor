/**
 */
package org.eclipse.kit.decia.impl;

import java.util.Collection;

import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.util.EObjectContainmentEList;
import org.eclipse.emf.ecore.util.InternalEList;

import org.eclipse.kit.decia.DeciaPackage;
import org.eclipse.kit.decia.MessageQueue;
import org.eclipse.kit.decia.QueueMetric;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Message Queue</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link org.eclipse.kit.decia.impl.MessageQueueImpl#getContainsQueuemetric <em>Contains Queuemetric</em>}</li>
 * </ul>
 *
 * @generated
 */
public abstract class MessageQueueImpl extends AutoscalerImpl implements MessageQueue {
	/**
	 * The cached value of the '{@link #getContainsQueuemetric() <em>Contains Queuemetric</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getContainsQueuemetric()
	 * @generated
	 * @ordered
	 */
	protected EList<QueueMetric> containsQueuemetric;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected MessageQueueImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return DeciaPackage.Literals.MESSAGE_QUEUE;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<QueueMetric> getContainsQueuemetric() {
		if (containsQueuemetric == null) {
			containsQueuemetric = new EObjectContainmentEList<QueueMetric>(QueueMetric.class, this,
					DeciaPackage.MESSAGE_QUEUE__CONTAINS_QUEUEMETRIC);
		}
		return containsQueuemetric;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
		case DeciaPackage.MESSAGE_QUEUE__CONTAINS_QUEUEMETRIC:
			return ((InternalEList<?>) getContainsQueuemetric()).basicRemove(otherEnd, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
		case DeciaPackage.MESSAGE_QUEUE__CONTAINS_QUEUEMETRIC:
			return getContainsQueuemetric();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
		case DeciaPackage.MESSAGE_QUEUE__CONTAINS_QUEUEMETRIC:
			getContainsQueuemetric().clear();
			getContainsQueuemetric().addAll((Collection<? extends QueueMetric>) newValue);
			return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
		case DeciaPackage.MESSAGE_QUEUE__CONTAINS_QUEUEMETRIC:
			getContainsQueuemetric().clear();
			return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
		case DeciaPackage.MESSAGE_QUEUE__CONTAINS_QUEUEMETRIC:
			return containsQueuemetric != null && !containsQueuemetric.isEmpty();
		}
		return super.eIsSet(featureID);
	}

} //MessageQueueImpl
