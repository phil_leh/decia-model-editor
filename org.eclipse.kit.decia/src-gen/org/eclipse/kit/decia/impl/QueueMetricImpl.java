/**
 */
package org.eclipse.kit.decia.impl;

import java.util.Collection;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;

import org.eclipse.emf.ecore.util.EDataTypeUniqueEList;

import org.eclipse.kit.decia.DeciaPackage;
import org.eclipse.kit.decia.QueueMetric;
import org.eclipse.kit.decia.QueueMetricType;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Queue Metric</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link org.eclipse.kit.decia.impl.QueueMetricImpl#getQueueMetricType <em>Queue Metric Type</em>}</li>
 * </ul>
 *
 * @generated
 */
public class QueueMetricImpl extends CloudEntityImpl implements QueueMetric {
	/**
	 * The cached value of the '{@link #getQueueMetricType() <em>Queue Metric Type</em>}' attribute list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getQueueMetricType()
	 * @generated
	 * @ordered
	 */
	protected EList<QueueMetricType> queueMetricType;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected QueueMetricImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return DeciaPackage.Literals.QUEUE_METRIC;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<QueueMetricType> getQueueMetricType() {
		if (queueMetricType == null) {
			queueMetricType = new EDataTypeUniqueEList<QueueMetricType>(QueueMetricType.class, this,
					DeciaPackage.QUEUE_METRIC__QUEUE_METRIC_TYPE);
		}
		return queueMetricType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
		case DeciaPackage.QUEUE_METRIC__QUEUE_METRIC_TYPE:
			return getQueueMetricType();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
		case DeciaPackage.QUEUE_METRIC__QUEUE_METRIC_TYPE:
			getQueueMetricType().clear();
			getQueueMetricType().addAll((Collection<? extends QueueMetricType>) newValue);
			return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
		case DeciaPackage.QUEUE_METRIC__QUEUE_METRIC_TYPE:
			getQueueMetricType().clear();
			return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
		case DeciaPackage.QUEUE_METRIC__QUEUE_METRIC_TYPE:
			return queueMetricType != null && !queueMetricType.isEmpty();
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy())
			return super.toString();

		StringBuffer result = new StringBuffer(super.toString());
		result.append(" (QueueMetricType: ");
		result.append(queueMetricType);
		result.append(')');
		return result.toString();
	}

} //QueueMetricImpl
