/**
 */
package org.eclipse.kit.decia;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Autoscaler</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see org.eclipse.kit.decia.DeciaPackage#getAutoscaler()
 * @model abstract="true"
 * @generated
 */
public interface Autoscaler extends CloudEntity {
} // Autoscaler
