/**
 */
package org.eclipse.kit.decia;

import org.eclipse.emf.ecore.EFactory;

/**
 * <!-- begin-user-doc -->
 * The <b>Factory</b> for the model.
 * It provides a create method for each non-abstract class of the model.
 * <!-- end-user-doc -->
 * @see org.eclipse.kit.decia.DeciaPackage
 * @generated
 */
public interface DeciaFactory extends EFactory {
	/**
	 * The singleton instance of the factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	DeciaFactory eINSTANCE = org.eclipse.kit.decia.impl.DeciaFactoryImpl.init();

	/**
	 * Returns a new object of class '<em>CF Configuration</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>CF Configuration</em>'.
	 * @generated
	 */
	CFConfiguration createCFConfiguration();

	/**
	 * Returns a new object of class '<em>Cloud</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Cloud</em>'.
	 * @generated
	 */
	Cloud createCloud();

	/**
	 * Returns a new object of class '<em>Microservice</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Microservice</em>'.
	 * @generated
	 */
	Microservice createMicroservice();

	/**
	 * Returns a new object of class '<em>Rabbit MQ Broker</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Rabbit MQ Broker</em>'.
	 * @generated
	 */
	RabbitMQBroker createRabbitMQBroker();

	/**
	 * Returns a new object of class '<em>Thresholdbased Autoscaler</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Thresholdbased Autoscaler</em>'.
	 * @generated
	 */
	ThresholdbasedAutoscaler createThresholdbasedAutoscaler();

	/**
	 * Returns a new object of class '<em>Rabbit MQ</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Rabbit MQ</em>'.
	 * @generated
	 */
	RabbitMQ createRabbitMQ();

	/**
	 * Returns a new object of class '<em>Environment</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Environment</em>'.
	 * @generated
	 */
	Environment createEnvironment();

	/**
	 * Returns a new object of class '<em>System Stat Metric</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>System Stat Metric</em>'.
	 * @generated
	 */
	SystemStatMetric createSystemStatMetric();

	/**
	 * Returns a new object of class '<em>Queue Metric</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Queue Metric</em>'.
	 * @generated
	 */
	QueueMetric createQueueMetric();

	/**
	 * Returns the package supported by this factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the package supported by this factory.
	 * @generated
	 */
	DeciaPackage getDeciaPackage();

} //DeciaFactory
