/**
 */
package org.eclipse.kit.decia;

import org.eclipse.emf.common.util.EList;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Message Queue</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link org.eclipse.kit.decia.MessageQueue#getContainsQueuemetric <em>Contains Queuemetric</em>}</li>
 * </ul>
 *
 * @see org.eclipse.kit.decia.DeciaPackage#getMessageQueue()
 * @model abstract="true"
 * @generated
 */
public interface MessageQueue extends Autoscaler {
	/**
	 * Returns the value of the '<em><b>Contains Queuemetric</b></em>' containment reference list.
	 * The list contents are of type {@link org.eclipse.kit.decia.QueueMetric}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Contains Queuemetric</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Contains Queuemetric</em>' containment reference list.
	 * @see org.eclipse.kit.decia.DeciaPackage#getMessageQueue_ContainsQueuemetric()
	 * @model containment="true"
	 * @generated
	 */
	EList<QueueMetric> getContainsQueuemetric();

} // MessageQueue
