/**
 */
package org.eclipse.kit.decia;

import org.eclipse.emf.common.util.EList;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Thresholdbased Autoscaler</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link org.eclipse.kit.decia.ThresholdbasedAutoscaler#getMinInstances <em>Min Instances</em>}</li>
 *   <li>{@link org.eclipse.kit.decia.ThresholdbasedAutoscaler#getMaxInstances <em>Max Instances</em>}</li>
 *   <li>{@link org.eclipse.kit.decia.ThresholdbasedAutoscaler#getLowerThreshold <em>Lower Threshold</em>}</li>
 *   <li>{@link org.eclipse.kit.decia.ThresholdbasedAutoscaler#getUpperThreshold <em>Upper Threshold</em>}</li>
 *   <li>{@link org.eclipse.kit.decia.ThresholdbasedAutoscaler#getScalesMicroservice <em>Scales Microservice</em>}</li>
 *   <li>{@link org.eclipse.kit.decia.ThresholdbasedAutoscaler#getScalesMessagequeue <em>Scales Messagequeue</em>}</li>
 *   <li>{@link org.eclipse.kit.decia.ThresholdbasedAutoscaler#getName <em>Name</em>}</li>
 *   <li>{@link org.eclipse.kit.decia.ThresholdbasedAutoscaler#getObservesSystemStatMetric <em>Observes System Stat Metric</em>}</li>
 *   <li>{@link org.eclipse.kit.decia.ThresholdbasedAutoscaler#getObservesQueuemetric <em>Observes Queuemetric</em>}</li>
 * </ul>
 *
 * @see org.eclipse.kit.decia.DeciaPackage#getThresholdbasedAutoscaler()
 * @model
 * @generated
 */
public interface ThresholdbasedAutoscaler extends Autoscaler {
	/**
	 * Returns the value of the '<em><b>Min Instances</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Min Instances</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Min Instances</em>' attribute.
	 * @see #setMinInstances(String)
	 * @see org.eclipse.kit.decia.DeciaPackage#getThresholdbasedAutoscaler_MinInstances()
	 * @model
	 * @generated
	 */
	String getMinInstances();

	/**
	 * Sets the value of the '{@link org.eclipse.kit.decia.ThresholdbasedAutoscaler#getMinInstances <em>Min Instances</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Min Instances</em>' attribute.
	 * @see #getMinInstances()
	 * @generated
	 */
	void setMinInstances(String value);

	/**
	 * Returns the value of the '<em><b>Max Instances</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Max Instances</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Max Instances</em>' attribute.
	 * @see #setMaxInstances(String)
	 * @see org.eclipse.kit.decia.DeciaPackage#getThresholdbasedAutoscaler_MaxInstances()
	 * @model
	 * @generated
	 */
	String getMaxInstances();

	/**
	 * Sets the value of the '{@link org.eclipse.kit.decia.ThresholdbasedAutoscaler#getMaxInstances <em>Max Instances</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Max Instances</em>' attribute.
	 * @see #getMaxInstances()
	 * @generated
	 */
	void setMaxInstances(String value);

	/**
	 * Returns the value of the '<em><b>Lower Threshold</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Lower Threshold</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Lower Threshold</em>' attribute.
	 * @see #setLowerThreshold(String)
	 * @see org.eclipse.kit.decia.DeciaPackage#getThresholdbasedAutoscaler_LowerThreshold()
	 * @model
	 * @generated
	 */
	String getLowerThreshold();

	/**
	 * Sets the value of the '{@link org.eclipse.kit.decia.ThresholdbasedAutoscaler#getLowerThreshold <em>Lower Threshold</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Lower Threshold</em>' attribute.
	 * @see #getLowerThreshold()
	 * @generated
	 */
	void setLowerThreshold(String value);

	/**
	 * Returns the value of the '<em><b>Upper Threshold</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Upper Threshold</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Upper Threshold</em>' attribute.
	 * @see #setUpperThreshold(String)
	 * @see org.eclipse.kit.decia.DeciaPackage#getThresholdbasedAutoscaler_UpperThreshold()
	 * @model
	 * @generated
	 */
	String getUpperThreshold();

	/**
	 * Sets the value of the '{@link org.eclipse.kit.decia.ThresholdbasedAutoscaler#getUpperThreshold <em>Upper Threshold</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Upper Threshold</em>' attribute.
	 * @see #getUpperThreshold()
	 * @generated
	 */
	void setUpperThreshold(String value);

	/**
	 * Returns the value of the '<em><b>Scales Microservice</b></em>' reference list.
	 * The list contents are of type {@link org.eclipse.kit.decia.Microservice}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Scales Microservice</em>' reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Scales Microservice</em>' reference list.
	 * @see org.eclipse.kit.decia.DeciaPackage#getThresholdbasedAutoscaler_ScalesMicroservice()
	 * @model
	 * @generated
	 */
	EList<Microservice> getScalesMicroservice();

	/**
	 * Returns the value of the '<em><b>Scales Messagequeue</b></em>' reference list.
	 * The list contents are of type {@link org.eclipse.kit.decia.MessageQueue}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Scales Messagequeue</em>' reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Scales Messagequeue</em>' reference list.
	 * @see org.eclipse.kit.decia.DeciaPackage#getThresholdbasedAutoscaler_ScalesMessagequeue()
	 * @model
	 * @generated
	 */
	EList<MessageQueue> getScalesMessagequeue();

	/**
	 * Returns the value of the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Name</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Name</em>' attribute.
	 * @see #setName(String)
	 * @see org.eclipse.kit.decia.DeciaPackage#getThresholdbasedAutoscaler_Name()
	 * @model
	 * @generated
	 */
	String getName();

	/**
	 * Sets the value of the '{@link org.eclipse.kit.decia.ThresholdbasedAutoscaler#getName <em>Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Name</em>' attribute.
	 * @see #getName()
	 * @generated
	 */
	void setName(String value);

	/**
	 * Returns the value of the '<em><b>Observes System Stat Metric</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Observes System Stat Metric</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Observes System Stat Metric</em>' reference.
	 * @see #setObservesSystemStatMetric(SystemStatMetric)
	 * @see org.eclipse.kit.decia.DeciaPackage#getThresholdbasedAutoscaler_ObservesSystemStatMetric()
	 * @model
	 * @generated
	 */
	SystemStatMetric getObservesSystemStatMetric();

	/**
	 * Sets the value of the '{@link org.eclipse.kit.decia.ThresholdbasedAutoscaler#getObservesSystemStatMetric <em>Observes System Stat Metric</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Observes System Stat Metric</em>' reference.
	 * @see #getObservesSystemStatMetric()
	 * @generated
	 */
	void setObservesSystemStatMetric(SystemStatMetric value);

	/**
	 * Returns the value of the '<em><b>Observes Queuemetric</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Observes Queuemetric</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Observes Queuemetric</em>' reference.
	 * @see #setObservesQueuemetric(QueueMetric)
	 * @see org.eclipse.kit.decia.DeciaPackage#getThresholdbasedAutoscaler_ObservesQueuemetric()
	 * @model
	 * @generated
	 */
	QueueMetric getObservesQueuemetric();

	/**
	 * Sets the value of the '{@link org.eclipse.kit.decia.ThresholdbasedAutoscaler#getObservesQueuemetric <em>Observes Queuemetric</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Observes Queuemetric</em>' reference.
	 * @see #getObservesQueuemetric()
	 * @generated
	 */
	void setObservesQueuemetric(QueueMetric value);

} // ThresholdbasedAutoscaler
