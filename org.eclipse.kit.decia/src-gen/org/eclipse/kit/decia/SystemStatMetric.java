/**
 */
package org.eclipse.kit.decia;

import org.eclipse.emf.common.util.EList;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>System Stat Metric</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link org.eclipse.kit.decia.SystemStatMetric#getServiceMetricType <em>Service Metric Type</em>}</li>
 * </ul>
 *
 * @see org.eclipse.kit.decia.DeciaPackage#getSystemStatMetric()
 * @model
 * @generated
 */
public interface SystemStatMetric extends CloudEntity {
	/**
	 * Returns the value of the '<em><b>Service Metric Type</b></em>' attribute list.
	 * The list contents are of type {@link org.eclipse.kit.decia.SystemStatMetricType}.
	 * The literals are from the enumeration {@link org.eclipse.kit.decia.SystemStatMetricType}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Service Metric Type</em>' attribute list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Service Metric Type</em>' attribute list.
	 * @see org.eclipse.kit.decia.SystemStatMetricType
	 * @see org.eclipse.kit.decia.DeciaPackage#getSystemStatMetric_ServiceMetricType()
	 * @model
	 * @generated
	 */
	EList<SystemStatMetricType> getServiceMetricType();

} // SystemStatMetric
