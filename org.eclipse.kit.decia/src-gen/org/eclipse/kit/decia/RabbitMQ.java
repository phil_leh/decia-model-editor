/**
 */
package org.eclipse.kit.decia;

import org.eclipse.emf.common.util.EList;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Rabbit MQ</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link org.eclipse.kit.decia.RabbitMQ#getMessageQueueName <em>Message Queue Name</em>}</li>
 *   <li>{@link org.eclipse.kit.decia.RabbitMQ#getType <em>Type</em>}</li>
 * </ul>
 *
 * @see org.eclipse.kit.decia.DeciaPackage#getRabbitMQ()
 * @model
 * @generated
 */
public interface RabbitMQ extends MessageQueue {
	/**
	 * Returns the value of the '<em><b>Message Queue Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Message Queue Name</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Message Queue Name</em>' attribute.
	 * @see #setMessageQueueName(String)
	 * @see org.eclipse.kit.decia.DeciaPackage#getRabbitMQ_MessageQueueName()
	 * @model
	 * @generated
	 */
	String getMessageQueueName();

	/**
	 * Sets the value of the '{@link org.eclipse.kit.decia.RabbitMQ#getMessageQueueName <em>Message Queue Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Message Queue Name</em>' attribute.
	 * @see #getMessageQueueName()
	 * @generated
	 */
	void setMessageQueueName(String value);

	/**
	 * Returns the value of the '<em><b>Type</b></em>' attribute list.
	 * The list contents are of type {@link org.eclipse.kit.decia.RabbitMQType}.
	 * The literals are from the enumeration {@link org.eclipse.kit.decia.RabbitMQType}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Type</em>' attribute list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Type</em>' attribute list.
	 * @see org.eclipse.kit.decia.RabbitMQType
	 * @see org.eclipse.kit.decia.DeciaPackage#getRabbitMQ_Type()
	 * @model upper="2"
	 * @generated
	 */
	EList<RabbitMQType> getType();

} // RabbitMQ
