/**
 */
package org.eclipse.kit.decia;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>AMQP Broker</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see org.eclipse.kit.decia.DeciaPackage#getAMQPBroker()
 * @model abstract="true"
 * @generated
 */
public interface AMQPBroker extends MessageBroker {
} // AMQPBroker
