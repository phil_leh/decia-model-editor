/**
 */
package org.eclipse.kit.decia;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Cloud</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link org.eclipse.kit.decia.Cloud#getHasConfig <em>Has Config</em>}</li>
 *   <li>{@link org.eclipse.kit.decia.Cloud#getContains <em>Contains</em>}</li>
 *   <li>{@link org.eclipse.kit.decia.Cloud#getName <em>Name</em>}</li>
 *   <li>{@link org.eclipse.kit.decia.Cloud#getHasEnvironment <em>Has Environment</em>}</li>
 * </ul>
 *
 * @see org.eclipse.kit.decia.DeciaPackage#getCloud()
 * @model
 * @generated
 */
public interface Cloud extends EObject {
	/**
	 * Returns the value of the '<em><b>Has Config</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Has Config</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Has Config</em>' containment reference.
	 * @see #setHasConfig(CFConfiguration)
	 * @see org.eclipse.kit.decia.DeciaPackage#getCloud_HasConfig()
	 * @model containment="true"
	 * @generated
	 */
	CFConfiguration getHasConfig();

	/**
	 * Sets the value of the '{@link org.eclipse.kit.decia.Cloud#getHasConfig <em>Has Config</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Has Config</em>' containment reference.
	 * @see #getHasConfig()
	 * @generated
	 */
	void setHasConfig(CFConfiguration value);

	/**
	 * Returns the value of the '<em><b>Contains</b></em>' containment reference list.
	 * The list contents are of type {@link org.eclipse.kit.decia.CloudEntity}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Contains</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Contains</em>' containment reference list.
	 * @see org.eclipse.kit.decia.DeciaPackage#getCloud_Contains()
	 * @model containment="true"
	 * @generated
	 */
	EList<CloudEntity> getContains();

	/**
	 * Returns the value of the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Name</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Name</em>' attribute.
	 * @see #setName(String)
	 * @see org.eclipse.kit.decia.DeciaPackage#getCloud_Name()
	 * @model
	 * @generated
	 */
	String getName();

	/**
	 * Sets the value of the '{@link org.eclipse.kit.decia.Cloud#getName <em>Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Name</em>' attribute.
	 * @see #getName()
	 * @generated
	 */
	void setName(String value);

	/**
	 * Returns the value of the '<em><b>Has Environment</b></em>' containment reference list.
	 * The list contents are of type {@link org.eclipse.kit.decia.Environment}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Has Environment</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Has Environment</em>' containment reference list.
	 * @see org.eclipse.kit.decia.DeciaPackage#getCloud_HasEnvironment()
	 * @model containment="true"
	 * @generated
	 */
	EList<Environment> getHasEnvironment();

} // Cloud
