/**
 */
package org.eclipse.kit.decia;

import org.eclipse.emf.common.util.EList;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Queue Metric</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link org.eclipse.kit.decia.QueueMetric#getQueueMetricType <em>Queue Metric Type</em>}</li>
 * </ul>
 *
 * @see org.eclipse.kit.decia.DeciaPackage#getQueueMetric()
 * @model
 * @generated
 */
public interface QueueMetric extends CloudEntity {
	/**
	 * Returns the value of the '<em><b>Queue Metric Type</b></em>' attribute list.
	 * The list contents are of type {@link org.eclipse.kit.decia.QueueMetricType}.
	 * The literals are from the enumeration {@link org.eclipse.kit.decia.QueueMetricType}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Queue Metric Type</em>' attribute list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Queue Metric Type</em>' attribute list.
	 * @see org.eclipse.kit.decia.QueueMetricType
	 * @see org.eclipse.kit.decia.DeciaPackage#getQueueMetric_QueueMetricType()
	 * @model
	 * @generated
	 */
	EList<QueueMetricType> getQueueMetricType();

} // QueueMetric
