/**
 */
package org.eclipse.kit.decia;

import org.eclipse.emf.common.util.EList;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Rabbit MQ Broker</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link org.eclipse.kit.decia.RabbitMQBroker#getMessageBrokerManagementURL <em>Message Broker Management URL</em>}</li>
 *   <li>{@link org.eclipse.kit.decia.RabbitMQBroker#getUser <em>User</em>}</li>
 *   <li>{@link org.eclipse.kit.decia.RabbitMQBroker#getPassword <em>Password</em>}</li>
 *   <li>{@link org.eclipse.kit.decia.RabbitMQBroker#getName <em>Name</em>}</li>
 *   <li>{@link org.eclipse.kit.decia.RabbitMQBroker#getContainsSystemstatmetric <em>Contains Systemstatmetric</em>}</li>
 * </ul>
 *
 * @see org.eclipse.kit.decia.DeciaPackage#getRabbitMQBroker()
 * @model
 * @generated
 */
public interface RabbitMQBroker extends AMQPBroker {
	/**
	 * Returns the value of the '<em><b>Message Broker Management URL</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Message Broker Management URL</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Message Broker Management URL</em>' attribute.
	 * @see #setMessageBrokerManagementURL(String)
	 * @see org.eclipse.kit.decia.DeciaPackage#getRabbitMQBroker_MessageBrokerManagementURL()
	 * @model
	 * @generated
	 */
	String getMessageBrokerManagementURL();

	/**
	 * Sets the value of the '{@link org.eclipse.kit.decia.RabbitMQBroker#getMessageBrokerManagementURL <em>Message Broker Management URL</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Message Broker Management URL</em>' attribute.
	 * @see #getMessageBrokerManagementURL()
	 * @generated
	 */
	void setMessageBrokerManagementURL(String value);

	/**
	 * Returns the value of the '<em><b>User</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>User</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>User</em>' attribute.
	 * @see #setUser(String)
	 * @see org.eclipse.kit.decia.DeciaPackage#getRabbitMQBroker_User()
	 * @model
	 * @generated
	 */
	String getUser();

	/**
	 * Sets the value of the '{@link org.eclipse.kit.decia.RabbitMQBroker#getUser <em>User</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>User</em>' attribute.
	 * @see #getUser()
	 * @generated
	 */
	void setUser(String value);

	/**
	 * Returns the value of the '<em><b>Password</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Password</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Password</em>' attribute.
	 * @see #setPassword(String)
	 * @see org.eclipse.kit.decia.DeciaPackage#getRabbitMQBroker_Password()
	 * @model
	 * @generated
	 */
	String getPassword();

	/**
	 * Sets the value of the '{@link org.eclipse.kit.decia.RabbitMQBroker#getPassword <em>Password</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Password</em>' attribute.
	 * @see #getPassword()
	 * @generated
	 */
	void setPassword(String value);

	/**
	 * Returns the value of the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Name</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Name</em>' attribute.
	 * @see #setName(String)
	 * @see org.eclipse.kit.decia.DeciaPackage#getRabbitMQBroker_Name()
	 * @model
	 * @generated
	 */
	String getName();

	/**
	 * Sets the value of the '{@link org.eclipse.kit.decia.RabbitMQBroker#getName <em>Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Name</em>' attribute.
	 * @see #getName()
	 * @generated
	 */
	void setName(String value);

	/**
	 * Returns the value of the '<em><b>Contains Systemstatmetric</b></em>' containment reference list.
	 * The list contents are of type {@link org.eclipse.kit.decia.SystemStatMetric}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Contains Systemstatmetric</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Contains Systemstatmetric</em>' containment reference list.
	 * @see org.eclipse.kit.decia.DeciaPackage#getRabbitMQBroker_ContainsSystemstatmetric()
	 * @model containment="true"
	 * @generated
	 */
	EList<SystemStatMetric> getContainsSystemstatmetric();

} // RabbitMQBroker
