/**
 */
package org.eclipse.kit.decia.util;

import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EPackage;

import org.eclipse.emf.ecore.util.Switch;

import org.eclipse.kit.decia.*;

/**
 * <!-- begin-user-doc -->
 * The <b>Switch</b> for the model's inheritance hierarchy.
 * It supports the call {@link #doSwitch(EObject) doSwitch(object)}
 * to invoke the <code>caseXXX</code> method for each class of the model,
 * starting with the actual class of the object
 * and proceeding up the inheritance hierarchy
 * until a non-null result is returned,
 * which is the result of the switch.
 * <!-- end-user-doc -->
 * @see org.eclipse.kit.decia.DeciaPackage
 * @generated
 */
public class DeciaSwitch<T> extends Switch<T> {
	/**
	 * The cached model package
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected static DeciaPackage modelPackage;

	/**
	 * Creates an instance of the switch.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public DeciaSwitch() {
		if (modelPackage == null) {
			modelPackage = DeciaPackage.eINSTANCE;
		}
	}

	/**
	 * Checks whether this is a switch for the given package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param ePackage the package in question.
	 * @return whether this is a switch for the given package.
	 * @generated
	 */
	@Override
	protected boolean isSwitchFor(EPackage ePackage) {
		return ePackage == modelPackage;
	}

	/**
	 * Calls <code>caseXXX</code> for each class of the model until one returns a non null result; it yields that result.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the first non-null result returned by a <code>caseXXX</code> call.
	 * @generated
	 */
	@Override
	protected T doSwitch(int classifierID, EObject theEObject) {
		switch (classifierID) {
		case DeciaPackage.CF_CONFIGURATION: {
			CFConfiguration cfConfiguration = (CFConfiguration) theEObject;
			T result = caseCFConfiguration(cfConfiguration);
			if (result == null)
				result = defaultCase(theEObject);
			return result;
		}
		case DeciaPackage.CLOUD: {
			Cloud cloud = (Cloud) theEObject;
			T result = caseCloud(cloud);
			if (result == null)
				result = defaultCase(theEObject);
			return result;
		}
		case DeciaPackage.CLOUD_ENTITY: {
			CloudEntity cloudEntity = (CloudEntity) theEObject;
			T result = caseCloudEntity(cloudEntity);
			if (result == null)
				result = defaultCase(theEObject);
			return result;
		}
		case DeciaPackage.MICROSERVICE: {
			Microservice microservice = (Microservice) theEObject;
			T result = caseMicroservice(microservice);
			if (result == null)
				result = caseCloudEntity(microservice);
			if (result == null)
				result = defaultCase(theEObject);
			return result;
		}
		case DeciaPackage.MESSAGE_QUEUE: {
			MessageQueue messageQueue = (MessageQueue) theEObject;
			T result = caseMessageQueue(messageQueue);
			if (result == null)
				result = caseAutoscaler(messageQueue);
			if (result == null)
				result = caseCloudEntity(messageQueue);
			if (result == null)
				result = defaultCase(theEObject);
			return result;
		}
		case DeciaPackage.MESSAGE_BROKER: {
			MessageBroker messageBroker = (MessageBroker) theEObject;
			T result = caseMessageBroker(messageBroker);
			if (result == null)
				result = caseCloudEntity(messageBroker);
			if (result == null)
				result = defaultCase(theEObject);
			return result;
		}
		case DeciaPackage.AMQP_BROKER: {
			AMQPBroker amqpBroker = (AMQPBroker) theEObject;
			T result = caseAMQPBroker(amqpBroker);
			if (result == null)
				result = caseMessageBroker(amqpBroker);
			if (result == null)
				result = caseCloudEntity(amqpBroker);
			if (result == null)
				result = defaultCase(theEObject);
			return result;
		}
		case DeciaPackage.RABBIT_MQ_BROKER: {
			RabbitMQBroker rabbitMQBroker = (RabbitMQBroker) theEObject;
			T result = caseRabbitMQBroker(rabbitMQBroker);
			if (result == null)
				result = caseAMQPBroker(rabbitMQBroker);
			if (result == null)
				result = caseMessageBroker(rabbitMQBroker);
			if (result == null)
				result = caseCloudEntity(rabbitMQBroker);
			if (result == null)
				result = defaultCase(theEObject);
			return result;
		}
		case DeciaPackage.AUTOSCALER: {
			Autoscaler autoscaler = (Autoscaler) theEObject;
			T result = caseAutoscaler(autoscaler);
			if (result == null)
				result = caseCloudEntity(autoscaler);
			if (result == null)
				result = defaultCase(theEObject);
			return result;
		}
		case DeciaPackage.THRESHOLDBASED_AUTOSCALER: {
			ThresholdbasedAutoscaler thresholdbasedAutoscaler = (ThresholdbasedAutoscaler) theEObject;
			T result = caseThresholdbasedAutoscaler(thresholdbasedAutoscaler);
			if (result == null)
				result = caseAutoscaler(thresholdbasedAutoscaler);
			if (result == null)
				result = caseCloudEntity(thresholdbasedAutoscaler);
			if (result == null)
				result = defaultCase(theEObject);
			return result;
		}
		case DeciaPackage.RABBIT_MQ: {
			RabbitMQ rabbitMQ = (RabbitMQ) theEObject;
			T result = caseRabbitMQ(rabbitMQ);
			if (result == null)
				result = caseMessageQueue(rabbitMQ);
			if (result == null)
				result = caseAutoscaler(rabbitMQ);
			if (result == null)
				result = caseCloudEntity(rabbitMQ);
			if (result == null)
				result = defaultCase(theEObject);
			return result;
		}
		case DeciaPackage.ENVIRONMENT: {
			Environment environment = (Environment) theEObject;
			T result = caseEnvironment(environment);
			if (result == null)
				result = defaultCase(theEObject);
			return result;
		}
		case DeciaPackage.SYSTEM_STAT_METRIC: {
			SystemStatMetric systemStatMetric = (SystemStatMetric) theEObject;
			T result = caseSystemStatMetric(systemStatMetric);
			if (result == null)
				result = caseCloudEntity(systemStatMetric);
			if (result == null)
				result = defaultCase(theEObject);
			return result;
		}
		case DeciaPackage.QUEUE_METRIC: {
			QueueMetric queueMetric = (QueueMetric) theEObject;
			T result = caseQueueMetric(queueMetric);
			if (result == null)
				result = caseCloudEntity(queueMetric);
			if (result == null)
				result = defaultCase(theEObject);
			return result;
		}
		default:
			return defaultCase(theEObject);
		}
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>CF Configuration</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>CF Configuration</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseCFConfiguration(CFConfiguration object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Cloud</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Cloud</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseCloud(Cloud object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Cloud Entity</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Cloud Entity</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseCloudEntity(CloudEntity object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Microservice</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Microservice</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseMicroservice(Microservice object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Message Queue</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Message Queue</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseMessageQueue(MessageQueue object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Message Broker</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Message Broker</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseMessageBroker(MessageBroker object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>AMQP Broker</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>AMQP Broker</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseAMQPBroker(AMQPBroker object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Rabbit MQ Broker</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Rabbit MQ Broker</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseRabbitMQBroker(RabbitMQBroker object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Autoscaler</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Autoscaler</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseAutoscaler(Autoscaler object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Thresholdbased Autoscaler</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Thresholdbased Autoscaler</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseThresholdbasedAutoscaler(ThresholdbasedAutoscaler object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Rabbit MQ</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Rabbit MQ</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseRabbitMQ(RabbitMQ object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Environment</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Environment</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseEnvironment(Environment object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>System Stat Metric</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>System Stat Metric</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseSystemStatMetric(SystemStatMetric object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Queue Metric</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Queue Metric</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseQueueMetric(QueueMetric object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>EObject</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch, but this is the last case anyway.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>EObject</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject)
	 * @generated
	 */
	@Override
	public T defaultCase(EObject object) {
		return null;
	}

} //DeciaSwitch
