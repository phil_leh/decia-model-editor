/**
 */
package org.eclipse.kit.decia;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>CF Configuration</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link org.eclipse.kit.decia.CFConfiguration#getAPIEndpoint <em>API Endpoint</em>}</li>
 *   <li>{@link org.eclipse.kit.decia.CFConfiguration#getUsername <em>Username</em>}</li>
 *   <li>{@link org.eclipse.kit.decia.CFConfiguration#getPassword <em>Password</em>}</li>
 *   <li>{@link org.eclipse.kit.decia.CFConfiguration#getOrgName <em>Org Name</em>}</li>
 *   <li>{@link org.eclipse.kit.decia.CFConfiguration#getSpaceName <em>Space Name</em>}</li>
 * </ul>
 *
 * @see org.eclipse.kit.decia.DeciaPackage#getCFConfiguration()
 * @model
 * @generated
 */
public interface CFConfiguration extends EObject {
	/**
	 * Returns the value of the '<em><b>API Endpoint</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>API Endpoint</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>API Endpoint</em>' attribute.
	 * @see #setAPIEndpoint(String)
	 * @see org.eclipse.kit.decia.DeciaPackage#getCFConfiguration_APIEndpoint()
	 * @model
	 * @generated
	 */
	String getAPIEndpoint();

	/**
	 * Sets the value of the '{@link org.eclipse.kit.decia.CFConfiguration#getAPIEndpoint <em>API Endpoint</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>API Endpoint</em>' attribute.
	 * @see #getAPIEndpoint()
	 * @generated
	 */
	void setAPIEndpoint(String value);

	/**
	 * Returns the value of the '<em><b>Username</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Username</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Username</em>' attribute.
	 * @see #setUsername(String)
	 * @see org.eclipse.kit.decia.DeciaPackage#getCFConfiguration_Username()
	 * @model
	 * @generated
	 */
	String getUsername();

	/**
	 * Sets the value of the '{@link org.eclipse.kit.decia.CFConfiguration#getUsername <em>Username</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Username</em>' attribute.
	 * @see #getUsername()
	 * @generated
	 */
	void setUsername(String value);

	/**
	 * Returns the value of the '<em><b>Password</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Password</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Password</em>' attribute.
	 * @see #setPassword(String)
	 * @see org.eclipse.kit.decia.DeciaPackage#getCFConfiguration_Password()
	 * @model
	 * @generated
	 */
	String getPassword();

	/**
	 * Sets the value of the '{@link org.eclipse.kit.decia.CFConfiguration#getPassword <em>Password</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Password</em>' attribute.
	 * @see #getPassword()
	 * @generated
	 */
	void setPassword(String value);

	/**
	 * Returns the value of the '<em><b>Org Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Org Name</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Org Name</em>' attribute.
	 * @see #setOrgName(String)
	 * @see org.eclipse.kit.decia.DeciaPackage#getCFConfiguration_OrgName()
	 * @model
	 * @generated
	 */
	String getOrgName();

	/**
	 * Sets the value of the '{@link org.eclipse.kit.decia.CFConfiguration#getOrgName <em>Org Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Org Name</em>' attribute.
	 * @see #getOrgName()
	 * @generated
	 */
	void setOrgName(String value);

	/**
	 * Returns the value of the '<em><b>Space Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Space Name</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Space Name</em>' attribute.
	 * @see #setSpaceName(String)
	 * @see org.eclipse.kit.decia.DeciaPackage#getCFConfiguration_SpaceName()
	 * @model
	 * @generated
	 */
	String getSpaceName();

	/**
	 * Sets the value of the '{@link org.eclipse.kit.decia.CFConfiguration#getSpaceName <em>Space Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Space Name</em>' attribute.
	 * @see #getSpaceName()
	 * @generated
	 */
	void setSpaceName(String value);

} // CFConfiguration
