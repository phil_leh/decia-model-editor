/**
 */
package org.eclipse.kit.decia;

import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EEnum;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EReference;

/**
 * <!-- begin-user-doc -->
 * The <b>Package</b> for the model.
 * It contains accessors for the meta objects to represent
 * <ul>
 *   <li>each class,</li>
 *   <li>each feature of each class,</li>
 *   <li>each operation of each class,</li>
 *   <li>each enum,</li>
 *   <li>and each data type</li>
 * </ul>
 * <!-- end-user-doc -->
 * @see org.eclipse.kit.decia.DeciaFactory
 * @model kind="package"
 * @generated
 */
public interface DeciaPackage extends EPackage {
	/**
	 * The package name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNAME = "decia";

	/**
	 * The package namespace URI.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNS_URI = "http://www.eclipse.org/edu/kit/decia";

	/**
	 * The package namespace name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNS_PREFIX = "decia";

	/**
	 * The singleton instance of the package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	DeciaPackage eINSTANCE = org.eclipse.kit.decia.impl.DeciaPackageImpl.init();

	/**
	 * The meta object id for the '{@link org.eclipse.kit.decia.impl.CFConfigurationImpl <em>CF Configuration</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.kit.decia.impl.CFConfigurationImpl
	 * @see org.eclipse.kit.decia.impl.DeciaPackageImpl#getCFConfiguration()
	 * @generated
	 */
	int CF_CONFIGURATION = 0;

	/**
	 * The feature id for the '<em><b>API Endpoint</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CF_CONFIGURATION__API_ENDPOINT = 0;

	/**
	 * The feature id for the '<em><b>Username</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CF_CONFIGURATION__USERNAME = 1;

	/**
	 * The feature id for the '<em><b>Password</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CF_CONFIGURATION__PASSWORD = 2;

	/**
	 * The feature id for the '<em><b>Org Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CF_CONFIGURATION__ORG_NAME = 3;

	/**
	 * The feature id for the '<em><b>Space Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CF_CONFIGURATION__SPACE_NAME = 4;

	/**
	 * The number of structural features of the '<em>CF Configuration</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CF_CONFIGURATION_FEATURE_COUNT = 5;

	/**
	 * The number of operations of the '<em>CF Configuration</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CF_CONFIGURATION_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link org.eclipse.kit.decia.impl.CloudImpl <em>Cloud</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.kit.decia.impl.CloudImpl
	 * @see org.eclipse.kit.decia.impl.DeciaPackageImpl#getCloud()
	 * @generated
	 */
	int CLOUD = 1;

	/**
	 * The feature id for the '<em><b>Has Config</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CLOUD__HAS_CONFIG = 0;

	/**
	 * The feature id for the '<em><b>Contains</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CLOUD__CONTAINS = 1;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CLOUD__NAME = 2;

	/**
	 * The feature id for the '<em><b>Has Environment</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CLOUD__HAS_ENVIRONMENT = 3;

	/**
	 * The number of structural features of the '<em>Cloud</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CLOUD_FEATURE_COUNT = 4;

	/**
	 * The number of operations of the '<em>Cloud</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CLOUD_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link org.eclipse.kit.decia.impl.CloudEntityImpl <em>Cloud Entity</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.kit.decia.impl.CloudEntityImpl
	 * @see org.eclipse.kit.decia.impl.DeciaPackageImpl#getCloudEntity()
	 * @generated
	 */
	int CLOUD_ENTITY = 2;

	/**
	 * The feature id for the '<em><b>Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CLOUD_ENTITY__ID = 0;

	/**
	 * The number of structural features of the '<em>Cloud Entity</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CLOUD_ENTITY_FEATURE_COUNT = 1;

	/**
	 * The number of operations of the '<em>Cloud Entity</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CLOUD_ENTITY_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link org.eclipse.kit.decia.impl.MicroserviceImpl <em>Microservice</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.kit.decia.impl.MicroserviceImpl
	 * @see org.eclipse.kit.decia.impl.DeciaPackageImpl#getMicroservice()
	 * @generated
	 */
	int MICROSERVICE = 3;

	/**
	 * The feature id for the '<em><b>Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MICROSERVICE__ID = CLOUD_ENTITY__ID;

	/**
	 * The feature id for the '<em><b>Produces Messages</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MICROSERVICE__PRODUCES_MESSAGES = CLOUD_ENTITY_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Cf App Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MICROSERVICE__CF_APP_NAME = CLOUD_ENTITY_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>Consumes Messages</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MICROSERVICE__CONSUMES_MESSAGES = CLOUD_ENTITY_FEATURE_COUNT + 2;

	/**
	 * The feature id for the '<em><b>Contains System Stat Metric</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MICROSERVICE__CONTAINS_SYSTEM_STAT_METRIC = CLOUD_ENTITY_FEATURE_COUNT + 3;

	/**
	 * The number of structural features of the '<em>Microservice</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MICROSERVICE_FEATURE_COUNT = CLOUD_ENTITY_FEATURE_COUNT + 4;

	/**
	 * The number of operations of the '<em>Microservice</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MICROSERVICE_OPERATION_COUNT = CLOUD_ENTITY_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link org.eclipse.kit.decia.impl.AutoscalerImpl <em>Autoscaler</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.kit.decia.impl.AutoscalerImpl
	 * @see org.eclipse.kit.decia.impl.DeciaPackageImpl#getAutoscaler()
	 * @generated
	 */
	int AUTOSCALER = 8;

	/**
	 * The feature id for the '<em><b>Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AUTOSCALER__ID = CLOUD_ENTITY__ID;

	/**
	 * The number of structural features of the '<em>Autoscaler</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AUTOSCALER_FEATURE_COUNT = CLOUD_ENTITY_FEATURE_COUNT + 0;

	/**
	 * The number of operations of the '<em>Autoscaler</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AUTOSCALER_OPERATION_COUNT = CLOUD_ENTITY_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link org.eclipse.kit.decia.impl.MessageQueueImpl <em>Message Queue</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.kit.decia.impl.MessageQueueImpl
	 * @see org.eclipse.kit.decia.impl.DeciaPackageImpl#getMessageQueue()
	 * @generated
	 */
	int MESSAGE_QUEUE = 4;

	/**
	 * The feature id for the '<em><b>Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MESSAGE_QUEUE__ID = AUTOSCALER__ID;

	/**
	 * The feature id for the '<em><b>Contains Queuemetric</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MESSAGE_QUEUE__CONTAINS_QUEUEMETRIC = AUTOSCALER_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>Message Queue</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MESSAGE_QUEUE_FEATURE_COUNT = AUTOSCALER_FEATURE_COUNT + 1;

	/**
	 * The number of operations of the '<em>Message Queue</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MESSAGE_QUEUE_OPERATION_COUNT = AUTOSCALER_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link org.eclipse.kit.decia.impl.MessageBrokerImpl <em>Message Broker</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.kit.decia.impl.MessageBrokerImpl
	 * @see org.eclipse.kit.decia.impl.DeciaPackageImpl#getMessageBroker()
	 * @generated
	 */
	int MESSAGE_BROKER = 5;

	/**
	 * The feature id for the '<em><b>Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MESSAGE_BROKER__ID = CLOUD_ENTITY__ID;

	/**
	 * The feature id for the '<em><b>Manages</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MESSAGE_BROKER__MANAGES = CLOUD_ENTITY_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>Message Broker</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MESSAGE_BROKER_FEATURE_COUNT = CLOUD_ENTITY_FEATURE_COUNT + 1;

	/**
	 * The number of operations of the '<em>Message Broker</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MESSAGE_BROKER_OPERATION_COUNT = CLOUD_ENTITY_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link org.eclipse.kit.decia.impl.AMQPBrokerImpl <em>AMQP Broker</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.kit.decia.impl.AMQPBrokerImpl
	 * @see org.eclipse.kit.decia.impl.DeciaPackageImpl#getAMQPBroker()
	 * @generated
	 */
	int AMQP_BROKER = 6;

	/**
	 * The feature id for the '<em><b>Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AMQP_BROKER__ID = MESSAGE_BROKER__ID;

	/**
	 * The feature id for the '<em><b>Manages</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AMQP_BROKER__MANAGES = MESSAGE_BROKER__MANAGES;

	/**
	 * The number of structural features of the '<em>AMQP Broker</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AMQP_BROKER_FEATURE_COUNT = MESSAGE_BROKER_FEATURE_COUNT + 0;

	/**
	 * The number of operations of the '<em>AMQP Broker</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AMQP_BROKER_OPERATION_COUNT = MESSAGE_BROKER_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link org.eclipse.kit.decia.impl.RabbitMQBrokerImpl <em>Rabbit MQ Broker</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.kit.decia.impl.RabbitMQBrokerImpl
	 * @see org.eclipse.kit.decia.impl.DeciaPackageImpl#getRabbitMQBroker()
	 * @generated
	 */
	int RABBIT_MQ_BROKER = 7;

	/**
	 * The feature id for the '<em><b>Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RABBIT_MQ_BROKER__ID = AMQP_BROKER__ID;

	/**
	 * The feature id for the '<em><b>Manages</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RABBIT_MQ_BROKER__MANAGES = AMQP_BROKER__MANAGES;

	/**
	 * The feature id for the '<em><b>Message Broker Management URL</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RABBIT_MQ_BROKER__MESSAGE_BROKER_MANAGEMENT_URL = AMQP_BROKER_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>User</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RABBIT_MQ_BROKER__USER = AMQP_BROKER_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>Password</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RABBIT_MQ_BROKER__PASSWORD = AMQP_BROKER_FEATURE_COUNT + 2;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RABBIT_MQ_BROKER__NAME = AMQP_BROKER_FEATURE_COUNT + 3;

	/**
	 * The feature id for the '<em><b>Contains Systemstatmetric</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RABBIT_MQ_BROKER__CONTAINS_SYSTEMSTATMETRIC = AMQP_BROKER_FEATURE_COUNT + 4;

	/**
	 * The number of structural features of the '<em>Rabbit MQ Broker</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RABBIT_MQ_BROKER_FEATURE_COUNT = AMQP_BROKER_FEATURE_COUNT + 5;

	/**
	 * The number of operations of the '<em>Rabbit MQ Broker</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RABBIT_MQ_BROKER_OPERATION_COUNT = AMQP_BROKER_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link org.eclipse.kit.decia.impl.ThresholdbasedAutoscalerImpl <em>Thresholdbased Autoscaler</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.kit.decia.impl.ThresholdbasedAutoscalerImpl
	 * @see org.eclipse.kit.decia.impl.DeciaPackageImpl#getThresholdbasedAutoscaler()
	 * @generated
	 */
	int THRESHOLDBASED_AUTOSCALER = 9;

	/**
	 * The feature id for the '<em><b>Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int THRESHOLDBASED_AUTOSCALER__ID = AUTOSCALER__ID;

	/**
	 * The feature id for the '<em><b>Min Instances</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int THRESHOLDBASED_AUTOSCALER__MIN_INSTANCES = AUTOSCALER_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Max Instances</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int THRESHOLDBASED_AUTOSCALER__MAX_INSTANCES = AUTOSCALER_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>Lower Threshold</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int THRESHOLDBASED_AUTOSCALER__LOWER_THRESHOLD = AUTOSCALER_FEATURE_COUNT + 2;

	/**
	 * The feature id for the '<em><b>Upper Threshold</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int THRESHOLDBASED_AUTOSCALER__UPPER_THRESHOLD = AUTOSCALER_FEATURE_COUNT + 3;

	/**
	 * The feature id for the '<em><b>Scales Microservice</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int THRESHOLDBASED_AUTOSCALER__SCALES_MICROSERVICE = AUTOSCALER_FEATURE_COUNT + 4;

	/**
	 * The feature id for the '<em><b>Scales Messagequeue</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int THRESHOLDBASED_AUTOSCALER__SCALES_MESSAGEQUEUE = AUTOSCALER_FEATURE_COUNT + 5;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int THRESHOLDBASED_AUTOSCALER__NAME = AUTOSCALER_FEATURE_COUNT + 6;

	/**
	 * The feature id for the '<em><b>Observes System Stat Metric</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int THRESHOLDBASED_AUTOSCALER__OBSERVES_SYSTEM_STAT_METRIC = AUTOSCALER_FEATURE_COUNT + 7;

	/**
	 * The feature id for the '<em><b>Observes Queuemetric</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int THRESHOLDBASED_AUTOSCALER__OBSERVES_QUEUEMETRIC = AUTOSCALER_FEATURE_COUNT + 8;

	/**
	 * The number of structural features of the '<em>Thresholdbased Autoscaler</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int THRESHOLDBASED_AUTOSCALER_FEATURE_COUNT = AUTOSCALER_FEATURE_COUNT + 9;

	/**
	 * The number of operations of the '<em>Thresholdbased Autoscaler</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int THRESHOLDBASED_AUTOSCALER_OPERATION_COUNT = AUTOSCALER_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link org.eclipse.kit.decia.impl.RabbitMQImpl <em>Rabbit MQ</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.kit.decia.impl.RabbitMQImpl
	 * @see org.eclipse.kit.decia.impl.DeciaPackageImpl#getRabbitMQ()
	 * @generated
	 */
	int RABBIT_MQ = 10;

	/**
	 * The feature id for the '<em><b>Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RABBIT_MQ__ID = MESSAGE_QUEUE__ID;

	/**
	 * The feature id for the '<em><b>Contains Queuemetric</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RABBIT_MQ__CONTAINS_QUEUEMETRIC = MESSAGE_QUEUE__CONTAINS_QUEUEMETRIC;

	/**
	 * The feature id for the '<em><b>Message Queue Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RABBIT_MQ__MESSAGE_QUEUE_NAME = MESSAGE_QUEUE_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Type</b></em>' attribute list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RABBIT_MQ__TYPE = MESSAGE_QUEUE_FEATURE_COUNT + 1;

	/**
	 * The number of structural features of the '<em>Rabbit MQ</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RABBIT_MQ_FEATURE_COUNT = MESSAGE_QUEUE_FEATURE_COUNT + 2;

	/**
	 * The number of operations of the '<em>Rabbit MQ</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RABBIT_MQ_OPERATION_COUNT = MESSAGE_QUEUE_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link org.eclipse.kit.decia.impl.EnvironmentImpl <em>Environment</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.kit.decia.impl.EnvironmentImpl
	 * @see org.eclipse.kit.decia.impl.DeciaPackageImpl#getEnvironment()
	 * @generated
	 */
	int ENVIRONMENT = 11;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ENVIRONMENT__NAME = 0;

	/**
	 * The feature id for the '<em><b>Proxy Host</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ENVIRONMENT__PROXY_HOST = 1;

	/**
	 * The feature id for the '<em><b>Proxy Port</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ENVIRONMENT__PROXY_PORT = 2;

	/**
	 * The number of structural features of the '<em>Environment</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ENVIRONMENT_FEATURE_COUNT = 3;

	/**
	 * The number of operations of the '<em>Environment</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ENVIRONMENT_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link org.eclipse.kit.decia.impl.SystemStatMetricImpl <em>System Stat Metric</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.kit.decia.impl.SystemStatMetricImpl
	 * @see org.eclipse.kit.decia.impl.DeciaPackageImpl#getSystemStatMetric()
	 * @generated
	 */
	int SYSTEM_STAT_METRIC = 12;

	/**
	 * The feature id for the '<em><b>Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SYSTEM_STAT_METRIC__ID = CLOUD_ENTITY__ID;

	/**
	 * The feature id for the '<em><b>Service Metric Type</b></em>' attribute list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SYSTEM_STAT_METRIC__SERVICE_METRIC_TYPE = CLOUD_ENTITY_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>System Stat Metric</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SYSTEM_STAT_METRIC_FEATURE_COUNT = CLOUD_ENTITY_FEATURE_COUNT + 1;

	/**
	 * The number of operations of the '<em>System Stat Metric</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SYSTEM_STAT_METRIC_OPERATION_COUNT = CLOUD_ENTITY_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link org.eclipse.kit.decia.impl.QueueMetricImpl <em>Queue Metric</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.kit.decia.impl.QueueMetricImpl
	 * @see org.eclipse.kit.decia.impl.DeciaPackageImpl#getQueueMetric()
	 * @generated
	 */
	int QUEUE_METRIC = 13;

	/**
	 * The feature id for the '<em><b>Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int QUEUE_METRIC__ID = CLOUD_ENTITY__ID;

	/**
	 * The feature id for the '<em><b>Queue Metric Type</b></em>' attribute list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int QUEUE_METRIC__QUEUE_METRIC_TYPE = CLOUD_ENTITY_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>Queue Metric</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int QUEUE_METRIC_FEATURE_COUNT = CLOUD_ENTITY_FEATURE_COUNT + 1;

	/**
	 * The number of operations of the '<em>Queue Metric</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int QUEUE_METRIC_OPERATION_COUNT = CLOUD_ENTITY_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link org.eclipse.kit.decia.SystemStatMetricType <em>System Stat Metric Type</em>}' enum.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.kit.decia.SystemStatMetricType
	 * @see org.eclipse.kit.decia.impl.DeciaPackageImpl#getSystemStatMetricType()
	 * @generated
	 */
	int SYSTEM_STAT_METRIC_TYPE = 14;

	/**
	 * The meta object id for the '{@link org.eclipse.kit.decia.RabbitMQType <em>Rabbit MQ Type</em>}' enum.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.kit.decia.RabbitMQType
	 * @see org.eclipse.kit.decia.impl.DeciaPackageImpl#getRabbitMQType()
	 * @generated
	 */
	int RABBIT_MQ_TYPE = 15;

	/**
	 * The meta object id for the '{@link org.eclipse.kit.decia.QueueMetricType <em>Queue Metric Type</em>}' enum.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.kit.decia.QueueMetricType
	 * @see org.eclipse.kit.decia.impl.DeciaPackageImpl#getQueueMetricType()
	 * @generated
	 */
	int QUEUE_METRIC_TYPE = 16;

	/**
	 * Returns the meta object for class '{@link org.eclipse.kit.decia.CFConfiguration <em>CF Configuration</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>CF Configuration</em>'.
	 * @see org.eclipse.kit.decia.CFConfiguration
	 * @generated
	 */
	EClass getCFConfiguration();

	/**
	 * Returns the meta object for the attribute '{@link org.eclipse.kit.decia.CFConfiguration#getAPIEndpoint <em>API Endpoint</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>API Endpoint</em>'.
	 * @see org.eclipse.kit.decia.CFConfiguration#getAPIEndpoint()
	 * @see #getCFConfiguration()
	 * @generated
	 */
	EAttribute getCFConfiguration_APIEndpoint();

	/**
	 * Returns the meta object for the attribute '{@link org.eclipse.kit.decia.CFConfiguration#getUsername <em>Username</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Username</em>'.
	 * @see org.eclipse.kit.decia.CFConfiguration#getUsername()
	 * @see #getCFConfiguration()
	 * @generated
	 */
	EAttribute getCFConfiguration_Username();

	/**
	 * Returns the meta object for the attribute '{@link org.eclipse.kit.decia.CFConfiguration#getPassword <em>Password</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Password</em>'.
	 * @see org.eclipse.kit.decia.CFConfiguration#getPassword()
	 * @see #getCFConfiguration()
	 * @generated
	 */
	EAttribute getCFConfiguration_Password();

	/**
	 * Returns the meta object for the attribute '{@link org.eclipse.kit.decia.CFConfiguration#getOrgName <em>Org Name</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Org Name</em>'.
	 * @see org.eclipse.kit.decia.CFConfiguration#getOrgName()
	 * @see #getCFConfiguration()
	 * @generated
	 */
	EAttribute getCFConfiguration_OrgName();

	/**
	 * Returns the meta object for the attribute '{@link org.eclipse.kit.decia.CFConfiguration#getSpaceName <em>Space Name</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Space Name</em>'.
	 * @see org.eclipse.kit.decia.CFConfiguration#getSpaceName()
	 * @see #getCFConfiguration()
	 * @generated
	 */
	EAttribute getCFConfiguration_SpaceName();

	/**
	 * Returns the meta object for class '{@link org.eclipse.kit.decia.Cloud <em>Cloud</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Cloud</em>'.
	 * @see org.eclipse.kit.decia.Cloud
	 * @generated
	 */
	EClass getCloud();

	/**
	 * Returns the meta object for the containment reference '{@link org.eclipse.kit.decia.Cloud#getHasConfig <em>Has Config</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Has Config</em>'.
	 * @see org.eclipse.kit.decia.Cloud#getHasConfig()
	 * @see #getCloud()
	 * @generated
	 */
	EReference getCloud_HasConfig();

	/**
	 * Returns the meta object for the containment reference list '{@link org.eclipse.kit.decia.Cloud#getContains <em>Contains</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Contains</em>'.
	 * @see org.eclipse.kit.decia.Cloud#getContains()
	 * @see #getCloud()
	 * @generated
	 */
	EReference getCloud_Contains();

	/**
	 * Returns the meta object for the attribute '{@link org.eclipse.kit.decia.Cloud#getName <em>Name</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Name</em>'.
	 * @see org.eclipse.kit.decia.Cloud#getName()
	 * @see #getCloud()
	 * @generated
	 */
	EAttribute getCloud_Name();

	/**
	 * Returns the meta object for the containment reference list '{@link org.eclipse.kit.decia.Cloud#getHasEnvironment <em>Has Environment</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Has Environment</em>'.
	 * @see org.eclipse.kit.decia.Cloud#getHasEnvironment()
	 * @see #getCloud()
	 * @generated
	 */
	EReference getCloud_HasEnvironment();

	/**
	 * Returns the meta object for class '{@link org.eclipse.kit.decia.CloudEntity <em>Cloud Entity</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Cloud Entity</em>'.
	 * @see org.eclipse.kit.decia.CloudEntity
	 * @generated
	 */
	EClass getCloudEntity();

	/**
	 * Returns the meta object for the attribute '{@link org.eclipse.kit.decia.CloudEntity#getId <em>Id</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Id</em>'.
	 * @see org.eclipse.kit.decia.CloudEntity#getId()
	 * @see #getCloudEntity()
	 * @generated
	 */
	EAttribute getCloudEntity_Id();

	/**
	 * Returns the meta object for class '{@link org.eclipse.kit.decia.Microservice <em>Microservice</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Microservice</em>'.
	 * @see org.eclipse.kit.decia.Microservice
	 * @generated
	 */
	EClass getMicroservice();

	/**
	 * Returns the meta object for the reference list '{@link org.eclipse.kit.decia.Microservice#getProducesMessages <em>Produces Messages</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference list '<em>Produces Messages</em>'.
	 * @see org.eclipse.kit.decia.Microservice#getProducesMessages()
	 * @see #getMicroservice()
	 * @generated
	 */
	EReference getMicroservice_ProducesMessages();

	/**
	 * Returns the meta object for the attribute '{@link org.eclipse.kit.decia.Microservice#getCfAppName <em>Cf App Name</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Cf App Name</em>'.
	 * @see org.eclipse.kit.decia.Microservice#getCfAppName()
	 * @see #getMicroservice()
	 * @generated
	 */
	EAttribute getMicroservice_CfAppName();

	/**
	 * Returns the meta object for the reference list '{@link org.eclipse.kit.decia.Microservice#getConsumesMessages <em>Consumes Messages</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference list '<em>Consumes Messages</em>'.
	 * @see org.eclipse.kit.decia.Microservice#getConsumesMessages()
	 * @see #getMicroservice()
	 * @generated
	 */
	EReference getMicroservice_ConsumesMessages();

	/**
	 * Returns the meta object for the containment reference list '{@link org.eclipse.kit.decia.Microservice#getContainsSystemStatMetric <em>Contains System Stat Metric</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Contains System Stat Metric</em>'.
	 * @see org.eclipse.kit.decia.Microservice#getContainsSystemStatMetric()
	 * @see #getMicroservice()
	 * @generated
	 */
	EReference getMicroservice_ContainsSystemStatMetric();

	/**
	 * Returns the meta object for class '{@link org.eclipse.kit.decia.MessageQueue <em>Message Queue</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Message Queue</em>'.
	 * @see org.eclipse.kit.decia.MessageQueue
	 * @generated
	 */
	EClass getMessageQueue();

	/**
	 * Returns the meta object for the containment reference list '{@link org.eclipse.kit.decia.MessageQueue#getContainsQueuemetric <em>Contains Queuemetric</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Contains Queuemetric</em>'.
	 * @see org.eclipse.kit.decia.MessageQueue#getContainsQueuemetric()
	 * @see #getMessageQueue()
	 * @generated
	 */
	EReference getMessageQueue_ContainsQueuemetric();

	/**
	 * Returns the meta object for class '{@link org.eclipse.kit.decia.MessageBroker <em>Message Broker</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Message Broker</em>'.
	 * @see org.eclipse.kit.decia.MessageBroker
	 * @generated
	 */
	EClass getMessageBroker();

	/**
	 * Returns the meta object for the containment reference list '{@link org.eclipse.kit.decia.MessageBroker#getManages <em>Manages</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Manages</em>'.
	 * @see org.eclipse.kit.decia.MessageBroker#getManages()
	 * @see #getMessageBroker()
	 * @generated
	 */
	EReference getMessageBroker_Manages();

	/**
	 * Returns the meta object for class '{@link org.eclipse.kit.decia.AMQPBroker <em>AMQP Broker</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>AMQP Broker</em>'.
	 * @see org.eclipse.kit.decia.AMQPBroker
	 * @generated
	 */
	EClass getAMQPBroker();

	/**
	 * Returns the meta object for class '{@link org.eclipse.kit.decia.RabbitMQBroker <em>Rabbit MQ Broker</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Rabbit MQ Broker</em>'.
	 * @see org.eclipse.kit.decia.RabbitMQBroker
	 * @generated
	 */
	EClass getRabbitMQBroker();

	/**
	 * Returns the meta object for the attribute '{@link org.eclipse.kit.decia.RabbitMQBroker#getMessageBrokerManagementURL <em>Message Broker Management URL</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Message Broker Management URL</em>'.
	 * @see org.eclipse.kit.decia.RabbitMQBroker#getMessageBrokerManagementURL()
	 * @see #getRabbitMQBroker()
	 * @generated
	 */
	EAttribute getRabbitMQBroker_MessageBrokerManagementURL();

	/**
	 * Returns the meta object for the attribute '{@link org.eclipse.kit.decia.RabbitMQBroker#getUser <em>User</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>User</em>'.
	 * @see org.eclipse.kit.decia.RabbitMQBroker#getUser()
	 * @see #getRabbitMQBroker()
	 * @generated
	 */
	EAttribute getRabbitMQBroker_User();

	/**
	 * Returns the meta object for the attribute '{@link org.eclipse.kit.decia.RabbitMQBroker#getPassword <em>Password</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Password</em>'.
	 * @see org.eclipse.kit.decia.RabbitMQBroker#getPassword()
	 * @see #getRabbitMQBroker()
	 * @generated
	 */
	EAttribute getRabbitMQBroker_Password();

	/**
	 * Returns the meta object for the attribute '{@link org.eclipse.kit.decia.RabbitMQBroker#getName <em>Name</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Name</em>'.
	 * @see org.eclipse.kit.decia.RabbitMQBroker#getName()
	 * @see #getRabbitMQBroker()
	 * @generated
	 */
	EAttribute getRabbitMQBroker_Name();

	/**
	 * Returns the meta object for the containment reference list '{@link org.eclipse.kit.decia.RabbitMQBroker#getContainsSystemstatmetric <em>Contains Systemstatmetric</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Contains Systemstatmetric</em>'.
	 * @see org.eclipse.kit.decia.RabbitMQBroker#getContainsSystemstatmetric()
	 * @see #getRabbitMQBroker()
	 * @generated
	 */
	EReference getRabbitMQBroker_ContainsSystemstatmetric();

	/**
	 * Returns the meta object for class '{@link org.eclipse.kit.decia.Autoscaler <em>Autoscaler</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Autoscaler</em>'.
	 * @see org.eclipse.kit.decia.Autoscaler
	 * @generated
	 */
	EClass getAutoscaler();

	/**
	 * Returns the meta object for class '{@link org.eclipse.kit.decia.ThresholdbasedAutoscaler <em>Thresholdbased Autoscaler</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Thresholdbased Autoscaler</em>'.
	 * @see org.eclipse.kit.decia.ThresholdbasedAutoscaler
	 * @generated
	 */
	EClass getThresholdbasedAutoscaler();

	/**
	 * Returns the meta object for the attribute '{@link org.eclipse.kit.decia.ThresholdbasedAutoscaler#getMinInstances <em>Min Instances</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Min Instances</em>'.
	 * @see org.eclipse.kit.decia.ThresholdbasedAutoscaler#getMinInstances()
	 * @see #getThresholdbasedAutoscaler()
	 * @generated
	 */
	EAttribute getThresholdbasedAutoscaler_MinInstances();

	/**
	 * Returns the meta object for the attribute '{@link org.eclipse.kit.decia.ThresholdbasedAutoscaler#getMaxInstances <em>Max Instances</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Max Instances</em>'.
	 * @see org.eclipse.kit.decia.ThresholdbasedAutoscaler#getMaxInstances()
	 * @see #getThresholdbasedAutoscaler()
	 * @generated
	 */
	EAttribute getThresholdbasedAutoscaler_MaxInstances();

	/**
	 * Returns the meta object for the attribute '{@link org.eclipse.kit.decia.ThresholdbasedAutoscaler#getLowerThreshold <em>Lower Threshold</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Lower Threshold</em>'.
	 * @see org.eclipse.kit.decia.ThresholdbasedAutoscaler#getLowerThreshold()
	 * @see #getThresholdbasedAutoscaler()
	 * @generated
	 */
	EAttribute getThresholdbasedAutoscaler_LowerThreshold();

	/**
	 * Returns the meta object for the attribute '{@link org.eclipse.kit.decia.ThresholdbasedAutoscaler#getUpperThreshold <em>Upper Threshold</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Upper Threshold</em>'.
	 * @see org.eclipse.kit.decia.ThresholdbasedAutoscaler#getUpperThreshold()
	 * @see #getThresholdbasedAutoscaler()
	 * @generated
	 */
	EAttribute getThresholdbasedAutoscaler_UpperThreshold();

	/**
	 * Returns the meta object for the reference list '{@link org.eclipse.kit.decia.ThresholdbasedAutoscaler#getScalesMicroservice <em>Scales Microservice</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference list '<em>Scales Microservice</em>'.
	 * @see org.eclipse.kit.decia.ThresholdbasedAutoscaler#getScalesMicroservice()
	 * @see #getThresholdbasedAutoscaler()
	 * @generated
	 */
	EReference getThresholdbasedAutoscaler_ScalesMicroservice();

	/**
	 * Returns the meta object for the reference list '{@link org.eclipse.kit.decia.ThresholdbasedAutoscaler#getScalesMessagequeue <em>Scales Messagequeue</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference list '<em>Scales Messagequeue</em>'.
	 * @see org.eclipse.kit.decia.ThresholdbasedAutoscaler#getScalesMessagequeue()
	 * @see #getThresholdbasedAutoscaler()
	 * @generated
	 */
	EReference getThresholdbasedAutoscaler_ScalesMessagequeue();

	/**
	 * Returns the meta object for the attribute '{@link org.eclipse.kit.decia.ThresholdbasedAutoscaler#getName <em>Name</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Name</em>'.
	 * @see org.eclipse.kit.decia.ThresholdbasedAutoscaler#getName()
	 * @see #getThresholdbasedAutoscaler()
	 * @generated
	 */
	EAttribute getThresholdbasedAutoscaler_Name();

	/**
	 * Returns the meta object for the reference '{@link org.eclipse.kit.decia.ThresholdbasedAutoscaler#getObservesSystemStatMetric <em>Observes System Stat Metric</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Observes System Stat Metric</em>'.
	 * @see org.eclipse.kit.decia.ThresholdbasedAutoscaler#getObservesSystemStatMetric()
	 * @see #getThresholdbasedAutoscaler()
	 * @generated
	 */
	EReference getThresholdbasedAutoscaler_ObservesSystemStatMetric();

	/**
	 * Returns the meta object for the reference '{@link org.eclipse.kit.decia.ThresholdbasedAutoscaler#getObservesQueuemetric <em>Observes Queuemetric</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Observes Queuemetric</em>'.
	 * @see org.eclipse.kit.decia.ThresholdbasedAutoscaler#getObservesQueuemetric()
	 * @see #getThresholdbasedAutoscaler()
	 * @generated
	 */
	EReference getThresholdbasedAutoscaler_ObservesQueuemetric();

	/**
	 * Returns the meta object for class '{@link org.eclipse.kit.decia.RabbitMQ <em>Rabbit MQ</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Rabbit MQ</em>'.
	 * @see org.eclipse.kit.decia.RabbitMQ
	 * @generated
	 */
	EClass getRabbitMQ();

	/**
	 * Returns the meta object for the attribute '{@link org.eclipse.kit.decia.RabbitMQ#getMessageQueueName <em>Message Queue Name</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Message Queue Name</em>'.
	 * @see org.eclipse.kit.decia.RabbitMQ#getMessageQueueName()
	 * @see #getRabbitMQ()
	 * @generated
	 */
	EAttribute getRabbitMQ_MessageQueueName();

	/**
	 * Returns the meta object for the attribute list '{@link org.eclipse.kit.decia.RabbitMQ#getType <em>Type</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute list '<em>Type</em>'.
	 * @see org.eclipse.kit.decia.RabbitMQ#getType()
	 * @see #getRabbitMQ()
	 * @generated
	 */
	EAttribute getRabbitMQ_Type();

	/**
	 * Returns the meta object for class '{@link org.eclipse.kit.decia.Environment <em>Environment</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Environment</em>'.
	 * @see org.eclipse.kit.decia.Environment
	 * @generated
	 */
	EClass getEnvironment();

	/**
	 * Returns the meta object for the attribute '{@link org.eclipse.kit.decia.Environment#getName <em>Name</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Name</em>'.
	 * @see org.eclipse.kit.decia.Environment#getName()
	 * @see #getEnvironment()
	 * @generated
	 */
	EAttribute getEnvironment_Name();

	/**
	 * Returns the meta object for the attribute '{@link org.eclipse.kit.decia.Environment#getProxyHost <em>Proxy Host</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Proxy Host</em>'.
	 * @see org.eclipse.kit.decia.Environment#getProxyHost()
	 * @see #getEnvironment()
	 * @generated
	 */
	EAttribute getEnvironment_ProxyHost();

	/**
	 * Returns the meta object for the attribute '{@link org.eclipse.kit.decia.Environment#getProxyPort <em>Proxy Port</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Proxy Port</em>'.
	 * @see org.eclipse.kit.decia.Environment#getProxyPort()
	 * @see #getEnvironment()
	 * @generated
	 */
	EAttribute getEnvironment_ProxyPort();

	/**
	 * Returns the meta object for class '{@link org.eclipse.kit.decia.SystemStatMetric <em>System Stat Metric</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>System Stat Metric</em>'.
	 * @see org.eclipse.kit.decia.SystemStatMetric
	 * @generated
	 */
	EClass getSystemStatMetric();

	/**
	 * Returns the meta object for the attribute list '{@link org.eclipse.kit.decia.SystemStatMetric#getServiceMetricType <em>Service Metric Type</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute list '<em>Service Metric Type</em>'.
	 * @see org.eclipse.kit.decia.SystemStatMetric#getServiceMetricType()
	 * @see #getSystemStatMetric()
	 * @generated
	 */
	EAttribute getSystemStatMetric_ServiceMetricType();

	/**
	 * Returns the meta object for class '{@link org.eclipse.kit.decia.QueueMetric <em>Queue Metric</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Queue Metric</em>'.
	 * @see org.eclipse.kit.decia.QueueMetric
	 * @generated
	 */
	EClass getQueueMetric();

	/**
	 * Returns the meta object for the attribute list '{@link org.eclipse.kit.decia.QueueMetric#getQueueMetricType <em>Queue Metric Type</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute list '<em>Queue Metric Type</em>'.
	 * @see org.eclipse.kit.decia.QueueMetric#getQueueMetricType()
	 * @see #getQueueMetric()
	 * @generated
	 */
	EAttribute getQueueMetric_QueueMetricType();

	/**
	 * Returns the meta object for enum '{@link org.eclipse.kit.decia.SystemStatMetricType <em>System Stat Metric Type</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for enum '<em>System Stat Metric Type</em>'.
	 * @see org.eclipse.kit.decia.SystemStatMetricType
	 * @generated
	 */
	EEnum getSystemStatMetricType();

	/**
	 * Returns the meta object for enum '{@link org.eclipse.kit.decia.RabbitMQType <em>Rabbit MQ Type</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for enum '<em>Rabbit MQ Type</em>'.
	 * @see org.eclipse.kit.decia.RabbitMQType
	 * @generated
	 */
	EEnum getRabbitMQType();

	/**
	 * Returns the meta object for enum '{@link org.eclipse.kit.decia.QueueMetricType <em>Queue Metric Type</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for enum '<em>Queue Metric Type</em>'.
	 * @see org.eclipse.kit.decia.QueueMetricType
	 * @generated
	 */
	EEnum getQueueMetricType();

	/**
	 * Returns the factory that creates the instances of the model.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the factory that creates the instances of the model.
	 * @generated
	 */
	DeciaFactory getDeciaFactory();

	/**
	 * <!-- begin-user-doc -->
	 * Defines literals for the meta objects that represent
	 * <ul>
	 *   <li>each class,</li>
	 *   <li>each feature of each class,</li>
	 *   <li>each operation of each class,</li>
	 *   <li>each enum,</li>
	 *   <li>and each data type</li>
	 * </ul>
	 * <!-- end-user-doc -->
	 * @generated
	 */
	interface Literals {
		/**
		 * The meta object literal for the '{@link org.eclipse.kit.decia.impl.CFConfigurationImpl <em>CF Configuration</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.eclipse.kit.decia.impl.CFConfigurationImpl
		 * @see org.eclipse.kit.decia.impl.DeciaPackageImpl#getCFConfiguration()
		 * @generated
		 */
		EClass CF_CONFIGURATION = eINSTANCE.getCFConfiguration();

		/**
		 * The meta object literal for the '<em><b>API Endpoint</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute CF_CONFIGURATION__API_ENDPOINT = eINSTANCE.getCFConfiguration_APIEndpoint();

		/**
		 * The meta object literal for the '<em><b>Username</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute CF_CONFIGURATION__USERNAME = eINSTANCE.getCFConfiguration_Username();

		/**
		 * The meta object literal for the '<em><b>Password</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute CF_CONFIGURATION__PASSWORD = eINSTANCE.getCFConfiguration_Password();

		/**
		 * The meta object literal for the '<em><b>Org Name</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute CF_CONFIGURATION__ORG_NAME = eINSTANCE.getCFConfiguration_OrgName();

		/**
		 * The meta object literal for the '<em><b>Space Name</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute CF_CONFIGURATION__SPACE_NAME = eINSTANCE.getCFConfiguration_SpaceName();

		/**
		 * The meta object literal for the '{@link org.eclipse.kit.decia.impl.CloudImpl <em>Cloud</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.eclipse.kit.decia.impl.CloudImpl
		 * @see org.eclipse.kit.decia.impl.DeciaPackageImpl#getCloud()
		 * @generated
		 */
		EClass CLOUD = eINSTANCE.getCloud();

		/**
		 * The meta object literal for the '<em><b>Has Config</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference CLOUD__HAS_CONFIG = eINSTANCE.getCloud_HasConfig();

		/**
		 * The meta object literal for the '<em><b>Contains</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference CLOUD__CONTAINS = eINSTANCE.getCloud_Contains();

		/**
		 * The meta object literal for the '<em><b>Name</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute CLOUD__NAME = eINSTANCE.getCloud_Name();

		/**
		 * The meta object literal for the '<em><b>Has Environment</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference CLOUD__HAS_ENVIRONMENT = eINSTANCE.getCloud_HasEnvironment();

		/**
		 * The meta object literal for the '{@link org.eclipse.kit.decia.impl.CloudEntityImpl <em>Cloud Entity</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.eclipse.kit.decia.impl.CloudEntityImpl
		 * @see org.eclipse.kit.decia.impl.DeciaPackageImpl#getCloudEntity()
		 * @generated
		 */
		EClass CLOUD_ENTITY = eINSTANCE.getCloudEntity();

		/**
		 * The meta object literal for the '<em><b>Id</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute CLOUD_ENTITY__ID = eINSTANCE.getCloudEntity_Id();

		/**
		 * The meta object literal for the '{@link org.eclipse.kit.decia.impl.MicroserviceImpl <em>Microservice</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.eclipse.kit.decia.impl.MicroserviceImpl
		 * @see org.eclipse.kit.decia.impl.DeciaPackageImpl#getMicroservice()
		 * @generated
		 */
		EClass MICROSERVICE = eINSTANCE.getMicroservice();

		/**
		 * The meta object literal for the '<em><b>Produces Messages</b></em>' reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference MICROSERVICE__PRODUCES_MESSAGES = eINSTANCE.getMicroservice_ProducesMessages();

		/**
		 * The meta object literal for the '<em><b>Cf App Name</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute MICROSERVICE__CF_APP_NAME = eINSTANCE.getMicroservice_CfAppName();

		/**
		 * The meta object literal for the '<em><b>Consumes Messages</b></em>' reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference MICROSERVICE__CONSUMES_MESSAGES = eINSTANCE.getMicroservice_ConsumesMessages();

		/**
		 * The meta object literal for the '<em><b>Contains System Stat Metric</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference MICROSERVICE__CONTAINS_SYSTEM_STAT_METRIC = eINSTANCE.getMicroservice_ContainsSystemStatMetric();

		/**
		 * The meta object literal for the '{@link org.eclipse.kit.decia.impl.MessageQueueImpl <em>Message Queue</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.eclipse.kit.decia.impl.MessageQueueImpl
		 * @see org.eclipse.kit.decia.impl.DeciaPackageImpl#getMessageQueue()
		 * @generated
		 */
		EClass MESSAGE_QUEUE = eINSTANCE.getMessageQueue();

		/**
		 * The meta object literal for the '<em><b>Contains Queuemetric</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference MESSAGE_QUEUE__CONTAINS_QUEUEMETRIC = eINSTANCE.getMessageQueue_ContainsQueuemetric();

		/**
		 * The meta object literal for the '{@link org.eclipse.kit.decia.impl.MessageBrokerImpl <em>Message Broker</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.eclipse.kit.decia.impl.MessageBrokerImpl
		 * @see org.eclipse.kit.decia.impl.DeciaPackageImpl#getMessageBroker()
		 * @generated
		 */
		EClass MESSAGE_BROKER = eINSTANCE.getMessageBroker();

		/**
		 * The meta object literal for the '<em><b>Manages</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference MESSAGE_BROKER__MANAGES = eINSTANCE.getMessageBroker_Manages();

		/**
		 * The meta object literal for the '{@link org.eclipse.kit.decia.impl.AMQPBrokerImpl <em>AMQP Broker</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.eclipse.kit.decia.impl.AMQPBrokerImpl
		 * @see org.eclipse.kit.decia.impl.DeciaPackageImpl#getAMQPBroker()
		 * @generated
		 */
		EClass AMQP_BROKER = eINSTANCE.getAMQPBroker();

		/**
		 * The meta object literal for the '{@link org.eclipse.kit.decia.impl.RabbitMQBrokerImpl <em>Rabbit MQ Broker</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.eclipse.kit.decia.impl.RabbitMQBrokerImpl
		 * @see org.eclipse.kit.decia.impl.DeciaPackageImpl#getRabbitMQBroker()
		 * @generated
		 */
		EClass RABBIT_MQ_BROKER = eINSTANCE.getRabbitMQBroker();

		/**
		 * The meta object literal for the '<em><b>Message Broker Management URL</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute RABBIT_MQ_BROKER__MESSAGE_BROKER_MANAGEMENT_URL = eINSTANCE
				.getRabbitMQBroker_MessageBrokerManagementURL();

		/**
		 * The meta object literal for the '<em><b>User</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute RABBIT_MQ_BROKER__USER = eINSTANCE.getRabbitMQBroker_User();

		/**
		 * The meta object literal for the '<em><b>Password</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute RABBIT_MQ_BROKER__PASSWORD = eINSTANCE.getRabbitMQBroker_Password();

		/**
		 * The meta object literal for the '<em><b>Name</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute RABBIT_MQ_BROKER__NAME = eINSTANCE.getRabbitMQBroker_Name();

		/**
		 * The meta object literal for the '<em><b>Contains Systemstatmetric</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference RABBIT_MQ_BROKER__CONTAINS_SYSTEMSTATMETRIC = eINSTANCE.getRabbitMQBroker_ContainsSystemstatmetric();

		/**
		 * The meta object literal for the '{@link org.eclipse.kit.decia.impl.AutoscalerImpl <em>Autoscaler</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.eclipse.kit.decia.impl.AutoscalerImpl
		 * @see org.eclipse.kit.decia.impl.DeciaPackageImpl#getAutoscaler()
		 * @generated
		 */
		EClass AUTOSCALER = eINSTANCE.getAutoscaler();

		/**
		 * The meta object literal for the '{@link org.eclipse.kit.decia.impl.ThresholdbasedAutoscalerImpl <em>Thresholdbased Autoscaler</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.eclipse.kit.decia.impl.ThresholdbasedAutoscalerImpl
		 * @see org.eclipse.kit.decia.impl.DeciaPackageImpl#getThresholdbasedAutoscaler()
		 * @generated
		 */
		EClass THRESHOLDBASED_AUTOSCALER = eINSTANCE.getThresholdbasedAutoscaler();

		/**
		 * The meta object literal for the '<em><b>Min Instances</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute THRESHOLDBASED_AUTOSCALER__MIN_INSTANCES = eINSTANCE.getThresholdbasedAutoscaler_MinInstances();

		/**
		 * The meta object literal for the '<em><b>Max Instances</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute THRESHOLDBASED_AUTOSCALER__MAX_INSTANCES = eINSTANCE.getThresholdbasedAutoscaler_MaxInstances();

		/**
		 * The meta object literal for the '<em><b>Lower Threshold</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute THRESHOLDBASED_AUTOSCALER__LOWER_THRESHOLD = eINSTANCE.getThresholdbasedAutoscaler_LowerThreshold();

		/**
		 * The meta object literal for the '<em><b>Upper Threshold</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute THRESHOLDBASED_AUTOSCALER__UPPER_THRESHOLD = eINSTANCE.getThresholdbasedAutoscaler_UpperThreshold();

		/**
		 * The meta object literal for the '<em><b>Scales Microservice</b></em>' reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference THRESHOLDBASED_AUTOSCALER__SCALES_MICROSERVICE = eINSTANCE
				.getThresholdbasedAutoscaler_ScalesMicroservice();

		/**
		 * The meta object literal for the '<em><b>Scales Messagequeue</b></em>' reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference THRESHOLDBASED_AUTOSCALER__SCALES_MESSAGEQUEUE = eINSTANCE
				.getThresholdbasedAutoscaler_ScalesMessagequeue();

		/**
		 * The meta object literal for the '<em><b>Name</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute THRESHOLDBASED_AUTOSCALER__NAME = eINSTANCE.getThresholdbasedAutoscaler_Name();

		/**
		 * The meta object literal for the '<em><b>Observes System Stat Metric</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference THRESHOLDBASED_AUTOSCALER__OBSERVES_SYSTEM_STAT_METRIC = eINSTANCE
				.getThresholdbasedAutoscaler_ObservesSystemStatMetric();

		/**
		 * The meta object literal for the '<em><b>Observes Queuemetric</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference THRESHOLDBASED_AUTOSCALER__OBSERVES_QUEUEMETRIC = eINSTANCE
				.getThresholdbasedAutoscaler_ObservesQueuemetric();

		/**
		 * The meta object literal for the '{@link org.eclipse.kit.decia.impl.RabbitMQImpl <em>Rabbit MQ</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.eclipse.kit.decia.impl.RabbitMQImpl
		 * @see org.eclipse.kit.decia.impl.DeciaPackageImpl#getRabbitMQ()
		 * @generated
		 */
		EClass RABBIT_MQ = eINSTANCE.getRabbitMQ();

		/**
		 * The meta object literal for the '<em><b>Message Queue Name</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute RABBIT_MQ__MESSAGE_QUEUE_NAME = eINSTANCE.getRabbitMQ_MessageQueueName();

		/**
		 * The meta object literal for the '<em><b>Type</b></em>' attribute list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute RABBIT_MQ__TYPE = eINSTANCE.getRabbitMQ_Type();

		/**
		 * The meta object literal for the '{@link org.eclipse.kit.decia.impl.EnvironmentImpl <em>Environment</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.eclipse.kit.decia.impl.EnvironmentImpl
		 * @see org.eclipse.kit.decia.impl.DeciaPackageImpl#getEnvironment()
		 * @generated
		 */
		EClass ENVIRONMENT = eINSTANCE.getEnvironment();

		/**
		 * The meta object literal for the '<em><b>Name</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute ENVIRONMENT__NAME = eINSTANCE.getEnvironment_Name();

		/**
		 * The meta object literal for the '<em><b>Proxy Host</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute ENVIRONMENT__PROXY_HOST = eINSTANCE.getEnvironment_ProxyHost();

		/**
		 * The meta object literal for the '<em><b>Proxy Port</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute ENVIRONMENT__PROXY_PORT = eINSTANCE.getEnvironment_ProxyPort();

		/**
		 * The meta object literal for the '{@link org.eclipse.kit.decia.impl.SystemStatMetricImpl <em>System Stat Metric</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.eclipse.kit.decia.impl.SystemStatMetricImpl
		 * @see org.eclipse.kit.decia.impl.DeciaPackageImpl#getSystemStatMetric()
		 * @generated
		 */
		EClass SYSTEM_STAT_METRIC = eINSTANCE.getSystemStatMetric();

		/**
		 * The meta object literal for the '<em><b>Service Metric Type</b></em>' attribute list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute SYSTEM_STAT_METRIC__SERVICE_METRIC_TYPE = eINSTANCE.getSystemStatMetric_ServiceMetricType();

		/**
		 * The meta object literal for the '{@link org.eclipse.kit.decia.impl.QueueMetricImpl <em>Queue Metric</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.eclipse.kit.decia.impl.QueueMetricImpl
		 * @see org.eclipse.kit.decia.impl.DeciaPackageImpl#getQueueMetric()
		 * @generated
		 */
		EClass QUEUE_METRIC = eINSTANCE.getQueueMetric();

		/**
		 * The meta object literal for the '<em><b>Queue Metric Type</b></em>' attribute list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute QUEUE_METRIC__QUEUE_METRIC_TYPE = eINSTANCE.getQueueMetric_QueueMetricType();

		/**
		 * The meta object literal for the '{@link org.eclipse.kit.decia.SystemStatMetricType <em>System Stat Metric Type</em>}' enum.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.eclipse.kit.decia.SystemStatMetricType
		 * @see org.eclipse.kit.decia.impl.DeciaPackageImpl#getSystemStatMetricType()
		 * @generated
		 */
		EEnum SYSTEM_STAT_METRIC_TYPE = eINSTANCE.getSystemStatMetricType();

		/**
		 * The meta object literal for the '{@link org.eclipse.kit.decia.RabbitMQType <em>Rabbit MQ Type</em>}' enum.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.eclipse.kit.decia.RabbitMQType
		 * @see org.eclipse.kit.decia.impl.DeciaPackageImpl#getRabbitMQType()
		 * @generated
		 */
		EEnum RABBIT_MQ_TYPE = eINSTANCE.getRabbitMQType();

		/**
		 * The meta object literal for the '{@link org.eclipse.kit.decia.QueueMetricType <em>Queue Metric Type</em>}' enum.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.eclipse.kit.decia.QueueMetricType
		 * @see org.eclipse.kit.decia.impl.DeciaPackageImpl#getQueueMetricType()
		 * @generated
		 */
		EEnum QUEUE_METRIC_TYPE = eINSTANCE.getQueueMetricType();

	}

} //DeciaPackage
