/**
 */
package org.eclipse.kit.decia;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import org.eclipse.emf.common.util.Enumerator;

/**
 * <!-- begin-user-doc -->
 * A representation of the literals of the enumeration '<em><b>Queue Metric Type</b></em>',
 * and utility methods for working with them.
 * <!-- end-user-doc -->
 * @see org.eclipse.kit.decia.DeciaPackage#getQueueMetricType()
 * @model
 * @generated
 */
public enum QueueMetricType implements Enumerator {
	/**
	 * The '<em><b>Queue Length</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #QUEUE_LENGTH_VALUE
	 * @generated
	 * @ordered
	 */
	QUEUE_LENGTH(0, "QueueLength", "QueueLength"),

	/**
	 * The '<em><b>Queue Growth</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #QUEUE_GROWTH_VALUE
	 * @generated
	 * @ordered
	 */
	QUEUE_GROWTH(1, "QueueGrowth", "QueueGrowth"),

	/**
	 * The '<em><b>Queueing Delay</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #QUEUEING_DELAY_VALUE
	 * @generated
	 * @ordered
	 */
	QUEUEING_DELAY(2, "QueueingDelay", "QueueingDelay");

	/**
	 * The '<em><b>Queue Length</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>Queue Length</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #QUEUE_LENGTH
	 * @model name="QueueLength"
	 * @generated
	 * @ordered
	 */
	public static final int QUEUE_LENGTH_VALUE = 0;

	/**
	 * The '<em><b>Queue Growth</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>Queue Growth</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #QUEUE_GROWTH
	 * @model name="QueueGrowth"
	 * @generated
	 * @ordered
	 */
	public static final int QUEUE_GROWTH_VALUE = 1;

	/**
	 * The '<em><b>Queueing Delay</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>Queueing Delay</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #QUEUEING_DELAY
	 * @model name="QueueingDelay"
	 * @generated
	 * @ordered
	 */
	public static final int QUEUEING_DELAY_VALUE = 2;

	/**
	 * An array of all the '<em><b>Queue Metric Type</b></em>' enumerators.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private static final QueueMetricType[] VALUES_ARRAY = new QueueMetricType[] { QUEUE_LENGTH, QUEUE_GROWTH,
			QUEUEING_DELAY, };

	/**
	 * A public read-only list of all the '<em><b>Queue Metric Type</b></em>' enumerators.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static final List<QueueMetricType> VALUES = Collections.unmodifiableList(Arrays.asList(VALUES_ARRAY));

	/**
	 * Returns the '<em><b>Queue Metric Type</b></em>' literal with the specified literal value.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param literal the literal.
	 * @return the matching enumerator or <code>null</code>.
	 * @generated
	 */
	public static QueueMetricType get(String literal) {
		for (int i = 0; i < VALUES_ARRAY.length; ++i) {
			QueueMetricType result = VALUES_ARRAY[i];
			if (result.toString().equals(literal)) {
				return result;
			}
		}
		return null;
	}

	/**
	 * Returns the '<em><b>Queue Metric Type</b></em>' literal with the specified name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param name the name.
	 * @return the matching enumerator or <code>null</code>.
	 * @generated
	 */
	public static QueueMetricType getByName(String name) {
		for (int i = 0; i < VALUES_ARRAY.length; ++i) {
			QueueMetricType result = VALUES_ARRAY[i];
			if (result.getName().equals(name)) {
				return result;
			}
		}
		return null;
	}

	/**
	 * Returns the '<em><b>Queue Metric Type</b></em>' literal with the specified integer value.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the integer value.
	 * @return the matching enumerator or <code>null</code>.
	 * @generated
	 */
	public static QueueMetricType get(int value) {
		switch (value) {
		case QUEUE_LENGTH_VALUE:
			return QUEUE_LENGTH;
		case QUEUE_GROWTH_VALUE:
			return QUEUE_GROWTH;
		case QUEUEING_DELAY_VALUE:
			return QUEUEING_DELAY;
		}
		return null;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private final int value;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private final String name;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private final String literal;

	/**
	 * Only this class can construct instances.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private QueueMetricType(int value, String name, String literal) {
		this.value = value;
		this.name = name;
		this.literal = literal;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public int getValue() {
		return value;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getName() {
		return name;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getLiteral() {
		return literal;
	}

	/**
	 * Returns the literal value of the enumerator, which is its string representation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		return literal;
	}

} //QueueMetricType
