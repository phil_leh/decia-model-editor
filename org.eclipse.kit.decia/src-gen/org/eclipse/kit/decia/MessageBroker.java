/**
 */
package org.eclipse.kit.decia;

import org.eclipse.emf.common.util.EList;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Message Broker</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link org.eclipse.kit.decia.MessageBroker#getManages <em>Manages</em>}</li>
 * </ul>
 *
 * @see org.eclipse.kit.decia.DeciaPackage#getMessageBroker()
 * @model abstract="true"
 * @generated
 */
public interface MessageBroker extends CloudEntity {
	/**
	 * Returns the value of the '<em><b>Manages</b></em>' containment reference list.
	 * The list contents are of type {@link org.eclipse.kit.decia.MessageQueue}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Manages</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Manages</em>' containment reference list.
	 * @see org.eclipse.kit.decia.DeciaPackage#getMessageBroker_Manages()
	 * @model containment="true"
	 * @generated
	 */
	EList<MessageQueue> getManages();

} // MessageBroker
