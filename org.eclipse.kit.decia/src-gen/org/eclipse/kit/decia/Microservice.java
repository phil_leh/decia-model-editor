/**
 */
package org.eclipse.kit.decia;

import org.eclipse.emf.common.util.EList;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Microservice</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link org.eclipse.kit.decia.Microservice#getProducesMessages <em>Produces Messages</em>}</li>
 *   <li>{@link org.eclipse.kit.decia.Microservice#getCfAppName <em>Cf App Name</em>}</li>
 *   <li>{@link org.eclipse.kit.decia.Microservice#getConsumesMessages <em>Consumes Messages</em>}</li>
 *   <li>{@link org.eclipse.kit.decia.Microservice#getContainsSystemStatMetric <em>Contains System Stat Metric</em>}</li>
 * </ul>
 *
 * @see org.eclipse.kit.decia.DeciaPackage#getMicroservice()
 * @model
 * @generated
 */
public interface Microservice extends CloudEntity {
	/**
	 * Returns the value of the '<em><b>Produces Messages</b></em>' reference list.
	 * The list contents are of type {@link org.eclipse.kit.decia.MessageQueue}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Produces Messages</em>' reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Produces Messages</em>' reference list.
	 * @see org.eclipse.kit.decia.DeciaPackage#getMicroservice_ProducesMessages()
	 * @model
	 * @generated
	 */
	EList<MessageQueue> getProducesMessages();

	/**
	 * Returns the value of the '<em><b>Cf App Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Cf App Name</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Cf App Name</em>' attribute.
	 * @see #setCfAppName(String)
	 * @see org.eclipse.kit.decia.DeciaPackage#getMicroservice_CfAppName()
	 * @model
	 * @generated
	 */
	String getCfAppName();

	/**
	 * Sets the value of the '{@link org.eclipse.kit.decia.Microservice#getCfAppName <em>Cf App Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Cf App Name</em>' attribute.
	 * @see #getCfAppName()
	 * @generated
	 */
	void setCfAppName(String value);

	/**
	 * Returns the value of the '<em><b>Consumes Messages</b></em>' reference list.
	 * The list contents are of type {@link org.eclipse.kit.decia.MessageQueue}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Consumes Messages</em>' reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Consumes Messages</em>' reference list.
	 * @see org.eclipse.kit.decia.DeciaPackage#getMicroservice_ConsumesMessages()
	 * @model
	 * @generated
	 */
	EList<MessageQueue> getConsumesMessages();

	/**
	 * Returns the value of the '<em><b>Contains System Stat Metric</b></em>' containment reference list.
	 * The list contents are of type {@link org.eclipse.kit.decia.SystemStatMetric}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Contains System Stat Metric</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Contains System Stat Metric</em>' containment reference list.
	 * @see org.eclipse.kit.decia.DeciaPackage#getMicroservice_ContainsSystemStatMetric()
	 * @model containment="true"
	 * @generated
	 */
	EList<SystemStatMetric> getContainsSystemStatMetric();

} // Microservice
