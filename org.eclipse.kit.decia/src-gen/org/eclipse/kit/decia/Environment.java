/**
 */
package org.eclipse.kit.decia;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Environment</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link org.eclipse.kit.decia.Environment#getName <em>Name</em>}</li>
 *   <li>{@link org.eclipse.kit.decia.Environment#getProxyHost <em>Proxy Host</em>}</li>
 *   <li>{@link org.eclipse.kit.decia.Environment#getProxyPort <em>Proxy Port</em>}</li>
 * </ul>
 *
 * @see org.eclipse.kit.decia.DeciaPackage#getEnvironment()
 * @model
 * @generated
 */
public interface Environment extends EObject {
	/**
	 * Returns the value of the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Name</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Name</em>' attribute.
	 * @see #setName(String)
	 * @see org.eclipse.kit.decia.DeciaPackage#getEnvironment_Name()
	 * @model
	 * @generated
	 */
	String getName();

	/**
	 * Sets the value of the '{@link org.eclipse.kit.decia.Environment#getName <em>Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Name</em>' attribute.
	 * @see #getName()
	 * @generated
	 */
	void setName(String value);

	/**
	 * Returns the value of the '<em><b>Proxy Host</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Proxy Host</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Proxy Host</em>' attribute.
	 * @see #setProxyHost(String)
	 * @see org.eclipse.kit.decia.DeciaPackage#getEnvironment_ProxyHost()
	 * @model
	 * @generated
	 */
	String getProxyHost();

	/**
	 * Sets the value of the '{@link org.eclipse.kit.decia.Environment#getProxyHost <em>Proxy Host</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Proxy Host</em>' attribute.
	 * @see #getProxyHost()
	 * @generated
	 */
	void setProxyHost(String value);

	/**
	 * Returns the value of the '<em><b>Proxy Port</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Proxy Port</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Proxy Port</em>' attribute.
	 * @see #setProxyPort(String)
	 * @see org.eclipse.kit.decia.DeciaPackage#getEnvironment_ProxyPort()
	 * @model
	 * @generated
	 */
	String getProxyPort();

	/**
	 * Sets the value of the '{@link org.eclipse.kit.decia.Environment#getProxyPort <em>Proxy Port</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Proxy Port</em>' attribute.
	 * @see #getProxyPort()
	 * @generated
	 */
	void setProxyPort(String value);

} // Environment
